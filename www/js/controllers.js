angular.module('starter.controllers', ['jrCrop','ngDraggable'])

.directive('repeatDone', function () {
   return function (scope, element, attrs) {
     if (scope.$last) {
       scope.$eval(attrs.repeatDone);
     }
   }
})

.controller('AppCtrl', function($scope, $rootScope, $ionicPopover) {
   $ionicPopover.fromTemplateUrl('templates/popup.html', {
    scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });
    $scope.show = function($event) {
      $scope.popover.show($event);
    };
    $scope.hidePop = function() {
      $scope.popover.hide();
    };
    // Deshabilita el drag desde el slide menu
    $rootScope.toggledrag = false;
})

.controller("mediaCtrl", function($scope, $ionicPlatform, $cordovaMedia, $cordovaFile, $http) {
   $ionicPlatform.ready(function(){
      $scope.play = function(src) {
          var media = $cordovaMedia.newMedia(src);
          media.play();
          //$cordovaMedia.play(media);
      }
      $scope.slideHasChanged = function(src, srcdos, Id){
          $scope.idPalabraCapturada = Id;
          //console.log('Id palabra vista: '+Id);
          if (srcdos !== null && srcdos !== '' && srcdos.length >= 9){
              $scope.play(srcdos);
              //  console.log('cuantos srcdos: ' + srcdos.length);
          } else {
              $scope.play(src);
              //console.log('cuantos src: ' + src.length);
          }
       }
       $scope.slideHasChangedColores = function(src){
               $scope.play(src);
        }
       $scope.slideHasChangedSrcOnly = function(src, Id){
           if (src !== null && src !== ''){
               $scope.play(src);
               //$scope.idPalabraCapturada = Id;
           }
       }
       $scope.slideHasChangedEnglish = function(src, srcdos){
               // Con el $http.head para comprobar la existencia de los audios
               // dentro de la carpeta www/raw/ de la app.
               $http.head("./"+srcdos).then(
                  function(response) {
                    $scope.play(srcdos);
                    //console.log('el archivo SI está');
                  },
                  function(error) {
                    window.TTS.speak({
                      text: src, rate: 1
                    }, function(){
                      }, function(reason){
                    })
                    //console.log('el archivo NO está');
                  }
               );
        }
        $scope.slideHasChangedEnglishFamily = function(src){
                window.TTS.speak(src, function(){
                }, function(reason){
                })
        }
        $scope.audioCompletarEsp = function(src, srcdos){
          $http.head("./"+srcdos).then(
             function(response) {
               $scope.play(srcdos);
             },
             function(error) {
               $scope.play(src);
             }
          );
        }
        $scope.audioCompletar = function(src, srcdos){
          $http.head("./"+srcdos).then(
             function(response) {
               $scope.play(srcdos);
             },
             function(error) {
               window.TTS.speak({
                 text: src, rate: 1
               }, function(){
                 }, function(reason){
               })
             }
          );
        }
   })
})

/* --- Inicio Login Ctrl --- */
.controller('loginCtrl', function($scope, $ionicModal, $ionicPopup, $timeout, $http, userService, $state, $location, $ionicHistory, $cordovaFileTransfer, $cordovaFile) {
  $scope.data = {};
  $scope.response = [];

  $ionicHistory.clearHistory();

  $scope.doLogin = function() {

    var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    var link = 'http://www.playerpasslist.com/miabcd_ios/miabc_login.php';

  $http.post(link,{
    username: $scope.data.username,
    password: $scope.data.password,
  },{
    headers: headers
  }).then(function successCallback(response){

    $scope.response = response.data.mensaje;
    $scope.variable = response.data.variable;
    $scope.id_app_user = response.data.id_app_user;
    $scope.name = response.data.name;
    $scope.username = response.data.username;
    $scope.email = response.data.email;
    $scope.password = response.data.password;
    $scope.player_id = response.data.player_id;
    $scope.sound_path = response.data.sound_path;
    $scope.image_path = response.data.image_path;
    $scope.congrats_path = response.data.congrats_path;

    if ($scope.variable == 1) {
      userService.setId(response.data.id_app_user);
      userService.setName(response.data.name);
      userService.setUsername(response.data.username);
      userService.setPassword(response.data.password);
      userService.setEmail(response.data.email);
      userService.setIdPlayer(response.data.player_id);
      userService.setPicture(response.data.image_path);
      userService.setSound(response.data.sound_path);
      userService.setCongrats(response.data.congrats_path);

      //console.log('Username userService: '+userService.userData.username);

      // Agignar el localStorage después de iniciar sesión.
      window.localStorage.setItem('profile', true);
      window.localStorage.setItem('count', 1);
      window.localStorage.setItem('nameProfile', $scope.name);
      window.localStorage.setItem('imageProfile', $scope.image_path);
      window.localStorage.setItem('usernameProfile', $scope.username);
      window.localStorage.setItem('emailProfile', $scope.email);
      window.localStorage.setItem('soundProfile', $scope.sound_path);
      window.localStorage.setItem('congratsProfile', $scope.congrats_path);

      $state.go('app.inicio');

      // Descargar los audios a la carpeta
      var nombrePerfilAudio = $scope.sound_path.split("/").pop();
      var felicidadesAudio = $scope.congrats_path.split("/").pop();
      var rutaAudioNombrePerfil = cordova.file.documentsDirectory + nombrePerfilAudio;
      var rutaAudioFelicidades = cordova.file.documentsDirectory + felicidadesAudio;

      $scope.getDescargarAudio1 = function(){
          var targetPath1 = encodeURI(rutaAudioNombrePerfil);
          var uri1 = encodeURI('http://www.playerpasslist.com/miabc_uploads/' + nombrePerfilAudio);
          $cordovaFileTransfer.download(uri1, targetPath1, {}, true).then(
                function(entry) {
                  //console.log("Archivo nombre audio descargado al dispositivo");
                },
                function (err) {
                  //console.log('Error en la descarga del archivo: ', err);
                },
                function (progress) {
                  $timeout(function () {
                    var percentComplete = (progress.loaded / progress.total) * 100;
                    //console.log('Archivo audio nombre perfil descargado - porcentaje = '+percentComplete);
                  })
                }
          );
       }
       $scope.getDescargarAudio2 = function(){
           var targetPath2 = encodeURI(rutaAudioFelicidades);
           var uri2 = encodeURI('http://www.playerpasslist.com/miabc_uploads/' + felicidadesAudio);
           $cordovaFileTransfer.download(uri2, targetPath2, {}, true).then(
                 function(entry) {
                   //console.log("Archivo felicidades audio descargado al dispositivo");
                 },
                 function (err) {
                   //console.log('Error en la descarga del archivo: ', err);
                 },
                 function (progress) {
                   $timeout(function () {
                     var percentComplete = (progress.loaded / progress.total) * 100;
                     //console.log('Archivo audio felicidades - porcentaje = '+percentComplete);
                   })
                 }
           );
        }
       $scope.getDescargarAudio1();
       $scope.getDescargarAudio2();

    }else {
      $scope.noEncontrado = $ionicPopup.show({
        title: $scope.response
      });
      setTimeout(function() {
        $scope.noEncontrado.close();
      }, 4000);
      // Limpiar campos del formulario
      $scope.data.username = '';
      $scope.data.password = '';
    }

    },function errorCallback(response) {
      $scope.mensaje = $ionicPopup.show({
        title: '¡ No se puede iniciar sesión, por favor conéctese a internet !',
        buttons: [
                { text: 'Ok',
                  type: 'button-dark button-full',
                  onTap: function() { return }
                }
        ]
      });
      //console.log(JSON.stringify(response));
    })

  } // fin doLogin()
})
/* --- Fin Login Ctrl --- */


/* --- Inicio Logout Button Ctrl --- */
.controller('logoutCtrl', function($ionicPlatform, $scope, $location) {
  $scope.logOut = function() {
    $scope.hidePop()
    $location.path("/app/login");
    // Destruir los datos del localStorage.
    window.localStorage.removeItem('count');
    window.localStorage.removeItem('profile');
    window.localStorage.removeItem('nameProfile');
    window.localStorage.removeItem('imageProfile');
    window.localStorage.removeItem('usernameProfile');
    window.localStorage.removeItem('emailProfile');
    window.localStorage.removeItem('soundProfile');
    window.localStorage.removeItem('congratsProfile');
  }
})
/* --- Fin Logout Button Ctrl --- */


/* --- Inicio Registro Ctrl --- */
.controller('registerCtrl', function($ionicPlatform, userService, $cordovaMedia, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $scope, $filter, $rootScope, $ionicModal, $ionicPopup, $timeout, $http, $state, $location) {
  $scope.data = {};
  $scope.response = [];

  $ionicPlatform.ready(function(){
     $scope.imgURI = null;

     $scope.play = function(src) {
         var media = $cordovaMedia.newMedia(src);
         media.play();
     };
     $scope.showPopup = function() {
       $scope.alertPopup = $ionicPopup.show({
         title: 'Grabando audio...'
       });
     };
     $scope.recordStart = function(){
         $scope.tempSrc = null;
         var random = 0;
         random = Math.round(Math.random()*1000000);
         var fecha = $filter('date')(new Date(),'ddMMyyyy');

         $scope.tempSrc = "documents://nombre_perfil_"+fecha+random+"_ios.wav";

         var media = $cordovaMedia.newMedia($scope.tempSrc);
         media.startRecord();
         $scope.showPopup();
         $scope.recordEnd = function(){
             media.stopRecord();
             $scope.alertPopup.close();
             media.release();
             // Para comprobar existencia de audio en la carpeta.
          /* var fileAudio = $scope.tempSrc.split("/").pop();
             console.log('fileAudio: '+fileAudio);
             $cordovaFile.checkFile(cordova.file.documentsDirectory, fileAudio)
              .then(function (success) {
                 console.log("Ruta audio encontrada");
               }, function (error) {
                  console.log("Ruta audio NO encontrada");
             }); */
         };
         $scope.name_sound_path = $scope.tempSrc;
     };

     $scope.recordStartCon = function(){
         $scope.tempSrc2 = null;
         var random = 0;
         random = Math.round(Math.random()*1000000);
         var fecha = $filter('date')(new Date(),'ddMMyyyy');

         $scope.tempSrc2 = "documents://frase_felicitacion_"+fecha+random+"_ios.wav";

         var media = $cordovaMedia.newMedia($scope.tempSrc2);
         media.startRecord();
         $scope.showPopup();
         $scope.recordEndCon = function(){
             media.stopRecord();
             $scope.alertPopup.close();
             media.release();
         };
         $scope.congrats_path = $scope.tempSrc2;
     };

     $scope.playing = function(src){
         if (src != null){
             $scope.play(src);
         }else{
             //console.log('No hay sonido!');
         }
      };

     $scope.savedPopup = function() {
       $scope.saved = $ionicPopup.show({
         title: '¡ Perfil registrado exitósamente !'
       });
       setTimeout(function() {
         $scope.saved.close();
         $location.path("/app/login");
       }, 4000);
     };

     $scope.nullPopup = function() {
           $scope.nullpop = $ionicPopup.show({
             title: 'Alert',
             template: '<p style="text-align: justify !important;">Datos incompletos, asegurese de seleccionar la imagen, completar los campos faltantes y grabar los sonidos del nombre del perfil y la frase de felicitaci&oacute;n.</p>',
             buttons: [
                       { text: 'Cerrar',
                         type: 'button-assertive'
                       }
                      ]
           });
     };
     $scope.helpPopupProfile = function() {
           var popupPerfil = $ionicPopup.show({
             title: 'Ayuda',
             templateUrl: 'templates/helpProfile.html',
             buttons: [
                         { text: 'Cerrar',
                           type: 'button-assertive'
                         }
                      ]
           });
     };

     $scope.gallery = function() {
         $scope.imgURI = null;
         var options = {
             quality : 75,
             destinationType : Camera.DestinationType.FILE_URI,
             sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
             allowEdit : true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 300,
             targetHeight: 300,
             popoverOptions: CameraPopoverOptions,
             saveToPhotoAlbum: false
         };
         $cordovaCamera.getPicture(options).then(function(imageData) {
             var rutaImagen = imageData.substr(7);
             $scope.rutaImagenPerfil = rutaImagen;

            // Para personalizar el nombre de la imagen
             var currentName = imageData.replace(/^.*[\\\/]/, '');
             var fechaHoy = new Date(),
             n = fechaHoy.getTime(),
             newFileName =  "profile_" + n + ".jpg";
             // Se obtiene la ruta de la imagen y se quita la imagen en .jpg
             var completePath = imageData.substr(0,imageData.lastIndexOf('/')+1);
             // Se copia la imagen con el nuevo nombre en la misma carpeta temp
             $cordovaFile.copyFile(completePath, currentName, cordova.file.tempDirectory, newFileName).then(function(success){
                  $scope.imgURI = newFileName;
             }, function(error){
                  $scope.showAlert('Error', error.exception);
             });
             // Nueva ruta de la imagen para la vista
             $scope.pathForImage = function(imgURI) {
               if (imgURI == null) {
                 return '';
               } else {
                 return cordova.file.tempDirectory + imgURI;
               }
             };

         }, function(err) {
         });
     }
     $scope.takePicture = function() {
         $scope.imgURI = null;
         var options = {
             quality : 75,
             destinationType : Camera.DestinationType.FILE_URI,
             sourceType : Camera.PictureSourceType.CAMERA,
             allowEdit : true,
             encodingType: Camera.EncodingType.JPEG,
             targetWidth: 300,
             targetHeight: 300,
             popoverOptions: CameraPopoverOptions,
             saveToPhotoAlbum: false
         };
         $cordovaCamera.getPicture(options).then(function(imageData) {
            var rutaImagen = imageData.substr(7);
            $scope.rutaImagenPerfil = rutaImagen;

           // Para personalizar el nombre de la imagen
            var currentName = imageData.replace(/^.*[\\\/]/, '');
            var fechaHoy = new Date(),
            n = fechaHoy.getTime(),
            newFileName =  "profile_" + n + ".jpg";
            // Se obtiene la ruta de la imagen y se quita la imagen en .jpg
            var completePath = imageData.substr(0,imageData.lastIndexOf('/')+1);
            // Se copia la imagen con el nuevo nombre en la misma carpeta temp
            $cordovaFile.copyFile(completePath, currentName, cordova.file.tempDirectory, newFileName).then(function(success){
                 $scope.imgURI = newFileName;
            }, function(error){
                 $scope.showAlert('Error', error.exception);
            });
            // Nueva ruta de la imagen para la vista
            $scope.pathForImage = function(imgURI) {
              if (imgURI == null) {
                return '';
              } else {
                return cordova.file.tempDirectory + imgURI;
              }
            };

         }, function(err) {
         });
     }
     $scope.editImage = function() {
       $scope.edit = $ionicPopup.show({
         title: 'Seleccione:',
         cssClass: 'popup-vertical-buttons',
         buttons: [
                 { text: 'Galería',
                   type: 'button-dark button-full',
                   onTap: function() {
                        $scope.gallery();
                   }
                 },
                 { text: 'Cámara',
                   type: 'button-dark button-full',
                   onTap: function() {
                        $scope.takePicture();
                   }
                 },
                 { text: 'Cancelar',
                   type: 'button-dark button-full',
                   onTap: function() {
                        return
                   }
                 }
               ]
         });
     }

  });

  // Subir foto al servidor.
  $scope.subirImagenAlServidor = function(id_app_user){
    var url = "http://www.playerpasslist.com/miabcd_ios/miabc_upload_image.php";

    var targetPath = $scope.pathForImage($scope.imgURI);
    var filename = targetPath.split("/").pop();

    var options = {
     fileKey: "file",
     fileName: filename,
     chunkedMode: false,
     mimeType: "image/jpeg",
     params: {'directory': 'miabc_uploads', 'filename': 'filename','id_app_user': userService.userData.id_app_user}
   };

   $cordovaFileTransfer.upload(url, targetPath, options, true).then(function(result) {
      //console.log("SUCCESS: " + JSON.stringify(result.response));
      //console.log("Subió la foto al servidor");
   }, function(err) {
      //console.log("ERROR: " + JSON.stringify(err));
   }, function (progress) {
         $timeout(function () {
           $scope.downloadProgress = (progress.loaded / progress.total) * 100;
         })
   });
   // Fin subir foto

  }

   // Subir Audio al servidor.
   $scope.subirAudioAlServidor = function(id_app_user){
     var url = "http://www.playerpasslist.com/miabcd_ios/miabc_upload_sound.php";

     var targetPathAudio1 = $scope.tempSrc;
     var filenameAudio1 = targetPathAudio1.split("/").pop();
     var targetPathAudioNew1 = cordova.file.documentsDirectory + filenameAudio1;

     var options = {
      fileKey: "file",
      fileName: filenameAudio1,
      chunkedMode: false,
      mimeType: "audio/wav",
      params: {'directory': 'miabc_uploads', 'filename': 'filenameAudio1','id_app_user': userService.userData.id_app_user}
    };

    $cordovaFileTransfer.upload(url, targetPathAudioNew1, options, true).then(function(result) {
       //console.log("SUCCESS: " + JSON.stringify(result.response));
       //console.log("Subió el audio 1 al servidor");
    }, function(err) {
       //console.log("ERROR: " + JSON.stringify(err));
    }, function (progress) {
    });
    // Fin subir audios

    // Audio 2
    var targetPathAudio2 = $scope.tempSrc2;
    var filenameAudio2 = targetPathAudio2.split("/").pop();
    var targetPathAudioNew2 = cordova.file.documentsDirectory + filenameAudio2;

    var options2 = {
     fileKey: "file",
     fileName: filenameAudio2,
     chunkedMode: false,
     mimeType: "audio/wav",
     params: {'directory': 'miabc_uploads', 'filename': 'filenameAudio2','id_app_user': userService.userData.id_app_user}
   };

   $cordovaFileTransfer.upload(url, targetPathAudioNew2, options2, true).then(function(result) {
      //console.log("SUCCESS: " + JSON.stringify(result.response));
      //console.log("Subió el audio 2 al servidor");
   }, function(err) {
      //console.log("ERROR: " + JSON.stringify(err));
   }, function (progress) {
   });

  }

  $scope.doRegister = function() {

    if ($scope.data.access_code_id == null || $scope.data.name == null || $scope.data.username == null || $scope.data.email == null || $scope.data.password == null || $scope.data.password2 == null || $scope.imgURI == null || $scope.tempSrc == null || $scope.tempSrc2 == null) {
      $scope.nullPopup();
    } else if ($scope.data.password != $scope.data.password2) {
        $scope.passwordError = $ionicPopup.show({
          title: 'Error Contraseña',
          template: '<p style="text-align: center !important;">Los campos Contraseña y Confirmar Contraseña no coinciden, por favor verificar.</p>',
          buttons: [
                    { text: 'Cerrar',
                      type: 'button-assertive'
                    }
                   ]
        });
    } else{

        var random_player_id = 0;
        random_player_id = Math.round(Math.random()*10000000);
        $scope.player_id = random_player_id;

        var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
        var link = 'http://www.playerpasslist.com/miabcd_ios/miabc_register_user.php';

        //Renombrar nombre archivo imagen con la ruta para subir a la BD MySQLi
        var filenameImage = $scope.imgURI;
        var filename = filenameImage.split("/").pop();
        var urlfilenameImage = "http://www.playerpasslist.com/miabc_uploads/"+filename;

        //Renombrar nombre audio 1 con la ruta para subir a la BD MySQLi
        var filenameAudio1 = $scope.tempSrc;
        var filenameA1 = filenameAudio1.split("/").pop();
        var urlfilenameAudio1 = "http://www.playerpasslist.com/miabc_uploads/"+filenameA1;

        //Renombrar nombre audio 2 con la ruta para subir a la BD MySQLi
        var filenameAudio2 = $scope.tempSrc2;
        var filenameA2 = filenameAudio2.split("/").pop();
        var urlfilenameAudio2 = "http://www.playerpasslist.com/miabc_uploads/"+filenameA2;

        $http.post(link,{
          access_code_id: $scope.data.access_code_id,
          name: $scope.data.name,
          username: $scope.data.username,
          email: $scope.data.email,
          password: $scope.data.password,
          player_id: $scope.player_id,
          sound_path: urlfilenameAudio1,
          image_path: urlfilenameImage,
          congrats_path: urlfilenameAudio2
        },{
          headers: headers
        }).then(function(response){
          // Response que traen las respuestas del archivo miabc_register_user.php
          $scope.responseUsername = response.data.mensaje1;
          $scope.variableUsername = response.data.variable1;
          $scope.responseEmail = response.data.mensaje2;
          $scope.variableEmail = response.data.variable2;
          $scope.responseAccessCodeDisp = response.data.mensaje3;
          $scope.variableAccessCodeDisp = response.data.variable3;

          if ($scope.variableUsername == 1) {
              $scope.mensaje = $ionicPopup.show({
                title: $scope.responseUsername,
                buttons: [
                        { text: 'Cerrar',
                          type: 'button-dark button-full',
                          onTap: function() {
                               return
                          }
                        }
                ]
              });
          } else if ($scope.variableEmail == 1) {
              $scope.mensaje = $ionicPopup.show({
                title: $scope.responseEmail,
                buttons: [
                        { text: 'Cerrar',
                          type: 'button-dark button-full',
                          onTap: function() {
                               return
                          }
                        }
                ]
              });
          } else if ($scope.variableAccessCodeDisp == 0) {
              $scope.mensaje = $ionicPopup.show({
                title: $scope.responseAccessCodeDisp,
                buttons: [
                        { text: 'Cerrar',
                          type: 'button-dark button-full',
                          onTap: function() {
                               return
                          }
                        }
                ]
              });
          } else if ($scope.variableAccessCodeDisp == 1) {
              //console.log('Registrado en la base de datos del servidor !');
              $scope.savedPopup();
              // Subir imagen al servidor
              $scope.subirImagenAlServidor();
              // Subir Audio 1 y 2 al servidor
              $scope.subirAudioAlServidor();
              $scope.limpiarCampos();
          }

        },function errorCallback(response) {
          //console.log(JSON.stringify(response));
          alert('No se pudo conectar al servidor, por favor revise el internet.');
        })

      // Limpiar campos
      $scope.limpiarCampos = function(){
          $scope.data.access_code_id = '';
          $scope.data.name = '';
          $scope.data.username = '';
          $scope.data.email = '';
          $scope.data.password = '';
          $scope.data.password2 = '';
          $scope.player_id = '';
          $scope.tempSrc = '';
          $scope.imgURI = '';
          $scope.tempSrc2 = '';
      }

    } // fin else

  }

})
/* --- Fin Registro Ctrl --- */


/* --- Inicio Olvidó Clave Ctrl --- */
.controller('olvidoClaveCtrl', function($scope, $ionicModal, $ionicPopup, $timeout, $http, $state, $location) {
  $scope.data = {};
  $scope.response = [];

  $scope.doForgetPassword = function() {
    var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    var link = 'http://www.playerpasslist.com/miabcd_ios/miabc_send_passw_email.php';

    $http.post(link,{
      email: $scope.data.email,
    },{
      headers: headers
    }).then(function successCallback(response){
      $scope.response = response.data.mensaje;
      $scope.variable = response.data.variable;
      //console.log('Response Mensaje: ' + $scope.response);
      //console.log('Response Variable: ' + $scope.variable);

      if ($scope.variable == 1) {
        $scope.enviado = $ionicPopup.show({
          title: $scope.response
        });
        setTimeout(function() {
          $scope.enviado.close();
          $location.path("/app/login");
        }, 4500);
      }else{
        $scope.cancelado = $ionicPopup.show({
          title: $scope.response
        });
        setTimeout(function() {
          $scope.cancelado.close();
        }, 4000);
      }

    }, function errorCallback(response) {
       alert('Error, no se pudo conectar al servidor.');
    })

  }

})
/* --- Fin Olvidó Clave Ctrl --- */


/* --- Inicio Cambiar Clave Ctrl --- */
.controller('cambiarClaveCtrl', function($scope, $ionicModal, userService, $ionicPopup, $timeout, $http, $location) {
  $scope.data = {};
  $scope.response = [];

  $scope.Username = window.localStorage.getItem('usernameProfile');

  $scope.doChangePassword = function() {
    var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    var link = 'http://www.playerpasslist.com/miabcd_ios/miabc_update_password.php';

    if ($scope.data.newpassword != $scope.data.newpassword2) {
      alert('El campo Nueva Contraseña y Confirmar Nueva Contraseña no coincide, por favor verifique bien.');
    }else{

      $http.post(link,{
        username: userService.userData.username,
        password: $scope.data.password,
        newpassword: $scope.data.newpassword,
      },{
        headers: headers
      }).then(function successCallback(response){
        $scope.response = response.data.mensaje;
        $scope.variable = response.data.variable;
        //console.log('Response Mensaje: ' + $scope.response);
        //console.log('Response Variable: ' + $scope.variable);

        if ($scope.variable == 1) {
          $scope.cambioClave = $ionicPopup.show({
            title: $scope.response
          });
          setTimeout(function() {
            $scope.cambioClave.close();
            $location.path("/app/login");
          }, 4000);
        }else{
          $scope.errorCambioClave = $ionicPopup.show({
            title: $scope.response
          });
          setTimeout(function() {
            $scope.errorCambioClave.close();
          }, 4000);
        }

      }, function errorCallback(response) {
         alert('Error, no se pudo conectar al servidor.');
      })

    }// fin else

  }

})
/* --- Fin Cambiar Clave Ctrl --- */


.controller('ProfileCtrl', function($ionicPlatform, $scope, $rootScope, $cordovaMedia, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $filter, userService, $ionicPopup, $timeout, $location, $ionicLoading, $http) {
  $scope.data = {};
  $scope.response = [];

  $scope.data.profileName = userService.userData.name;
  $scope.profileUsername = userService.userData.username;
  $scope.profileEmail = userService.userData.email;
  $scope.profileImage = userService.userData.image_path;
  $scope.profileSound = userService.userData.sound_path;
  $scope.profileCongrats = userService.userData.congrats_path;

  // Obtener el localStorage para mantener la sesión.
  var exp1 = window.localStorage.getItem('count');
  var exp2 = window.localStorage.getItem('profile');
  var exp3 = window.localStorage.getItem('nameProfile');
  var exp4 = window.localStorage.getItem('imageProfile');
  var exp5 = window.localStorage.getItem('usernameProfile');
  var exp6 = window.localStorage.getItem('emailProfile');
  var exp7 = window.localStorage.getItem('soundProfile');
  var exp8 = window.localStorage.getItem('congratsProfile');
  /*
  console.log('Valor localStorage count: ' + exp1);
  console.log('Valor localStorage profile: ' + exp2);
  console.log('Valor localStorage nameProfile: ' + exp3);
  console.log('Valor localStorage imageProfile: ' + exp4);
  console.log('Valor localStorage usernameProfile: ' + exp5);
  console.log('Valor localStorage emailProfile: ' + exp6);
  console.log('Valor localStorage soundProfile: ' + exp7);
  console.log('Valor localStorage congratsProfile: ' + exp8);

  console.log('Sonido perfil: ' + exp7);
  console.log('Sonido Felicidades: ' + exp8);
  */
  $rootScope.nameProfile = exp3;
  $scope.data.profileName = exp3;  // Para perfil personalízame.
  $scope.imageProfile = exp4;
  $rootScope.imageProfileMenu = exp4;  // Para Menú Lateral
  $scope.usernameProfile = exp5;
  $scope.emailProfile = exp6;
  $scope.soundProfile = exp7;
  $scope.congratsProfile = exp8;

  // Para reproducir audio descargado al dispositivo
  if ($scope.soundProfile != null && $scope.congratsProfile != null) {
    var nombrePerfilAudio = $scope.soundProfile.split("/").pop();
    var felicidadesAudio = $scope.congratsProfile.split("/").pop();

    //console.log('Nombre audio desde perfil: ' + nombrePerfilAudio);
    //console.log('Felicidades audio desde perfil: ' + felicidadesAudio);

    var rutaAudioNombrePerfil = "documents://" + nombrePerfilAudio;
    var rutaAudioFelicidadesPerfil = "documents://" + felicidadesAudio;

    //console.log('rutaAudioNombrePerfil: ' + rutaAudioNombrePerfil);
    //console.log('rutaAudioFelicidadesPerfil: ' + rutaAudioFelicidadesPerfil);
    $scope.rutaAudioNombrePerfil = rutaAudioNombrePerfil;
    $scope.rutaAudioFelicidadesPerfil = rutaAudioFelicidadesPerfil;
  }

  $ionicPlatform.ready(function(){
    $scope.imgURI = null;
    $scope.name_sound_path = '';
    $scope.congrats_path = '';

    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };
    $scope.showPopup = function() {
      $scope.alertPopup = $ionicPopup.show({
        title: 'Grabando audio...'
      });
    };
    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);
        var fecha = $filter('date')(new Date(),'ddMMyyyy');

        $scope.tempSrc = "documents://nombre_perfil_mod_"+fecha+random+"_ios.wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();
        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
            // Para comprobar existencia de audio en la carpeta.
            /*
            var fileAudio = $scope.tempSrc.split("/").pop();
            console.log('fileAudio: '+fileAudio);
            $cordovaFile.checkFile(cordova.file.documentsDirectory, fileAudio)
             .then(function (success) {
                 console.log("Ruta audio encontrada.");
              }, function (error) {
                 console.log("Ruta audio NO encontrada.");
            });
            */
        };
        $scope.name_sound_path = $scope.tempSrc;
    };

    $scope.recordStartCon = function(){
        $scope.tempSrc2 = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);
        var fecha = $filter('date')(new Date(),'ddMMyyyy');

        $scope.tempSrc2 = "documents://frase_felicitacion_mod_"+fecha+random+"_ios.wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc2);
        media.startRecord();
        $scope.showPopup();
        $scope.recordEndCon = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
        $scope.congrats_path = $scope.tempSrc2;
    };

    $scope.playing = function(src){
        if (src != null){
            $scope.play(src);
        }else{
            //console.log('No hay sonido!');
        }
     };

    $scope.savedPopup = function() {
      $scope.saved = $ionicPopup.show({
        title: '¡ El Perfil ha sido actualizado en la Base de Datos !'
      });
      setTimeout(function() {
        $scope.saved.close();
        $location.path("/app/inicio");
      }, 4000);
    };

    $scope.nullPopup = function() {
          $scope.nullpop = $ionicPopup.show({
            title: 'Alert',
            template: '<p style="text-align: justify !important;">Datos incompletos, por favor asegurese de seleccionar la imagen, completar los campos faltantes y grabar los sonidos del nombre del perfil y la frase de felicitaci&oacute;n.</p>',
            buttons: [{ text: 'Cerrar', type: 'button-assertive'}]
          });
    };
    $scope.helpPopupProfile = function() {
          var popupPerfil = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpProfile.html',
            buttons: [{ text: 'Cerrar', type: 'button-assertive'}]
          });
    };

    $scope.gallery = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var rutaImagen = imageData.substr(7);
            $scope.rutaImagenPerfil = rutaImagen;

           // Para personalizar el nombre de la imagen
            var currentName = imageData.replace(/^.*[\\\/]/, '');
            var fechaHoy = new Date(),
            n = fechaHoy.getTime(),
            newFileName =  "profile_mod_" + n + ".jpg";
            // Se obtiene la ruta de la imagen y se quita la imagen en .jpg
            var completePath = imageData.substr(0,imageData.lastIndexOf('/')+1);
            // Se copia la imagen con el nuevo nombre en la misma carpeta temp
            $cordovaFile.copyFile(completePath, currentName, cordova.file.tempDirectory, newFileName).then(function(success){
                 $scope.imgURI = newFileName;
                 //console.log('Ruta imagen galeria: '+$scope.imgURI);
            }, function(error){
                //console.log('Error durante la copia del archivo');
            });
            // Nueva ruta de la imagen para la vista
            $scope.pathForImage = function(imgURI) {
              if (imgURI == null) {
                return '';
              } else {
                return cordova.file.tempDirectory + imgURI;
              }
            };

        }, function(err) {
        });
    }
    $scope.takePicture = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7);
          $scope.rutaImagenPerfil = rutaImagen;

          // Para personalizar el nombre de la imagen
           var currentName = imageData.replace(/^.*[\\\/]/, '');
           var fechaHoy = new Date(),
           n = fechaHoy.getTime(),
           newFileName =  "profile_" + n + ".jpg";
           // Se obtiene la ruta de la imagen y se quita la imagen en .jpg
           var completePath = imageData.substr(0,imageData.lastIndexOf('/')+1);
           // Se copia la imagen con el nuevo nombre en la misma carpeta temp
           $cordovaFile.copyFile(completePath, currentName, cordova.file.tempDirectory, newFileName).then(function(success){
                $scope.imgURI = newFileName;
                //console.log('Ruta imagen galeria: '+$scope.imgURI);
           }, function(error){
                $scope.showAlert('Error', error.exception);
           });
           // Nueva ruta de la imagen para la vista
           $scope.pathForImage = function(imgURI) {
             if (imgURI == null) {
               return '';
             } else {
               return cordova.file.tempDirectory + imgURI;
             }
           };

        }, function(err) {
        });
    }
    $scope.editImage = function() {
      $scope.edit = $ionicPopup.show({
        title: 'Seleccione:',
        cssClass: 'popup-vertical-buttons',
        buttons: [
                { text: 'Galería',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.gallery();
                  }
                },
                { text: 'Cámara',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.takePicture();
                  }
                },
                { text: 'Cancelar',
                  type: 'button-dark button-full',
                  onTap: function() {
                       return
                  }
                }
              ]
        });
    }
 });

 // Subir foto al servidor.
 $scope.subirImagenAlServidor = function(id_app_user){
   var url = "http://www.playerpasslist.com/miabcd_ios/miabc_upload_image.php";

   var targetPath = $scope.pathForImage($scope.imgURI);
   var filename = targetPath.split("/").pop();

   var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "image/jpeg",
    params: {'directory': 'miabc_uploads', 'filename': 'filename','id_app_user': userService.userData.id_app_user}
  };

  $cordovaFileTransfer.upload(url, targetPath, options, true).then(function(result) {
     //console.log("SUCCESS: " + JSON.stringify(result.response));
     //console.log("Subió la foto al servidor");
  }, function(err) {
     //console.log("ERROR: " + JSON.stringify(err));
  }, function (progress) {
        $timeout(function () {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
        })
  });
  // Fin subir foto

 }

  // Subir Audio al servidor.
  $scope.subirAudioAlServidor = function(id_app_user){
    var url = "http://www.playerpasslist.com/miabcd_ios/miabc_upload_sound.php";

    // Audio 1
    if ($scope.tempSrc != null) {
        var targetPathAudio1 = $scope.tempSrc;
        var filenameAudio1 = targetPathAudio1.split("/").pop();
        var targetPathAudioNew1 = cordova.file.documentsDirectory + filenameAudio1;

        var options = {
         fileKey: "file",
         fileName: filenameAudio1,
         chunkedMode: false,
         mimeType: "audio/wav",
         params: {'directory': 'miabc_uploads', 'filename': 'filenameAudio1','id_app_user': userService.userData.id_app_user}
       };

       $cordovaFileTransfer.upload(url, targetPathAudioNew1, options, true).then(function(result) {
          //console.log("SUCCESS: " + JSON.stringify(result.response));
          //console.log("Subió el audio 1 al servidor");
       }, function(err) {
          //console.log("ERROR: " + JSON.stringify(err));
       }, function (progress) {
       });
    }

   // Audio 2
    if ($scope.tempSrc2 != null) {
        var targetPathAudio2 = $scope.tempSrc2;
        //console.log('targetPath audio2: '+targetPathAudio2);
        var filenameAudio2 = targetPathAudio2.split("/").pop();
        //console.log('Filename audio2: '+filenameAudio2);
        var targetPathAudioNew2 = cordova.file.documentsDirectory + filenameAudio2;

        var options2 = {
         fileKey: "file",
         fileName: filenameAudio2,
         chunkedMode: false,
         mimeType: "audio/wav",
         params: {'directory': 'miabc_uploads', 'filename': 'filenameAudio2','id_app_user': userService.userData.id_app_user}
       };

       $cordovaFileTransfer.upload(url, targetPathAudioNew2, options2, true).then(function(result) {
          //console.log("SUCCESS: " + JSON.stringify(result.response));
          //console.log("Subió el audio 2 al servidor");
       }, function(err) {
          //console.log("ERROR: " + JSON.stringify(err));
       }, function (progress) {
       });
    }
 }

  $scope.doUpdateProfile = function(){
      $scope.salvo = null;
      var src = null;

      var headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
      var link = 'http://www.playerpasslist.com/miabcd_ios/miabc_update_user.php';

      //Renombrar nombre archivo imagen con la ruta para subir a la BD MySQLi
      if ($scope.imgURI != null) {
        var filenameImage = $scope.imgURI;
        var filename = filenameImage.split("/").pop();
        var urlfilenameImage = "http://www.playerpasslist.com/miabc_uploads/"+filename;
      }

      //Renombrar nombre audio 1 con la ruta para subir a la BD MySQLi
      if ($scope.tempSrc != null) {
        var filenameAudio1 = $scope.tempSrc;
        var filenameA1 = filenameAudio1.split("/").pop();
        var urlfilenameAudio1 = "http://www.playerpasslist.com/miabc_uploads/"+filenameA1;
      }

      //Renombrar nombre audio 2 con la ruta para subir a la BD MySQLi
      if ($scope.tempSrc2 != null) {
        var filenameAudio2 = $scope.tempSrc2;
        var filenameA2 = filenameAudio2.split("/").pop();
        var urlfilenameAudio2 = "http://www.playerpasslist.com/miabc_uploads/"+filenameA2;
      }

      // Subir imagen al servidor
      if ($scope.imgURI != null) {
        $scope.subirImagenAlServidor();
      }
      // Subir Audio 1 y 2 al servidor
      if ($scope.tempSrc != null) {
        $scope.subirAudioAlServidor();
      }
      if ($scope.tempSrc2 != null) {
        $scope.subirAudioAlServidor();
      }

      $http.post(link,{
        name: $scope.data.profileName,
        username: $scope.usernameProfile,
        sound_path: urlfilenameAudio1,
        image_path: urlfilenameImage,
        congrats_path: urlfilenameAudio2
      },{
        headers: headers
      }).then(function(response){
        //console.log('Perfil actualizado en la base de datos !');
        $scope.savedPopup();

        // Para actualizar datos en el userService
        userService.setName(response.data.name);
        userService.setPicture(response.data.image_path);
        userService.setSound(response.data.sound_path);
        userService.setCongrats(response.data.congrats_path);
        /*
        console.log('Respuesta updated: ' + response.data.mensaje);
        console.log('Name updated: ' + userService.userData.name);
        console.log('Image updated: ' + userService.userData.image_path);
        console.log('Sound updated: ' + userService.userData.sound_path);
        console.log('Congrats updated: ' + userService.userData.congrats_path);
        */
        $scope.nuevaImagen = userService.userData.image_path;
        $scope.soundProfile = userService.userData.sound_path;
        $scope.congratsProfile = userService.userData.congrats_path;

        // Para asignar datos al localStorage después de actualizar el perfil.
        $rootScope.nameProfile = response.data.name;
        //console.log('rootScope actualizar nombre: ' + $rootScope.nameProfile);
        window.localStorage.removeItem('nameProfile');
        window.localStorage.removeItem('imageProfile');
        window.localStorage.removeItem('soundProfile');
        window.localStorage.removeItem('congratsProfile');
        window.localStorage.setItem('nameProfile',$rootScope.nameProfile);
        window.localStorage.setItem('imageProfile',$scope.nuevaImagen);
        window.localStorage.setItem('soundProfile',$scope.soundProfile);
        window.localStorage.setItem('congratsProfile',$scope.congratsProfile);

      },function errorCallback(response) {
        //console.log(JSON.stringify(response));
        alert('No se pudo conectar.');
      })

  }; // fin doUpdateProfile

})


/* --- Letras --- */
.controller('LettersCtrl', function($scope, Letters, $filter) {
  $scope.letters = [];
  $scope.letters = null;
  $scope.vocals  =  [];
  $scope.vocals  = null;
  $scope.items = [];
  $scope.itemsdos = [];
  $scope.temp = [];

  $scope.updateLetter = function() {
    Letters.all().then(function(letter){
      $scope.letters = letter;
      for(var i=0; i<letter.length;i++){
          $scope.items[i] = {
            'color': ('#'+Math.floor(Math.random()*16777215).toString(16)),
            'mintitle': $filter('lowercase')($scope.letters[i].title),
            'letterenglish': $scope.letters[i].english_sound
          };
        };
    });
  };
  $scope.updateLetter();

  $scope.updateVocals = function() {
    Letters.allVocals().then(function(vocal){
       $scope.vocals = vocal;
       for(var i=0; i<vocal.length;i++){
          $scope.itemsdos[i] = {
            'color': ('#'+Math.floor(Math.random()*16777215).toString(16)),
            'mintitle': $filter('lowercase')($scope.vocals[i].title),
            'letterenglish': $scope.vocals[i].english_sound
          };
        };
     });
  };
  $scope.updateVocals();

  $scope.createNewLetter = function(letter) {
    Letters.add(letter);
    $scope.updateLetter();
  };

  $scope.deleteLetter = function(letter) {
    Letters.remove(letter);
    $scope.updateLetter();
  };

  $scope.editLetter = function(origLetter, editLetter) {
    Letters.update(origMember, editMember);
    $scope.updateLetter();
  };

})

// --- Controlador Números ---
// Números del 0 al 9
.controller('NumbersCtrl10', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers10().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 10 al 19
.controller('NumbersCtrl20', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers20().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 20 al 29
.controller('NumbersCtrl30', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers30().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 30 al 39
.controller('NumbersCtrl40', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers40().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 40 al 49
.controller('NumbersCtrl50', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers50().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 50 al 59
.controller('NumbersCtrl60', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers60().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 60 al 69
.controller('NumbersCtrl70', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers70().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 70 al 79
.controller('NumbersCtrl80', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers80().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 80 al 89
.controller('NumbersCtrl90', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers90().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// Números del 90 al 100
.controller('NumbersCtrl100', function($scope, Letters, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.numbers = [];
  $scope.numbers = null;

  $scope.updateNumber = function() {
    Letters.allNumbers100().then(function(number){
       $scope.numbers = number;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.minuscula_sin_espacio = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < number.length; c++) {
           var str0 = $scope.numbers[c].content;
           var str1 = $scope.numbers[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           // Convierte cadena a minúscula y sin espacios.
           var minuscula0 = $filter('lowercase')($scope.numbers[c].content_english);
           var minuscula1 = minuscula0.replace(" ","");
           $scope.minuscula_sin_espacio[c] = minuscula1;
       }
     });
  };
  $scope.updateNumber();
  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
/* --- Fin Controlador Números --- */


// ---  Controlador Colores ---
.controller('ColoresCtrl', function($scope, Colores, Utilities, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
  $scope.colors = [];
  $scope.colors = null;
  $scope.updateColores = function() {
    Colores.allColors().then(function(color){
       $scope.colors = color;
       $scope.extraido0 = [];
       $scope.extraido1 = [];
       $scope.sintildes = [];
       $scope.minuscula = [];
       // Para extraer la 1ra letra.
       for (var c = 0; c < color.length; c++) {
           var str0 = $scope.colors[c].content;
           var str1 = $scope.colors[c].content_english;
           var res0 = str0.charAt(0);
           var res1 = str1.charAt(0);
           $scope.extraido0[c] = res0;
           $scope.extraido1[c] = res1;
           $scope.sintildes[c] = Utilities.removeAccents($scope.colors[c].content);
           // Convierte cadena a minúscula.
           var minuscula0 = $filter('lowercase')($scope.colors[c].content_english);
           $scope.minuscula[c] = minuscula0;
       }
     });
  };
  $scope.updateColores();

  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
    var deviceInformation = ionic.Platform.isIPad();
    $scope.isIPad = ionic.Platform.isIPad();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }

})
// --- Fin Colores Controlador ---

// --- Familia Controlador ---
.controller('FamiliaCtrl', function($scope, Familia, $ionicPlatform, $cordovaMedia, $timeout, $ionicPopover, $state) {
  $scope.familias = [];
  $scope.familias = null;
  // Trae los parentescos de la familia a la vista Familia.
  $scope.allFamiliaMembers = function() {
     Familia.allFamiliaMembers().then(function(familia){
       $scope.familias = familia;
     });
  };
  $timeout(function(){
    $scope.$apply(function () {
      $scope.allFamiliaMembers();
      //$state.go($state.current, {}, { reload: true });
    });
  }, 200);

  $scope.showE = function($event) {
    $scope.popover.show($event);
  };

})

.controller('FamiliaSlidesCtrl', function($scope, Familia, Words, $filter, $stateParams, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover, $timeout, $ionicPopup, $state) {
  $scope.familias = [];
  $scope.familias = null;
  $scope.familias11 = [];
  $scope.familias11 = null;
  // Trae todos los miembros de la familia al slides de la vista familiaSlides
  $scope.slidesFamilia = function() {
      Familia.allFamily().then(function(familia){
        $scope.familias = familia;
        if (familia.length > 0) {
          $scope.extraido0 = [];
          $scope.extraido1 = [];
          $scope.minuscula = [];
          // Para extraer la 1ra letra del parentesco de familia.
          for (var c = 0; c < familia.length; c++) {
              var str0 = $scope.familias[c].description;
              var str1 = $scope.familias[c].description_en;
              var res0 = str0.charAt(0);
              var res1 = str1.charAt(0);
              $scope.extraido0[c] = res0;
              $scope.extraido1[c] = res1;
              var Id = $scope.familias[c].word_id;
              // Convierte cadena a minúscula.
              var minuscula0 = $filter('lowercase')($scope.familias[c].description_en);
              $scope.minuscula[c] = minuscula0;
          }
        }
        else {
          $scope.no_se_encontro = "No hay diapositivas de la familia";
        }

      });
  };
  $scope.slidesFamilia();

  $scope.repeatDone = function() {
    $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.showE = function($event) {
    $scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
    $scope.popover.item = item;
  }


  /* --- Eliminar Familia del Slide y de la BD --- */
  $scope.eliminarFamilia = function(word_id) {
    $scope.eliminar = $ionicPopup.confirm({
      title: 'Confirmar:',
      template: '¿Desea eliminar?',
      buttons: [
                { text: 'Si',
                  type: 'button-light button-full',
                  onTap: function(e) {
                      $scope.eliminarFamiliaSeleccionada(word_id);
                  }
                },
                { text: 'No',
                  type: 'button-light button-full',
                  onTap:function(){
                      return false;
                  }
                }
              ]
      });
      $scope.popover.hide();
   };

    $scope.eliminarFamiliaSeleccionada = function(word_id) {
       $scope.getDeleteFamilia = function(word_id) {
          Familia.deleteFamilia(word_id).then(function(){
          });
          Words.deleteWord(word_id).then(function(){
          });

          /*
          // --- Revisar --- --- ----
          Familia.updateFamiliaMembers2(word_id).then(function(){
            console.log('Update Actualizado con el word_id ' + word_id);
          });
          console.log('Id Palabra familia seleccionada y eliminada: ' + word_id);
          */

       };
       $scope.getDeleteFamilia(word_id);
       $scope.eliminar.close();
       $scope.eliminadoPopup();
       // Recarga de nuevo los slides después de eliminar uno de los slides.
       $timeout(function(){
         $scope.$apply(function () {
            $scope.slidesFamilia();
         });
         $state.go($state.current, {}, { reload: true });
         //console.log('Actualizado después de haber eliminado !');
       }, 400);
    };

    $scope.eliminadoPopup = function() {
      $scope.eliminado = $ionicPopup.show({
        title: 'Familia Eliminada!'
      });
      setTimeout(function() {
        $scope.eliminado.close();
      }, 2000);
    };
  /* --- Fin Eliminar Familia --- */

})

.controller('FamiliaAddCtrl', function($scope, Familia, Words, $stateParams, $ionicPlatform, $cordovaMedia, $cordovaCamera, $ionicPopover, $ionicPopup, $location, $window, $jrCrop, $state) {
  $ionicPlatform.ready(function(){
    $scope.imgURI = null;
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };

    $scope.showPopup = function() {
        $scope.alertPopup = $ionicPopup.show({
          title: 'Grabando audio...'
        });
        $scope.alertPopup.then(function(res) {
        });
    };

    $scope.helpPopup = function() {
          $scope.alertPopup = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpFamilyAddPopup.html',
            buttons: [
                        { text: 'Cerrar',
                          type: 'button-assertive'
                        }
                     ]
          });
         $scope.alertPopup.then(function(res) {
          });
    };
    $scope.helpPopup();

    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);

        $scope.tempSrc = "documents://familia_"+random+".wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();

        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
    };

    $scope.playing = function(){
            var src = $scope.tempSrc;
            $scope.play(src);
     };

    $scope.savedPopup = function() {
      $scope.saved = $ionicPopup.show({
        title: 'Se agregó correctamente!'
      });
      setTimeout(function() {
        $scope.saved.close();
        $window.history.back();
        $state.go($state.current, {}, { reload: true });  // ----------------------
      }, 2000);
    };

    $scope.noSavedPopup = function() {
          $scope.nosaved = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Debes completar nombre del familiar, grabar audio y seleccionar foto, antes de guardar</p>',
            buttons: [
                        { text: 'Cerrar',
                          type: 'button-assertive'
                        }
                     ]
          });
    };

    $scope.nullPopup = function() {
          $scope.nullpop = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Está incompleto, debes completar el nombre del familiar, grabar audio y seleccionar la foto.</p>',
            buttons: [
                    { text: 'Cerrar',
                      type: 'button-assertive'
                    }
                  ]
          });
    };

    $scope.gallery = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7); // elimina file://
          $scope.crop(rutaImagen);
        }, function(err) {
           });
    }

    $scope.takePicture = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7); // elimina file://
          $scope.crop(rutaImagen);
        }, function(err) {
        });
    }

    $scope.crop = function(rutaImagen) {
        $jrCrop.crop({
            url: rutaImagen,
            width: 355,
            height: 216,
            title: 'Move and scale'
        }).then(function(canvas) {
            var image = canvas.toDataURL('image/jpeg',0.85);
            //Resultado imagen en base64
            $scope.imgURI = image;
        }, function() {
        });
    }

    $scope.editImage = function() {
      $scope.edit = $ionicPopup.show({
        title: 'Seleccione:',
        cssClass: 'popup-vertical-buttons',
        buttons: [
                { text: 'Galería',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.gallery();
                  }
                },
                { text: 'Cámara',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.takePicture();
                  }
                },
                { text: 'Cancelar',
                  type: 'button-dark button-full',
                  onTap: function() {
                       return
                  }
                }
              ]
        });
    };

    // Para guardar miembro de familia en la Base de datos.
    $scope.save = function(){
            var idMiembro = $scope.familias.Id;
            var miembroFamilia = $scope.familias.description;
            var nombreMiembro = $scope.familias.name;

            var src = null;
            if ($scope.salvo == true){
                $scope.noSavedPopup();
            }else if ($scope.familias.name===null || $scope.imgURI===null || $scope.tempSrc===null)		{
                $scope.nullPopup();
            }else{
                src = $scope.tempSrc;
                var str = $scope.familias.name;
                var title = str.split("-").join("");
                function toTitleCase(str){
                    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
                }
                var content = toTitleCase($scope.familias.name);
                $scope.word={
                    content: content,
                    custom_image_path: $scope.imgURI,
                    custom_sound_path: src,
                    letter_id: '',
                    title: title,
                };
                var ultimoIdWordFamilia = $scope.ultimoId.Id + 1;
                $scope.familia={
                    family_member_id: $scope.familias.Id,
                    word_id: ultimoIdWordFamilia,
                    name : title,
                    congrats_sound_path: src,
                };
                var cantidad_miembro = $scope.familias.cant;
                var cantidad_resta = cantidad_miembro - 1;
                //console.log('Cantidad restada: ' + cantidad_resta);
                //console.log('Id Miembro familia a restar: ' + $scope.familias.Id);
                // Graba en la Base de Datos
                Familia.addFamilia($scope.familia);
                // Familia.UpdateFamiliaMembers($scope.familias.Id, cantidad_resta);
                Words.addWord($scope.word);
                $scope.savedPopup();
                $scope.salvo = true;
            }

         };
   });

  // Consulta a la BD y trae el último id_word para Familia
  $scope.ultimoIdWord = function(){
        return Words.getLastIdWord().then(function(detalle){
          if (!detalle) {
            $scope.ultimoId = {
              Id: null
            };
          }else{
            $scope.ultimoId = detalle;
          }
          return detalle;
        });
  };
  $scope.ultimoIdWord().then(function(detalle) {
      $scope.ultimoId = detalle;
      var ultimoIdWordFamilia = $scope.ultimoId.Id + 1;
      //console.log('Ultimo id Word Fam.: ' + ultimoIdWordFamilia);
  });

  // Consulta a la BD y trae el miembro de la familia de acuerdo al parametro.
  $scope.getFamiliaMembers = function($stateParams) {
    Familia.getFamiliaMembers($stateParams.familiaId).then(function(familia){
      $scope.familias = familia;
      // Se extrae la 1ra letra del miembro de Familia:
      var string = $scope.familias.description;
      var resultado = string.charAt(0);
      $scope.extraido = resultado;
    });
  };
  $scope.getFamiliaMembers($stateParams);


})

/* --- Controlador familia editar --- */
.controller('FamiliaEditCtrl', function($scope, Familia, Words, $stateParams, $ionicPlatform, $cordovaMedia, $cordovaCamera, $ionicPopover, $ionicPopup, $window, $jrCrop) {
  $ionicPlatform.ready(function(){
    $scope.imgURI = null;
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };

    $scope.showPopup = function() {
        $scope.alertPopup = $ionicPopup.show({
          title: 'Grabando audio...'
        });
        $scope.alertPopup.then(function(res) {
        });
    };

    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);
        $scope.tempSrc = "documents://familia_"+random+".wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();

        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
    };

    $scope.playing = function(src){
        if ($scope.tempSrc != undefined){
            var srcdos = $scope.tempSrc;
            $scope.play(srcdos);
        }else {
            $scope.play(src);
        }
     };

    $scope.savedPopup = function() {
      $scope.saved = $ionicPopup.show({
        title: 'Se modificó correctamente!'
      });
      setTimeout(function() {
        $scope.saved.close();
        $window.history.back();
      }, 2000);
    };

    $scope.noSavedPopup = function() {
          $scope.nosaved = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Debes completar nombre del familiar, grabar audio y seleccionar foto, antes de guardar</p>',
            buttons: [
                      { text: 'Cerrar',
                        type: 'button-assertive'
                      }
                     ]
          });
    };

    $scope.gallery = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7); // elimina file://
          $scope.crop(rutaImagen);
        }, function(err) {
           });
    }

    $scope.takePicture = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7); // elimina file://
          $scope.crop(rutaImagen);
        }, function(err) {
        });
    }

    $scope.crop = function(rutaImagen) {
        $jrCrop.crop({
            url: rutaImagen,
            width: 355,
            height: 216,
            title: 'Move and scale'
        }).then(function(canvas) {
            var image = canvas.toDataURL('image/jpeg',0.85);
            //Resultado imagen en base64
            $scope.imgURI = image;
        }, function() {
        });
    }

    $scope.editImage = function() {
      $scope.edit = $ionicPopup.show({
        title: 'Seleccione:',
        cssClass: 'popup-vertical-buttons',
        buttons: [
                { text: 'Galería', type: 'button-dark button-full',
                  onTap: function() {
                       $scope.gallery();
                  }
                },
                { text: 'Cámara', type: 'button-dark button-full',
                  onTap: function() {
                       $scope.takePicture();
                  }
                },
                { text: 'Cancelar',
                  type: 'button-dark button-full',
                  onTap: function() {
                       return
                  }
                }
              ]
        });
    };

    // Para modificar y guardar miembro de familia en la Base de datos.
    $scope.save = function(){
            var idNombre = $scope.familias.word_id;
            var miembroFamilia = $scope.familias.description;
            var nombreMiembro = $scope.familias.name;
            var rutaAudio = $scope.familias.custom_sound_path;
            var src = null;
            if ($scope.salvo == true){
                $scope.noSavedPopup();
            }else{
                src = $scope.tempSrc;
                var str = $scope.familias.name;
                var title = str.split("-").join("");
                function toTitleCase(str){
                    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
                }
                var content = toTitleCase($scope.familias.name);
                // Graba en la Base de Datos lo modificado.
                if ($scope.tempSrc != '' && $scope.tempSrc != null) {
                  Familia.updateFamiliaSound(idNombre, $scope.tempSrc);
                  Familia.updateFamiliaContWord(idNombre, content, $scope.tempSrc, title);
                }
                if ($scope.imgURI != '' && $scope.imgURI != null) {
                  Familia.updateFamiliaImg(idNombre, $scope.imgURI);
                }
                Familia.updateFamiliaCont(idNombre, title);
                $scope.savedPopup();
                $scope.salvo = true;
            }
         };
   });

  // Consulta a la BD y trae el nombre del familiar de acuerdo al parametro.
  $scope.getFamiliaMembers = function($stateParams) {
    Familia.getFamilia($stateParams.familiaId).then(function(familia){
      $scope.familias = familia;
      // Se extrae la 1ra letra del nombre del Familiar:
      var string = $scope.familias.name;
      var resultado = string.charAt(0);
      $scope.extraido = resultado;
    });
  };
  $scope.getFamiliaMembers($stateParams);

})

/* --- Controlador Sonidos Simples --- */
.controller('SoundsCtrl', function($scope, Sounds) {
 $scope.simples = [];
 $scope.simples = null;
 $scope.items = [];
 $scope.allSounds = function() {
    Sounds.allSimple().then(function(simple){
      $scope.simples = simple;
      for(var i=0; i<simple.length;i++){
          $scope.items[i] = {
            'color': ('#'+Math.floor(Math.random()*16777215).toString(16))
          };
        };
    });
  };
 $scope.allSounds();
})

/* --- Controlador Sonidos Compuestos --- */
.controller('SoundsCompCtrl', function($scope, Sounds) {
 $scope.compuestos = [];
 $scope.compuestos = null;
 $scope.items = [];
 $scope.allSounds = function() {
    Sounds.allCompuestos().then(function(compuesto){
      $scope.compuestos = compuesto;
      for(var i=0; i<compuesto.length;i++){
          $scope.items[i] = {
            'color': ('#'+Math.floor(Math.random()*16777215).toString(16))
          };
        };
    });
  };
 $scope.allSounds();
})

.controller('SoundsSimpleCtrl', function($scope, Sounds, $stateParams, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
    $scope.mintitle = [];
    $ionicPlatform.ready(function(){
      $scope.play = function(src) {
          var media = $cordovaMedia.newMedia(src);
          media.play();
      }
      $scope.init = function (valor, valordos) {
          if (valordos == null){
              $scope.play('raw/'+valor+'.mp3');
          }else {
              $scope.play(valordos);
          }
      };
    })

  $scope.getArrSimple = function(value){
      Sounds.getSimpleArr(value.Id).then(function(detalle){
       $scope.sencillo = detalle;
       $scope.mintitle[0] = $filter('lowercase')($scope.sencillo[0].sound_title);
       $scope.init($scope.mintitle[0], $scope.sencillo[0].custom_sound_path);
       $scope.setE($scope.sencillo[0]);
       for(var i=0; i<detalle.length;i++){
          $scope.mintitle[i] = $filter('lowercase')($scope.sencillo[i].sound_title);
       };

      });
  };

  $scope.repeatDone = function() {
      $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };

  $scope.getSimple = function($stateParams) {
    Sounds.getSimple($stateParams.simpleId).then(function(single){
      $scope.simple = single;
      $scope.getArrSimple($scope.simple);
    });
  };
  $scope.getEditSimple = function($stateParams) {
    Sounds.getEditSimple($stateParams.sencilloId).then(function(single){
      $scope.editsimple = single;
    });
  };
  $scope.getSimple($stateParams);
  $scope.getEditSimple($stateParams);

  $scope.popover = {};
  $ionicPopover.fromTemplateUrl('templates/popupSimples.html', {
    scope: $scope,
}).then(function(popover) {
    $scope.popover = popover;
    $scope.popover.item = {};
});

  $scope.showE = function($event) {
    //$scope.popover.item = item;
    //$scope.popover.show($event);
  };
  $scope.hideE = function() {
    $scope.popover.hide();
  };
  $scope.setE = function(item){
     $scope.popover.item = item;
  }

})

.controller('SoundsCompuestoCtrl', function($scope, Sounds, $stateParams, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover) {
    $scope.mintitle = [];
    $ionicPlatform.ready(function(){
        $scope.play = function(src) {
            var media = $cordovaMedia.newMedia(src);
            media.play();
        }
        $scope.init = function (valor, valordos) {
            if (valordos == null){
                $scope.play('raw/'+valor+'.mp3');
            }else {
                $scope.play(valordos);
            }
        };
    })

  $scope.getArrCompuesto = function(value){
      Sounds.getCompArr(value.Id).then(function(detalle){
       $scope.sencillo = detalle;
       $scope.mintitle[0] = $filter('lowercase')($scope.sencillo[0].sound_title);
       $scope.init($scope.mintitle[0], $scope.sencillo[0].custom_sound_path);
       $scope.setE($scope.sencillo[0]);
       for(var i=0; i<detalle.length;i++){
          $scope.mintitle[i] = $filter('lowercase')($scope.sencillo[i].sound_title);
        };
      });
  };

  $scope.repeatDone = function() {
      $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };

  $scope.getCompuesto = function($stateParams) {
    Sounds.getCompuesto($stateParams.compuestoId).then(function(comp){
      $scope.compuesto = comp;
      $scope.getArrCompuesto($scope.compuesto);
    });
  };
  $scope.getEditCompuesto = function($stateParams) {
    Sounds.getEditCompuesto($stateParams.compId).then(function(comp){
      $scope.editcompuesto = comp;
    });
  };

  $scope.getCompuesto($stateParams);
  $scope.getEditCompuesto($stateParams);

  $scope.popover = {};
  $ionicPopover.fromTemplateUrl('templates/popupComp.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
    $scope.popover.item = {};
    });

  $scope.showE = function($event) {
    //$scope.popover.item = item;
    //$scope.popover.show($event);
  };
 $scope.hideE = function() {
    $scope.popover.hide();
  };
 $scope.setE = function(item){
     $scope.popover.item = item;
 }

})

.controller('SoundsSimpleEditCtrl', function($ionicPlatform, $cordovaMedia, $scope, $filter, Sounds, $stateParams, $ionicPopup, $cordovaFile) {
  $ionicPlatform.ready(function(){
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };

    $scope.showPopup = function() {
      $scope.alertPopup = $ionicPopup.show({
        title: 'Grabando...'
      });
    };
    $scope.helpPopup = function() {
          $scope.alertPopup = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpSoundPopup.html',
            buttons: [
                      { text: 'Cerrar',
                        type: 'button-assertive'
                      }
                    ]
          });
    };
    $scope.helpPopup();

    $scope.recordStart = function(value){
        var random = 0;
        var src = null;
        random = Math.round(Math.random()*1000000);

        $scope.tempSrc = cordova.file.dataDirectory+'custom_'+value+'_'+random+'.wav';

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();
        createFile($scope.tempSrc);

        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };

        function createFile(src){
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
                fileSystem.root.getFile(
                                    src, { create: true, exclusive: false }, getFileWin, getFileFail
                                );
                            }, getFSFail);
        }

        // getFile Success & Fail Methods
        function getFileWin(fileEntry){
        }

        function getFileFail(error){
        }

        function getFSFail(evt) {
        }
    };

     $scope.playing = function(src,srcdos){
        if (src == null){
            $scope.play(srcdos);
        }
        else{
            $scope.play(src);
        }
     };

      $scope.savedPopup = function() {
        $scope.saved = $ionicPopup.show({
          title: 'Guardado !'
        });

        setTimeout(function() {
          $scope.saved.close();
        }, 2000);
      };

      $scope.noSavedPopup = function() {
            $scope.nosaved = $ionicPopup.show({
              title: 'Mensaje',
              template: '<p style="text-align: justify !important;">Debe realizar una operaci&oacute;n antes de guardar</p>',
              buttons: [
                      { text: 'Cerrar',
                        type: 'button-assertive'
                      }
                    ]
            });
      };

      $scope.getEditSimpleValue = function(value) {
              Sounds.getEditSimple(value).then(function(single){
                $scope.editsimpleValue = single;
              });
      };
      $scope.save = function(value){
              //$scope.count = true;
              var src = null;
              if ($scope.salvo == true ){
                  $scope.noSavedPopup();
              }else{
                  src = $scope.tempSrc;
                  Sounds.updateSound(value, src);
                  //deleteFile($scope.tempSrc);
                  $scope.editsimpleRow.custom_sound_path = src;
                  $scope.savedPopup();
                  $scope.salvo = true;
               }
      };
   });

  $scope.getEditSimpleRow = function($stateParams) {
    Sounds.getEditSimple($stateParams.sencilloId).then(function(single){
      $scope.editsimpleRow = single;
      $scope.mintitle =  $filter('lowercase')($scope.editsimpleRow.sound_title);
      $scope.getEditLetter($scope.editsimpleRow);
    });
  };

  $scope.getEditLetter = function(value){
      Sounds.getSimple(value.letter_id).then(function(detalle){
           $scope.letter = detalle;
      });
  };
  $scope.getEditSimpleRow($stateParams);

})

.controller('SoundsCompEditCtrl', function($ionicPlatform, $cordovaMedia, $filter, $scope, Sounds, $stateParams, $ionicPopup, $cordovaFile) {
  $ionicPlatform.ready(function(){
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };

    $scope.showPopup = function() {
      $scope.alertPopup = $ionicPopup.show({
        title: 'Grabando...'
      });
    };

    $scope.helpPopup = function() {
          $scope.alertPopup = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpSoundPopup.html',
            buttons: [
                      { text: 'Cerrar',
                        type: 'button-assertive'
                      }
                     ]
          });
    };
    $scope.helpPopup();

    $scope.recordStart = function(value){
        var random = 0;
        var src = null;
        random = Math.round(Math.random()*1000000);

        $scope.tempSrc = cordova.file.dataDirectory + 'custom_'+value+'_'+random+'.wav';

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();
        createFile($scope.tempSrc);
        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };

        function createFile(src){
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
                fileSystem.root.getFile(
                                    src, { create: true, exclusive: false }, getFileWin, getFileFail
                                );
                            }, getFSFail);
        }

        // getFile Success & Fail Methods
        function getFileWin(fileEntry){
        }
        function getFileFail(error){
        }
        // requestFileSystem Fail Method
        function getFSFail(evt) {
        }
    };

    $scope.playing = function(src,srcdos){
        if (src == null){
            $scope.play(srcdos);
        }else{
            $scope.play(src);
        }
     };

    $scope.savedPopup = function() {
        $scope.saved = $ionicPopup.show({
          title: 'Guardado !'
        });

        setTimeout(function() {
          $scope.saved.close();
        }, 2000);
    };

    $scope.noSavedPopup = function() {
          $scope.nosaved = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Debe realizar una operaci&oacute;n antes de guardar</p>',
            buttons: [
                    { text: 'Cerrar',
                      type: 'button-assertive'

                    }
                  ]
          });
    };

    $scope.getEditCompValue = function(value) {
            Sounds.getEditCompuesto(value).then(function(comp){
              $scope.editcompValue = comp;
              //$scope.editsimpleRow.custom_sound_path  = $scope.editsimpleValue.custom_sound_path;
            });
    };

    $scope.save = function(value){
            //$scope.count = true;
            var src = null;
            if ($scope.salvo == true ){
                $scope.noSavedPopup();
            }else{
                src = $scope.tempSrc;
                Sounds.updateSound(value , src);

                //$scope.getEditSimpleValue(value);
                $scope.editcompRow.custom_sound_path = src;
                $scope.savedPopup();
                $scope.salvo = true;
             }
         };
   });

  $scope.getEditCompRow = function($stateParams) {
      Sounds.getEditCompuesto($stateParams.compId).then(function(comp){
        $scope.editcompRow = comp;
        $scope.mintitle =  $filter('lowercase')($scope.editcompRow.sound_title);
        $scope.getEditLetter($scope.editcompRow);
      });
  };

  $scope.getEditLetter = function(value){
      Sounds.getCompuesto(value.letter_id).then(function(detalle){
           $scope.letter = detalle;
      });
  };

  $scope.getEditCompRow($stateParams);

})

/* --- Controlador Completar Palabras Español --- */
.controller('completarPalabrasEspCtrl', function($scope, Words, $stateParams, $filter, $timeout, $window, $state, $http) {
 $scope.minuscula1a = [];
 $scope.minuscula2a = [];
 $scope.minuscula1b = [];
 $scope.minuscula2b = [];
 $scope.minuscula1c = [];
 $scope.minuscula2c = [];
 $scope.minuscula1d = [];
 $scope.minuscula2d = [];

// --- Utilidad para remover las tildes y las ñ ---
 $scope.removeAccents = function(source){
      var accent = [
          /[\300-\306]/g, /[\340-\346]/g, // A, a
          /[\310-\313]/g, /[\350-\353]/g, // E, e
          /[\314-\317]/g, /[\354-\357]/g, // I, i
          /[\322-\330]/g, /[\362-\370]/g, // O, o
          /[\331-\334]/g, /[\371-\374]/g, // U, u
          /[\321]/g, /[\361]/g, // N, n
          /[\307]/g, /[\347]/g, // C, c
      ],
      noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
      for (var i = 0; i < accent.length; i++){
          source = source.replace(accent[i], noaccent[i]);
      }
      return source;
  };

 $scope.palabra11 = [];  $scope.palabra11 = null;
 $scope.palabra22 = [];  $scope.palabra22 = null;
 $scope.palabra33 = [];  $scope.palabra33 = null;
 $scope.palabra44 = [];  $scope.palabra44 = null;

 $scope.completarPalabra1 = function(completarId){
   Words.allcompletarPalabrasEsp(completarId.Id).then(function(word1){
      $scope.palabra11 = word1;
      // Convierte palabra en minuscula y sin tildes para la imagen y los audios.
      for(var i=0; i<word1.length; i++){
         var minuscula = $scope.palabra11[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1a[i] = minuscula2;
         $scope.minuscula2a[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.palabra11[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2a[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2a = nombreImagen;
              //console.log('La imagen SI está');
            },
            function(error) {
              $scope.nombreImagen2a = $scope.imagenCustom;
              //console.log('La imagen NO está');
            }
         );
      };
   });
 };
 $scope.completarPalabra2 = function(completarId){
   Words.allcompletarPalabrasEsp(completarId.Id).then(function(word2){
      $scope.palabra22 = word2;
      // Convierte palabra en minuscula y sin tildes para la imagen y los audios.
      for(var i=0; i<word2.length; i++){
         var minuscula = $scope.palabra22[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1b[i] = minuscula2;
         $scope.minuscula2b[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.palabra22[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2b[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2b = nombreImagen;
              //console.log('La imagen SI está');
            },
            function(error) {
              $scope.nombreImagen2b = $scope.imagenCustom;
              //console.log('La imagen NO está');
            }
         );
      };
   });
 };
 $scope.completarPalabra3 = function(completarId){
   Words.allcompletarPalabrasEsp(completarId.Id).then(function(word3){
      $scope.palabra33 = word3;
      // Convierte palabra en minuscula y sin tildes para la imagen y los audios.
      for(var i=0; i<word3.length; i++){
         var minuscula = $scope.palabra33[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1c[i] = minuscula2;
         $scope.minuscula2c[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.palabra33[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2c[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2c = nombreImagen;
              //console.log('La imagen SI está');
            },
            function(error) {
              $scope.nombreImagen2c = $scope.imagenCustom;
              //console.log('La imagen NO está');
            }
         );
      };
   });
 };
 $scope.completarPalabra4 = function(completarId){
   Words.allcompletarPalabrasEsp(completarId.Id).then(function(word4){
      $scope.palabra44 = word4;
      // Convierte palabra en minuscula y sin tildes para la imagen y los audios.
      for(var i=0; i<word4.length; i++){
         var minuscula = $scope.palabra44[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1d[i] = minuscula2;
         $scope.minuscula2d[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.palabra44[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2d[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2d = nombreImagen;
              //console.log('La imagen SI está');
            },
            function(error) {
              $scope.nombreImagen2d = $scope.imagenCustom;
              //console.log('La imagen NO está');
            }
         );
      };
   });
 };

 // Para traer las palabras a completar de acuerdo a cada letra, en español.
 $scope.getCompletarPalabrasEsp = function($stateParams) {
   Words.getCompletarPalabras($stateParams.palabrasEspId).then(function(single){
     $scope.palabrasEspId = single;
     $scope.completarPalabra1($scope.palabrasEspId);
     $scope.completarPalabra2($scope.palabrasEspId);
     $scope.completarPalabra3($scope.palabrasEspId);
     $scope.completarPalabra4($scope.palabrasEspId);
   });
 };
 $scope.getCompletarPalabrasEsp($stateParams);


// --- Drag y Drop ---
 $scope.droppedObjects1 = [];
 $scope.droppedObjects2 = [];
 $scope.droppedObjects3 = [];
 $scope.droppedObjects4 = [];

 $scope.onDragComplete1 = function(data, evt) {
    var index = $scope.droppedObjects1.indexOf(data);
    if (index > -1) { $scope.droppedObjects1.splice(index, 1); }
 }
 $scope.onDragComplete2 = function(data, evt) {
    var index = $scope.droppedObjects2.indexOf(data);
    if (index > -1) { $scope.droppedObjects2.splice(index, 1); }
 }
 $scope.onDragComplete3 = function(data, evt) {
    var index = $scope.droppedObjects3.indexOf(data);
    if (index > -1) { $scope.droppedObjects3.splice(index, 1); }
 }
 $scope.onDragComplete4 = function(data, evt) {
    var index = $scope.droppedObjects4.indexOf(data);
    if (index > -1) { $scope.droppedObjects4.splice(index, 1); }
 }

 $scope.onDropComplete1 = function(data, evt) {
    var index = $scope.droppedObjects1.indexOf(data);
    if (index == -1){
      $scope.droppedObjects1.push(data);
      $scope.items1 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete2 = function(data, evt) {
    var index = $scope.droppedObjects2.indexOf(data);
    if (index == -1){
      $scope.droppedObjects2.push(data);
      $scope.items2 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete3 = function(data, evt) {
    var index = $scope.droppedObjects3.indexOf(data);
    if (index == -1){
      $scope.droppedObjects3.push(data);
      $scope.items3 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete4 = function(data, evt) {
    var index = $scope.droppedObjects4.indexOf(data);
    if (index == -1){
      $scope.droppedObjects4.push(data);
      $scope.items4 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 // --- Para recargar solo la Vista de Completar ---
 $scope.recargarVista = function(){
    $timeout(function(){
      $state.go($state.current, {}, { reload: true });
    }, 400);
 }

})

/* --- Controlador Completar Palabras Inglés --- */
.controller('completarPalabrasEngCtrl', function($scope, Words, $stateParams, $filter, $timeout, $window, $state, $http) {
 $scope.minuscula1a_en = [];
 $scope.minuscula2a_en = [];
 $scope.minuscula1b_en = [];
 $scope.minuscula2b_en = [];
 $scope.minuscula1c_en = [];
 $scope.minuscula2c_en = [];
 $scope.minuscula1d_en = [];
 $scope.minuscula2d_en = [];

 $scope.min_eng11 = [];
 $scope.min_eng22 = [];
 $scope.min_eng33 = [];
 $scope.min_eng44 = [];

// --- Utilidad para remover las tildes y las ñ ---
 $scope.removeAccents = function(source){
      var accent = [
          /[\300-\306]/g, /[\340-\346]/g, // A, a
          /[\310-\313]/g, /[\350-\353]/g, // E, e
          /[\314-\317]/g, /[\354-\357]/g, // I, i
          /[\322-\330]/g, /[\362-\370]/g, // O, o
          /[\331-\334]/g, /[\371-\374]/g, // U, u
          /[\321]/g, /[\361]/g, // N, n
          /[\307]/g, /[\347]/g, // C, c
      ],
      noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
      for (var i = 0; i < accent.length; i++){
          source = source.replace(accent[i], noaccent[i]);
      }
      return source;
  };

 $scope.word11 = [];  $scope.word11 = null;
 $scope.word22 = [];  $scope.word22 = null;
 $scope.word33 = [];  $scope.word33 = null;
 $scope.word44 = [];  $scope.word44 = null;

 $scope.completeWord1 = function(completarId){
   Words.allcompletarPalabrasEng(completarId.Id).then(function(word1){
      $scope.word11 = word1;
      // Convierte palabra en minuscula y sin tildes para la imagen.
      for(var i=0; i<word1.length; i++){
         var minuscula = $scope.word11[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1a_en[i] = minuscula2;
         $scope.minuscula2a_en[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.word11[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2a_en[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2a_en = nombreImagen;
            },
            function(error) {
              $scope.nombreImagen2a_en = $scope.imagenCustom;
            }
         );

         // Inglés para el sonido
         var min_eng = $scope.word11[i].content_english;
         var min_eng1 = $filter('lowercase')(min_eng);
         $scope.min_eng11[i] = min_eng1;
      };
   });
 };
 $scope.completeWord2 = function(completarId){
   Words.allcompletarPalabrasEng(completarId.Id).then(function(word2){
      $scope.word22 = word2;
      // Convierte palabra en minuscula y sin tildes para la imagen.
      for(var i=0; i<word2.length; i++){
         var minuscula = $scope.word22[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1b_en[i] = minuscula2;
         $scope.minuscula2b_en[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.word22[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2b_en[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2b_en = nombreImagen;
            },
            function(error) {
              $scope.nombreImagen2b_en = $scope.imagenCustom;
            }
         );

         // Inglés para el sonido
         var min_eng = $scope.word22[i].content_english;
         var min_eng1 = $filter('lowercase')(min_eng);
         $scope.min_eng22[i] = min_eng1;
      };
   });
 };
 $scope.completeWord3 = function(completarId){
   Words.allcompletarPalabrasEng(completarId.Id).then(function(word3){
      $scope.word33 = word3;
      // Convierte palabra en minuscula y sin tildes para la imagen.
      for(var i=0; i<word3.length; i++){
         var minuscula = $scope.word33[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1c_en[i] = minuscula2;
         $scope.minuscula2c_en[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.word33[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2c_en[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2c_en = nombreImagen;
            },
            function(error) {
              $scope.nombreImagen2c_en = $scope.imagenCustom;
            }
         );

         // Inglés para el sonido
         var min_eng = $scope.word33[i].content_english;
         var min_eng1 = $filter('lowercase')(min_eng);
         $scope.min_eng33[i] = min_eng1;
      };
   });
 };
 $scope.completeWord4 = function(completarId){
   Words.allcompletarPalabrasEng(completarId.Id).then(function(word4){
      $scope.word44 = word4;
      // Convierte palabra en minuscula y sin tildes para la imagen.
      for(var i=0; i<word4.length; i++){
         var minuscula = $scope.word44[i].title;
         var minuscula1 = $filter('lowercase')(minuscula);
         var minuscula2 = minuscula1.split(" ").join("");
         $scope.minuscula1d_en[i] = minuscula2;
         $scope.minuscula2d_en[i] = $scope.removeAccents(minuscula2);

         $scope.imagenCustom = $scope.word44[i].custom_image_path;
         var nombreImagen = "img/palabras/"+$scope.minuscula2d_en[i]+".jpg";
         $http.head("./"+nombreImagen).then(
            function(response) {
              $scope.nombreImagen2d_en = nombreImagen;
            },
            function(error) {
              $scope.nombreImagen2d_en = $scope.imagenCustom;
            }
         );

         // Inglés para el sonido
         var min_eng = $scope.word44[i].content_english;
         var min_eng1 = $filter('lowercase')(min_eng);
         $scope.min_eng44[i] = min_eng1;
      };
   });
 };

 // Para traer las palabras a completar de acuerdo a cada letra, en inglés
 $scope.getCompletarPalabrasEng = function($stateParams) {
   Words.getCompletarPalabras($stateParams.palabrasEngId).then(function(single){
     $scope.palabrasEngId = single;
     $scope.completeWord1($scope.palabrasEngId);
     $scope.completeWord2($scope.palabrasEngId);
     $scope.completeWord3($scope.palabrasEngId);
     $scope.completeWord4($scope.palabrasEngId);
   });
 };
 $scope.getCompletarPalabrasEng($stateParams);


// --- Drag y Drop ---
 $scope.droppedObjects1 = [];
 $scope.droppedObjects2 = [];
 $scope.droppedObjects3 = [];
 $scope.droppedObjects4 = [];

 $scope.onDragComplete1 = function(data, evt) {
    var index = $scope.droppedObjects1.indexOf(data);
    if (index > -1) { $scope.droppedObjects1.splice(index, 1); }
 }
 $scope.onDragComplete2 = function(data, evt) {
    var index = $scope.droppedObjects2.indexOf(data);
    if (index > -1) { $scope.droppedObjects2.splice(index, 1); }
 }
 $scope.onDragComplete3 = function(data, evt) {
    var index = $scope.droppedObjects3.indexOf(data);
    if (index > -1) { $scope.droppedObjects3.splice(index, 1); }
 }
 $scope.onDragComplete4 = function(data, evt) {
    var index = $scope.droppedObjects4.indexOf(data);
    if (index > -1) { $scope.droppedObjects4.splice(index, 1); }
 }

 $scope.onDropComplete1 = function(data, evt) {
    var index = $scope.droppedObjects1.indexOf(data);
    if (index == -1){
      $scope.droppedObjects1.push(data);
      $scope.items1 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete2 = function(data, evt) {
    var index = $scope.droppedObjects2.indexOf(data);
    if (index == -1){
      $scope.droppedObjects2.push(data);
      $scope.items2 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete3 = function(data, evt) {
    var index = $scope.droppedObjects3.indexOf(data);
    if (index == -1){
      $scope.droppedObjects3.push(data);
      $scope.items3 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 $scope.onDropComplete4 = function(data, evt) {
    var index = $scope.droppedObjects4.indexOf(data);
    if (index == -1){
      $scope.droppedObjects4.push(data);
      $scope.items4 = { 'color': '#3a37e4', 'display': 'none' };
    }
 }
 // --- Para recargar solo la Vista de Completar ---
 $scope.recargarVista = function(){
    $timeout(function(){
      $state.go($state.current, {}, { reload: true });
    }, 400);
 }

})
/* --- Fin Controlador Completar --- */


/* --- Controlador Palabras --- */
.controller('wordsCtrl', function($scope, Words) {
 $scope.palabras = [];
 $scope.palabras = null;
 $scope.items=[];
 $scope.allLetters = function() {
    Words.allLetters().then(function(word){
        $scope.palabras = word;
        for(var i=0; i<word.length; i++){
          $scope.items[i] = {
            'color':('#'+Math.floor(Math.random()*16777215).toString(16))
          };
        };
    });
  };
 $scope.allLetters();
})

.controller('wordsDetalleCtrl', function($scope, $sce, Words, $stateParams, $timeout, $filter, $ionicSlideBoxDelegate, $ionicPlatform, $cordovaMedia, $ionicPopover, $ionicPopup, $state) {
   $scope.mintitle = [];
   $scope.nodiacri = [];
   $scope.nodiacriso=[];
   $scope.papa=[];
   $ionicPlatform.ready(function(){
        $scope.play = function(src) {
            var media = $cordovaMedia.newMedia(src);
            media.play();
        }
        $scope.playing = function(src){
            $scope.play(src);
        };
        $scope.playing1 = function(src,src2){
            if (src.length > 0) {
              //console.log('1. Reproduciendo src: '+src);
              $scope.play(src);
            } else {
              //console.log('2. Reproduciendo src2: '+src2);
              $scope.play(src2);
            }
        };
    })

   $scope.removeAccents = function(source){
        var accent = [
            /[\300-\306]/g, /[\340-\346]/g, // A, a
            /[\310-\313]/g, /[\350-\353]/g, // E, e
            /[\314-\317]/g, /[\354-\357]/g, // I, i
            /[\322-\330]/g, /[\362-\370]/g, // O, o
            /[\331-\334]/g, /[\371-\374]/g, // U, u
            /[\321]/g, /[\361]/g, // N, n
            /[\307]/g, /[\347]/g, // C, c
        ],
        noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
        for (var i = 0; i < accent.length; i++){
            source = source.replace(accent[i], noaccent[i]);
        }
        return source;
    };

  $scope.getArrWords = function(value){
      Words.getWordsArr(value.Id).then(function(detalle){
       $scope.sencillo = detalle;
       $scope.extraido_en=[];
       $scope.minuscula_en=[];
       $scope.letraExtraidaIngles=[];
       $scope.letraConvertidaIngles=[];
       if (detalle.length > 0) {
           $scope.final =[];
           $scope.sound = [];
           for (var c = 0; c < detalle.length; c++) {
               var cant = 0;
               var posDash = '';
               var strSub = '';
               var str = $scope.sencillo[c].content;
               var idcapturado = $scope.sencillo[c].Id;
               var cant = (str.match(/-/g) || []).length;
               $scope.sound[c] = [];
               for (var i = 0; i < cant+1; i++) {
                   posDash = str.indexOf("-");
                       if (posDash !== -1) {
                         strSub = str.substr(0,posDash);
                         $scope.sound[c][i] = strSub;
                         str = str.replace(strSub+'-','');
                       }else{
                         strSub = str.substr(0,str.length);
                         $scope.sound[c][i] = strSub;
                       }
               }
           }
           $scope.nodiacriso[0] = $scope.removeAccents($scope.sencillo[0].sound_title);
           $scope.mintitle[0] = $filter('lowercase')($scope.nodiacriso[0]);

           $scope.setE($scope.sencillo[0]);
           for(var i=0; i<detalle.length; i++){
             // Extrae la primera letra de la palabra en inglés
              var str1 = $scope.sencillo[i].content_english;
              var res1 = str1.charAt(0);
              $scope.extraido_en[i] = res1;

              $scope.letraExtraidaIngles[i] = $scope.sencillo[i].letter_id_english;
              //console.log('Letra extraida ingles id en palabras: ' + $scope.letraExtraidaIngles[i]);

              $scope.letraConvertidaIngles[i] = $scope.sencillo[i].letter_eng;
              //console.log('Letra convertida ingles en palabras: ' + $scope.letraConvertidaIngles[i]);

              // Convierte palabra de inglés en minúscula para el sonido.
              $scope.minuscula_en[i] = $filter('lowercase')($scope.sencillo[i].content_english);

              $scope.nodiacri[i] = $scope.removeAccents($scope.sencillo[i].title);
              $scope.nodiacriso[i] = $scope.removeAccents($scope.sencillo[i].sound_title);
              $scope.mintitle[i] = $filter('lowercase')($scope.nodiacriso[i]);
              $scope.papa[i] = $scope.sencillo[i].title;
           };
       }
       else {
         $scope.no_hay_palabras = "No hay palabras";
         //$scope.agregar_nueva_palabra = "¿ Agregar nueva palabra ?";
         //$scope.crear_palabras = "<a href='#/app/addPalabra/311' class='button button-icon ion-plus-circled boton-no-hay-palabras'>&nbsp;</a>";
         //$scope.crear_palabras = "<a href='#/app/addPalabra/{{popover.item.Id}}' class='button button-icon ion-plus-circled boton-no-hay-palabras'>&nbsp;</a>";
       }
      });
  };

  $scope.repeatDone = function() {
      $ionicSlideBoxDelegate.update();
  };
  $scope.previousSlide = function() {
     $ionicSlideBoxDelegate.previous();
  };
  $scope.nextSlide = function() {
     $ionicSlideBoxDelegate.next();
  };

  // Para traer datos de letters a partir del parámetro de la letra seleccionada.
  $scope.getWords = function($stateParams) {
    Words.getWords($stateParams.palabrasId).then(function(single){
      $scope.palabras = single;
      $scope.getArrWords($scope.palabras);
    });
    //console.log('Parámetro seleccionado: ' + $stateParams.palabrasId);
  };
  $scope.getWords($stateParams);

  // Popup para Agregar Palabras en menú superior derecha
  $scope.popover = {};
  $ionicPopover.fromTemplateUrl('templates/popupWord.html', {
    scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
        $scope.popover.item = {};
    });

 $scope.showE = function($event) {
    $scope.popover.show($event);
 };
 $scope.hideE = function() {
    $scope.popover.hide();
 };
 $scope.setE = function(item){
     $scope.popover.item = item;
 };

/* --- Eliminar Palabra del Slide y de la BD --- */
$scope.eliminarPalabra = function(Id) {
  $scope.eliminar = $ionicPopup.confirm({
    title: 'Confirmar:',
    template: '¿Desea eliminar esta palabra?',
    buttons: [
              { text: 'Si',
                type: 'button-light button-full',
                onTap: function(e) {
                    $scope.eliminarPalabraSeleccionada(Id);
                }
              },
              { text: 'No',
                type: 'button-light button-full',
                onTap:function(){
                    return false;
                }
              }
            ]
    });
    $scope.popover.hide();
 };

  $scope.eliminarPalabraSeleccionada = function(Id) {
      $scope.getDeleteWord = function(Id) {
        if (Id <= 620) {
          $scope.noSePuedeEliminarPopup();
          //console.log('No se puede borrar las palabras originales');
        }else {
          Words.deleteWord(Id).then(function(){
          });
          $scope.eliminadoPopup();
          $scope.recargarView();
          //console.log('Eliminada correctamente !');
        }
      };
      $scope.getDeleteWord(Id);
      $scope.eliminar.close();
  };

  // Recarga de nuevo los slides después de eliminar palabra en uno de los slides.
  $scope.recargarView = function() {
        $timeout(function(){
          $scope.$apply(function () {
            $scope.getWords($stateParams);
          });
          $state.go($state.current, {}, { reload: true });
          //console.log('Actualizado después de eliminar palabra !');
        }, 400);
  };

  $scope.eliminadoPopup = function() {
    $scope.eliminado = $ionicPopup.show({
      title: 'Palabra Eliminada!'
    });
    setTimeout(function() {
      $scope.eliminado.close();
    }, 2000);
  };

  $scope.noSePuedeEliminarPopup = function() {
    $scope.eliminado2 = $ionicPopup.show({
      title: 'Esta palabra no puede ser eliminada!'
    });
    setTimeout(function() {
      $scope.eliminado2.close();
    }, 2000);
  };
/* --- Fin Eliminar Palabra --- */

})

.controller('wordsEditCtrl', function($ionicPlatform, $cordovaMedia, $scope, $filter, Words, Utilities, $stateParams, $ionicPopup, $cordovaCamera, $ionicSlideBoxDelegate, $location, $window, $jrCrop ) {
  $ionicPlatform.ready(function(){
    $scope.imgURI = null;
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };

    $scope.showPopup = function() {
      $scope.alertPopup = $ionicPopup.show({
        title: 'Grabando...'
      });
    };

    $scope.helpPopup = function() {
          $scope.alertPopup = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpWordsPopup.html',
            buttons: [
                    { text: 'Cerrar',
                      type: 'button-assertive'
                    }
                  ]
          });
    };
    $scope.helpPopup();

    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);

        $scope.tempSrc = "documents://custom_edit_"+random+".wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();

        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
        $scope.editWordRow.custom_sound_path = $scope.tempSrc;
    };

    $scope.playing = function(src){
            $scope.play(src);
            //console.log('Ruta src: ' + src);
    };

    $scope.savedPopup = function() {
      $scope.saved = $ionicPopup.show({
        title: 'Se modificó correctamente !'
      });
      setTimeout(function() {
        $scope.saved.close();
        $window.history.back();
      }, 2000);
    };
    $scope.noSavedPopup = function() {
        $scope.nosaved = $ionicPopup.show({
          title: 'Mensaje',
          template: '<p style="text-align: justify !important;">Debe realizar una operación antes de guardar</p>',
          buttons: [
                  { text: 'Cerrar',
                    type: 'button-assertive'
                  }
                ]
        });
    };

    $scope.gallery = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var rutaImagen = imageData.substr(7); // elimina file://
            $scope.crop(rutaImagen);
        }, function(err) {
        });
    }

    $scope.takePicture = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var rutaImagen = imageData.substr(7); // elimina file://
            $scope.crop(rutaImagen);
        }, function(err) {
        });
    }

    $scope.crop = function(rutaImagen) {
        $jrCrop.crop({
            url: rutaImagen,
            width: 355,
            height: 216,
            title: 'Move and scale'
        }).then(function(canvas) {
            var image = canvas.toDataURL('image/jpeg',0.85);
            //Resultado imagen en base64
            $scope.imgURI = image;
        }, function() {
        });
    }

    $scope.editImage = function() {
      $scope.edit = $ionicPopup.show({
        title: 'Seleccione:',
        cssClass: 'popup-vertical-buttons',
        buttons: [
                  { text: 'Galería',
                    type: 'button-dark button-full',
                    onTap: function() {
                         $scope.gallery();
                    }
                  },
                  { text: 'Cámara',
                    type: 'button-dark button-full',
                    onTap: function() {
                         $scope.takePicture();
                    }
                  },
                  { text: 'Cancelar',
                    type: 'button-dark button-full',
                    onTap: function() {
                         return
                    }
                  }
                 ]
        });
    };

    $scope.save = function(value){
            var src = null;
            if ($scope.salvo == true ){
                $scope.noSavedPopup();
            }else{
                src = $scope.tempSrc;
                if (src != '' && src != null) {
                  Words.updateWord(value, src);
                }
                if ($scope.imgURI != '' && $scope.imgURI != null) {
                  Words.updateWordImg(value, $scope.imgURI);
                }
                Words.updateWordCont(value, $scope.editWordRow.content);
                Words.updateWordContEnglish(value, $scope.editWordRow.content_english);

                $scope.editWordRow.custom_sound_path = src;
                $scope.savedPopup();
                $scope.salvo = true;
             }
         };
   });

  // Para traer la palabra a editar
  $scope.getEditPalabraRow = function($stateParams) {
      Words.getEditSimple($stateParams.palabraId).then(function(single){
        $scope.editWordRow = single;
        $scope.mintitle =  $filter('lowercase')($scope.editWordRow.title);
        $scope.mintitle_sin_tildes = Utilities.removeAccents($scope.mintitle);
        //console.log('Minuscula y sin tildes: ' + $scope.mintitle_sin_tildes);
        $scope.getEditLetter($scope.editWordRow.letter_id);
        $scope.letraExt = $scope.editWordRow.title.charAt(0);
        //console.log('Letra Ext: '+$scope.letraExt);
      });
  };
  $scope.getEditLetter = function(value){
      Words.getWords(value).then(function(detalle){
           $scope.letter = detalle;
           //console.log('Letra en letter: '+$scope.letter.title);
      });
  };
  $scope.getEditPalabraRow($stateParams);

})

.controller('wordsAddCtrl', function($ionicPlatform, $cordovaMedia, $scope, $filter, Words, Utilities, $stateParams, $ionicPopup, $cordovaCamera, $location, $window, $jrCrop ) {
  $ionicPlatform.ready(function(){
    $scope.imgURI = null;
    $scope.play = function(src) {
        var media = $cordovaMedia.newMedia(src);
        media.play();
    };
    $scope.showPopup = function() {
        $scope.alertPopup = $ionicPopup.show({
          title: 'Grabando...'
        });
    };
    $scope.helpPopup = function() {
          $scope.alertPopup = $ionicPopup.show({
            title: 'Ayuda',
            templateUrl: 'templates/helpWordsAddPopup.html',
            buttons: [
                        { text: 'Cerrar',
                          type: 'button-assertive'
                        }
                     ]
          });
    };
    $scope.helpPopup();

    $scope.recordStart = function(){
        $scope.tempSrc = null;
        var random = 0;
        random = Math.round(Math.random()*1000000);

        $scope.tempSrc = "documents://custom_word_"+random+".wav";

        var media = $cordovaMedia.newMedia($scope.tempSrc);
        media.startRecord();
        $scope.showPopup();

        $scope.salvo = false;

        $scope.recordEnd = function(){
            media.stopRecord();
            $scope.alertPopup.close();
            media.release();
        };
        $scope.editWordRow.custom_sound_path = $scope.tempSrc;
    };

    $scope.playing = function(src){
            $scope.play(src);
     };

    $scope.savedPopup = function() {
        $scope.saved = $ionicPopup.show({
          title: 'Se agregó correctamente a palabras!'
        });
        setTimeout(function() {
          $scope.saved.close();
          $window.history.back();
        }, 2000);
    };
    $scope.noSavedPopup = function() {
          $scope.nosaved = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Debes realizar una operación antes de guardar.</p>',
            buttons: [{ text: 'Cerrar', type: 'button-assertive'}]
          });
    };
    $scope.nullPopup = function() {
          $scope.nullpop = $ionicPopup.show({
            title: 'Mensaje',
            template: '<p style="text-align: justify !important;">Revise si la imagen está seleccionada, el texto y el sonido para la palabra.</p>',
            buttons: [{ text: 'Cerrar', type: 'button-assertive'}]
          });
    };
    $scope.repeatWordPopup = function() {
          $scope.nullpop = $ionicPopup.show({
            title: 'Error',
            template: '<p style="text-align: justify !important;">La palabra está repetida, por favor escriba otra palabra diferente!</p>',
            buttons: [{ text: 'Cerrar', type: 'button-assertive'}]
          });
    };

    $scope.gallery = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 600,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            var rutaImagen = imageData.substr(7);
            $scope.crop(rutaImagen);
        }, function(err) {
           });
    }

    $scope.takePicture = function() {
        $scope.imgURI = null;
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 355,
            targetHeight: 216,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
          var rutaImagen = imageData.substr(7); // elimina file://
          $scope.crop(rutaImagen);
        }, function(err){
        });
    }

    $scope.crop = function(rutaImagen) {
        $jrCrop.crop({
            url: rutaImagen,
            width: 355,
            height: 216,
            title: 'Move and scale'
        }).then(function(canvas) {
            var image = canvas.toDataURL('image/jpeg',0.85);
            //Resultado imagen en base64
            $scope.imgURI = image;
        }, function() {
        });
    }

    $scope.editImage = function() {
      $scope.edit = $ionicPopup.show({
        title: 'Seleccione:',
        cssClass: 'popup-vertical-buttons',
        buttons: [
                { text: 'Galería',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.gallery();
                  }
                },
                { text: 'Cámara',
                  type: 'button-dark button-full',
                  onTap: function() {
                       $scope.takePicture();
                  }
                },
                { text: 'Cancelar',
                  type: 'button-dark button-full',
                  onTap: function() {
                       return
                  }
                }
              ]
        });
    };

    // Función que permite reemplazar la letra por número para la letra de inglés, observar el 14 - 16.
    $scope.nuevaletraextraidaingles = [];
    $scope.cambiarLetraPorNumero = function(source){
        var letras_ingles = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'],
            num_letras_ingles = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','16','17','18','19','20','21','22','23','24','25','26','27'];
        for (var i = 0; i < letras_ingles.length; i++) {
          source = source.replace(letras_ingles[i], num_letras_ingles[i]);
        }
        return source;
    };

    // Función que permite convertir el id numérico por letras para la validación del campo de acuerdo a la letra seleccionada.
    $scope.idPorLetra = [];
    $scope.cambiarIdPorLetra = function(source){
        var num_letras = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27'],
            letras = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        for (var i = 0; i < num_letras.length; i++) {
            if (num_letras[i] == source) {
              source = source.replace(num_letras[i], letras[i]);
            }
        }
        return source;
    };

    $scope.save = function(id){
            var src = null;
            var id_letra = id;
            //console.log('id letra guardada: ' + id_letra);
            if ($scope.salvo == true ){
                $scope.noSavedPopup();
            }else if($scope.addWord.content == null || $scope.addWord.content_english == null || $scope.imgURI == null || $scope.tempSrc == null){
                $scope.nullPopup();
            }else{
              var extraerLetra = $scope.addWord.content;
              var extraerLetra1 = extraerLetra.charAt(0).toUpperCase();
              //console.log('Primera letra extraida palabra: ' + extraerLetra1);

              $scope.numPorLetra = $scope.cambiarIdPorLetra(''+id_letra+'');
              //console.log('id convertido a letra: ' + $scope.numPorLetra);

                // Validación de la palabra de acuerdo a la letra seleccionada.
                if ($scope.numPorLetra != extraerLetra1) {
                    //alert('Error, la letra debe comenzar por: ' + $scope.numPorLetra);
                    $scope.nullpop = $ionicPopup.show({
                      title: 'Error Validación',
                      template: '<p style="text-align: center !important;">Error, la palabra debe comenzar por la letra seleccionada: <b>'+$scope.numPorLetra+'</b></p>',
                      buttons: [ { text: 'Cerrar', type: 'button-assertive'}]
                    });
                }
                else {
                    src = $scope.tempSrc;
                    var str = $scope.addWord.content;
                    var str1 = $scope.addWord.content_english;
                    var title = str.split("-").join("");
                    function toTitleCase(str){
                        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
                    }
                    function toTitleCase1(str1){
                        return str1.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
                    }
                    var content = toTitleCase($scope.addWord.content);
                    var content_english = toTitleCase1($scope.addWord.content_english);
                    var letra_extraida_ingles = content_english.charAt(0).toLowerCase();
                    //console.log('letra extraida ingles: '+letra_extraida_ingles);

                    var letra_ingles_mayuscula = content_english.charAt(0).toUpperCase();
                    var letra_ingles_minuscula = content_english.charAt(0).toLowerCase();
                    var nueva_letra_may_min = letra_ingles_mayuscula + letra_ingles_minuscula;
                    //console.log('Nueva letra Mayuscula Minuscula: '+nueva_letra_may_min);

                    $scope.nuevaletraextraidaingles[0] = $scope.cambiarLetraPorNumero(letra_extraida_ingles);
                    //console.log('Nueva letra extraida ingles convertido a num.: ' + $scope.nuevaletraextraidaingles[0]);

                    $scope.word={
                        content_english: content_english,
                        letter_id_english: $scope.nuevaletraextraidaingles[0],
                        letter_eng: nueva_letra_may_min,
                        content: content,
                        custom_image_path: $scope.imgURI,
                        custom_sound_path: src,
                        letter_id: id,
                        title: title,
                    };
                    /*
                    console.log('content_english: '+ content_english);
                    console.log('letter_id_english: '+ $scope.nuevaletraextraidaingles[0]);
                    console.log('letter_eng: ' + nueva_letra_may_min);
                    console.log('content: ' + content);
                    console.log('ruta sonido: ' + src);
                    console.log('id letra: ' + id);
                    console.log('titulo: ' + title);
                    */

                    // Consultar si la palabra está repetida en la BD o no, para agregarla.
                    var palabra_a_comparar = title;
                    Words.getWordsToCompare(palabra_a_comparar).then(function(detalle){
                         $scope.palabra_a_comparar = detalle;
                         //console.log('Detalle: ' + $scope.palabra_a_comparar);
                         if ($scope.palabra_a_comparar == undefined) {
                           //console.log('La palabra no existe en la BD y se puede agregar');
                           $scope.agregarPalabra();
                         } else {
                           $scope.repeatWordPopup();
                           //console.log('Palabra repetida !');
                         }
                    });

                    $scope.agregarPalabra = function(){
                        Words.addWord($scope.word);
                        $scope.editWordRow.custom_sound_path = src;
                        $scope.savedPopup();
                        $scope.salvo = true;
                    };

                }

             }
         };
   });

  // Para editar una palabra
  $scope.getEditPalabraRow = function($stateParams) {
    Words.getEditSimple($stateParams.palabraId).then(function(single){
      $scope.editWordRow = single;
      $scope.addWord = { content: null };
      $scope.nodiacriso = Utilities.removeAccents($scope.editWordRow.sound_title);
      $scope.mintitle =  $filter('lowercase')($scope.nodiacriso);
      $scope.nodiacri = Utilities.removeAccents($scope.editWordRow.title);
      $scope.getEditLetter($scope.editWordRow);
    });
  };

  $scope.getEditLetter = function(value){
      Words.getWords(value.letter_id).then(function(detalle){
           $scope.letter = detalle;
      });
  };
  $scope.getEditPalabraRow($stateParams);

})
