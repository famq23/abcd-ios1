var db = null;
angular.module('starter', ['ionic','ngCordova','starter.services','starter.controllers','jrCrop', 'ngDraggable'])

// Para mejorar el rendimiento cuando está en producción.
.config(['$compileProvider', function ($compileProvider) {
  if (window.cordova) {
    $compileProvider.debugInfoEnabled(false);
  }
}])

.run(function($ionicPlatform, $cordovaSQLite, $jrCrop) {
  $ionicPlatform.ready(function() {
    setTimeout(function () {
      navigator.splashscreen.hide();
    }, 100);

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    db = $cordovaSQLite.openDB("abc.db");

    /* ----- Tabla Words ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS words (sound_title TEXT, image_id INTEGER, content_english TEXT, letter_id_english	INTEGER, letter_eng TEXT, Id INTEGER PRIMARY KEY AUTOINCREMENT, content TEXT, custom_image_path TEXT, custom_sound_path TEXT, letter_id INTEGER, title TEXT, level INTEGER, tword TEXT)");

    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ma','','','','',1,'ma','','',40,'Ma','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Me','','','','',2,'me','','',40,'Me','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mi','','','','',3,'mi','','',40,'Mi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mo','','','','',4,'mo','','',40,'Mo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mu','','','','',5,'mu','','',40,'Mu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pa','','','','',6,'pa','','',44,'Pa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pe','','','','',7,'pe','','',44,'Pe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pi','','','','',8,'pi','','',44,'Pi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Po','','','','',9,'po','','',44,'Po','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pu','','','','',10,'pu','','',44,'Pu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['La','','','','',11,'la','','',39,'La','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Le','','','','',12,'le','','',39,'Le','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Li','','','','',13,'li','','',39,'Li','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lo','','','','',14,'lo','','',39,'Lo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lu','','','','',15,'lu','','',39,'Lu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Na','','','','',16,'na','','',41,'Na','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ne','','','','',17,'ne','','',41,'Ne','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ni','','','','',18,'ni','','',41,'Ni','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['No','','','','',19,'no','','',41,'No','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nu','','','','',20,'nu','','',41,'Nu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Da','','','','',21,'da','','',31,'Da','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['De','','','','',22,'de','','',31,'De','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Di','','','','',23,'di','','',31,'Di','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Do','','','','',24,'do','','',31,'Do','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Du','','','','',25,'du','','',31,'Du','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sa','','','','',26,'sa','','',46,'Sa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Se','','','','',27,'se','','',46,'Se','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Si','','','','',28,'si','','',46,'Si','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['So','','','','',29,'so','','',46,'So','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Su','','','','',30,'su','','',46,'Su','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ta','','','','',31,'ta','','',47,'Ta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Te','','','','',32,'te','','',47,'Te','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ti','','','','',33,'ti','','',47,'Ti','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['To','','','','',34,'to','','',47,'To','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tu','','','','',35,'tu','','',47,'Tu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fa','','','','',36,'fa','','',33,'Fa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fe','','','','',37,'fe','','',33,'Fe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fi','','','','',38,'fi','','',33,'Fi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fo','','','','',39,'fo','','',33,'Fo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fu','','','','',40,'fu','','',33,'Fu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ba','','','','',41,'ba','','',29,'Ba','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Be','','','','',42,'be','','',29,'Be','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bi','','','','',43,'bi','','',29,'Bi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bo','','','','',44,'bo','','',29,'Bo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bu','','','','',45,'bu','','',29,'Bu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ca','','','','',46,'ca','','',30,'Ca','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ce','','','','',47,'ce','','',30,'Ce','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ci','','','','',48,'ci','','',30,'Ci','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Co','','','','',49,'co','','',30,'Co','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cu','','','','',50,'cu','','',30,'Cu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ra','','','','',51,'ra','','',45,'Ra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Re','','','','',52,'re','','',45,'Re','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ri','','','','',53,'ri','','',45,'Ri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ro','','','','',54,'ro','','',45,'Ro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ru','','','','',55,'ru','','',45,'Ru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Va','','','','',56,'va','','',49,'Va','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ve','','','','',57,'ve','','',49,'Ve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vi','','','','',58,'vi','','',49,'Vi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vo','','','','',59,'vo','','',49,'Vo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vu','','','','',60,'vu','','',49,'Vu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ga','','','','',61,'ga','','',34,'Ga','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ge','','','','',62,'ge','','',34,'Ge','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gi','','','','',63,'gi','','',34,'Gi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Go','','','','',64,'go','','',34,'Go','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gu','','','','',65,'gu','','',34,'Gu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ya','','','','',66,'ya','','',52,'Ya','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ye','','','','',67,'ye','','',52,'Ye','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yi','','','','',68,'yi','','',52,'Yi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yo','','','','',69,'yo','','',52,'Yo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yu','','','','',70,'yu','','',52,'Yu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Za','','','','',71,'za','','',53,'Za','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ze','','','','',72,'ze','','',53,'Ze','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zi','','','','',73,'zi','','',53,'Zi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zo','','','','',74,'zo','','',53,'Zo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zu','','','','',75,'zu','','',53,'Zu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Abeja',1,'Bee',2,'Bb',76,'A-be-ja','','',1,'Abeja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Abrigo',2,'Coat',3,'Cc',77,'A-bri-go','','',1,'Abrigo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Abuela',3,'Grandmother',7,'Gg',78,'A-bue-la','','',1,'Abuela','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Abuelo',4,'Grandfather',7,'Gg',79,'A-bue-lo','','',1,'Abuelo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Aguacate',5,'Avocado',1,'Aa',80,'A-gua-ca-te','','',1,'Aguacate','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Águila',6,'Eagle',5,'Ee',81,'Á-gui-la','','',1,'Águila','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Avispa',7,'Wasp',24,'Ww',82,'A-vis-pa','','',1,'Avispa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Avestruz',8,'Ostrich',16,'Oo',83,'A-ves-truz','','',1,'Avestruz','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Anaranjado',9,'Orange',16,'Oo',84,'A-na-ran-ja-do','','',1,'Anaranjado','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ancla',10,'Anchor',1,'Aa',85,'An-cla','','',1,'Áncla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Aguja',11,'Needle',14,'Nn',86,'A-gu-ja','','',1,'Aguja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ajo',12,'Garlic',7,'Gg',87,'A-jo','','',1,'Ajo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Alfombra',13,'Rug',19,'Rr',88,'Al-fom-bra','','',1,'Alfombra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Almendra',14,'Almond',1,'Aa',89,'Al-men-dra','','',1,'Almendra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ardilla',15,'Squirrel',20,'Ss',90,'Ar-di-lla','','',1,'Ardilla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Amarillo',16,'Yellow',26,'Yy',91,'A-ma-ri-llo','','',1,'Amarillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Abanico',17,'Fan',6,'Ff',92,'A-ba-ni-co','','',1,'Abanico','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Angel_es',18,'Angel',1,'Aa',93,'Án-gel','','',1,'Ángel','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Avión',19,'Plane',17,'Pp',94,'A-vi-ón','','',1,'Avión','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Astronauta',20,'Astronaut',1,'Aa',95,'As-tro-nau-ta','','',1,'Astronauta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Anillo',21,'Ring',19,'Rr',96,'A-ni-llo','','',1,'Anillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Araña',22,'Spider',20,'Ss',97,'A-ra-ña','','',1,'Araña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Azul',23,'Blue',2,'Bb',98,'A-zul','','',1,'Azul','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bufanda',24,'Scarf',20,'Ss',99,'Bu-fan-da','','',2,'Bufanda','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bloque',25,'Block',2,'Bb',100,'Blo-que','','',2,'Bloque','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bombero',26,'Firefighter',6,'Ff',101,'Bom-be-ro','','',2,'Bombero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bombilla',27,'Lightbulb',12,'Ll',102,'Bom-bi-lla','','',2,'Bombilla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bosque',28,'Forest',6,'Ff',103,'Bos-que','','',2,'Bosque','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bota',29,'Boot',2,'Bb',104,'Bo-ta','','',2,'Bota','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Botella',30,'Bottle',2,'Bb',105,'Bo-te-lla','','',2,'Botella','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Brazo',31,'Arm',1,'Aa',106,'Bra-zo','','',2,'Brazo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Asno',32,'Donkey',4,'Dd',107,'As-no','','',1,'Asno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ballena',33,'Whale',24,'Ww',108,'Ba-lle-na','','',2,'Ballena','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Balón',34,'Ball',2,'Bb',109,'Ba-lón','','',2,'Balón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bambú',35,'Bamboo',2,'Bb',110,'Bam-bú','','',2,'Bambú','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Banano',36,'Banana',2,'Bb',111,'Ba-na-no','','',2,'Banano','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bandera',37,'Flag',6,'Ff',112,'Ban-de-ra','','',2,'Bandera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Barco',38,'Boat',2,'Bb',113,'Bar-co','','',2,'Barco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Barril',39,'Barrel',2,'Bb',114,'Ba-rril','','',2,'Barril','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bate',40,'Bat',2,'Bb',115,'Ba-te','','',2,'Bate','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Baúl',41,'Chest',3,'Cc',116,'Ba-ul','','',2,'Baúl','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bebe',42,'Baby',2,'Bb',117,'Be-bé','','',2,'Bebe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Biberón',43,'Bottle',2,'Bb',118,'Bi-be-rón','','',2,'Biberón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Biblioteca',44,'Library',12,'Ll',119,'Bi-bli-o-te-ca','','',2,'Biblioteca','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Búho',45,'Owl',16,'Oo',120,'Bú-ho','','',2,'Búho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Caballo',46,'Horse',8,'Hh',121,'Ca-ba-llo','','',3,'Caballo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cable_es',47,'Cable',3,'Cc',122,'Ca-ble','','',3,'Cable','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cacao',48,'Cocoa',3,'Cc',123,'Ca-ca-o','','',3,'Cacao','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cactus_es',49,'Cactus',3,'Cc',124,'Cac-tus','','',3,'Cactus','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cama',50,'Bed',2,'Bb',125,'Ca-ma','','',3,'Cama','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Calabaza',51,'Pumpkin',17,'Pp',126,'Ca-la-ba-za','','',3,'Calabaza','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Campana',52,'Bell',2,'Bb',127,'Cam-pa-na','','',3,'Campana','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cangrejo',53,'Crab',3,'Cc',128,'Can-gre-jo','','',3,'Cangrejo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Canguro',54,'Kangaroo',11,'Kk',129,'Can-gu-ro','','',3,'Canguro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Carro',55,'Car',3,'Cc',130,'Ca-rro','','',3,'Carro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cascada',56,'Waterfall',24,'Ww',131,'Cas-ca-da','','',3,'Cascada','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cebolla',57,'Onion',16,'Oo',132,'Ce-bo-lla','','',3,'Cebolla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ciguena',58,'Stork',20,'Ss',133,'Ci-güe-ña','','',3,'Ciguena','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cinturón',59,'Belt',2,'Bb',134,'Cin-tu-rón','','',3,'Cinturón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Circo',60,'Circus',3,'Cc',135,'Cir-co','','',3,'Circo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cisne',61,'Swan',20,'Ss',136,'Cis-ne','','',3,'Cisne','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Coco',62,'Coconut',3,'Cc',137,'Co-co','','',3,'Coco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cohete',63,'Rocket',19,'Rr',138,'Co-he-te','','',3,'Cohete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Colibrí',64,'Hummingbird',8,'Hh',139,'Co-li-brí','','',3,'Colibrí','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Conejo',65,'Bunny',2,'Bb',140,'Co-ne-jo','','',3,'Conejo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cono',66,'Cone',3,'Cc',141,'Co-no','','',3,'Cono','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Corazón',67,'Heart',8,'Hh',142,'Co-ra-zón','','',3,'Corazón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Corbata',68,'Tie',21,'Tt',143,'Cor-ba-ta','','',3,'Corbata','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cucaracha',69,'Cockroach',3,'Cc',144,'Cu-ca-ra-cha','','',3,'Cucaracha','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuchillo',70,'Knife',11,'Kk',145,'Cu-chi-llo','','',3,'Cuchillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dado',71,'Dice',4,'Dd',146,'Da-do','','',4,'Dado','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dedo',72,'Finger',6,'Ff',147,'De-do','','',4,'Dedo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Delfín',73,'Dolphin',4,'Dd',148,'Del-fín','','',4,'Delfín','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Desierto',74,'Desert',4,'Dd',149,'De-si-er-to','','',4,'Desierto','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Diente',75,'Tooth',21,'Tt',150,'Di-en-te','','',4,'Diente','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dinosaurio',76,'Dinosaur',4,'Dd',151,'Di-no-sau-ri-o','','',4,'Dinosaurio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Durazno',78,'Peach',17,'Pp',152,'Du-raz-no','','',4,'Durazno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Elefante',79,'Elephant',5,'Ee',153,'E-le-fan-te','','',5,'Elefante','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Escalera',80,'Ladder',12,'Ll',154,'Es-ca-le-ra','','',5,'Escalera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Esponja',81,'Sponge',20,'Ss',155,'Es-pon-ja','','',5,'Esponja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Estatua',82,'Statue',20,'Ss',156,'Es-ta-tu-a','','',5,'Estatua','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Faro',83,'Lighthouse',12,'Ll',157,'Fa-ro','','',6,'Faro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Flecha',84,'Arrow',1,'Aa',158,'Fle-cha','','',6,'Flecha','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Flor',85,'Flower',6,'Ff',159,'Flor','','',6,'Flor','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Frambuesa',86,'Raspberry',19,'Rr',160,'Fram-bue-sa','','',6,'Frambuesa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fresa',87,'Strawberry',20,'Ss',161,'Fre-sa','','',6,'Fresa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fuego',88,'Fire',6,'Ff',162,'Fue-go','','',6,'Fuego','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fuente',89,'Fountain',6,'Ff',163,'Fu-en-te','','',6,'Fuente','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Foca',90,'Seal',20,'Ss',164,'Fo-ca','','',6,'Foca','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Falda',91,'Skirt',20,'Ss',165,'Fal-da','','',6,'Falda','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Galleta',92,'Cookie',3,'Cc',166,'Ga-lle-ta','','',7,'Galleta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gallina',93,'Hen',8,'Hh',167,'Ga-lli-na','','',7,'Gallina','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gallo',94,'Rooster',19,'Rr',168,'Ga-llo','','',7,'Gallo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gato',95,'Cat',3,'Cc',169,'Ga-to','','',7,'Gato','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gavilán',96,'Hawk',8,'Hh',170,'Ga-vi-lán','','',7,'Gavilán','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Girasol',97,'Sunflower',20,'Ss',171,'Gi-ra-sol','','',7,'Girasol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gorra',98,'Cap',3,'Cc',172,'Go-rra','','',7,'Gorra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Guante',99,'Glove',7,'Gg',173,'Gu-an-te','','',7,'Guante','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Guerrero',100,'Warrior',24,'Ww',174,'Gue-rre-ro','','',7,'Guerrero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Guitarra',101,'Guitar',7,'Gg',175,'Gui-ta-rra','','',7,'Guitarra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gusano',102,'Worm',24,'Ww',176,'Gu-sa-no','','',7,'Gusano','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hacha',103,'Axe',1,'Aa',177,'Ha-cha','','',8,'Hacha','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Helicóptero',104,'Helicopter',8,'Hh',178,'He-li-cóp-te-ro','','',8,'Helicóptero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hormiga',105,'Ant',1,'Aa',179,'Hor-mi-ga','','',8,'Hormiga','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Huevo',106,'Egg',5,'Ee',180,'Hue-vo','','',8,'Huevo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Indio',107,'Indian',9,'Ii',181,'In-di-o','','',9,'Indio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Iglesia',108,'Church',3,'Cc',182,'I-gle-si-a','','',9,'Iglesia','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jirafa',109,'Giraffe',7,'Gg',183,'Ji-ra-fa','','',10,'Jirafa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jardín',110,'Garden',7,'Gg',184,'Jar-dín','','',10,'Jardín','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jarra',111,'Jar',10,'Jj',185,'Ja-rra','','',10,'Jarra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jaula',112,'Cage',3,'Cc',186,'Ja-u-la','','',10,'Jaula','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jabón',113,'Soap',20,'Ss',187,'Ja-bón','','',10,'Jabón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Juez',114,'Judge',10,'Jj',188,'Ju-ez','','',10,'Juez','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Kiwi_es',115,'Kiwi',11,'Kk',189,'Ki-wi','','',11,'Kiwi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Labio',116,'Lip',12,'Ll',190,'La-bi-o','','',12,'Labio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lámpara',117,'Lamp',12,'Ll',191,'Lám-pa-ra','','',12,'Lámpara','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lápiz',118,'Pencil',17,'Pp',192,'Lá-piz','','',12,'Lápiz','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Látigo',119,'Whip',24,'Ww',193,'Lá-ti-go','','',12,'Látigo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Leche',120,'Milk',13,'Mm',194,'Le-che','','',12,'Leche','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lengua',121,'Tongue',21,'Tt',195,'Len-gua','','',12,'Lengua','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['León',122,'Lion',12,'Ll',196,'Le-ón','','',12,'León','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Leopardo',123,'Leopard',12,'Ll',197,'Le-o-par-do','','',12,'Leopardo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Libro',124,'Book',2,'Bb',198,'Li-bro','','',12,'Libro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Limón',125,'Lemon',12,'Ll',199,'Li-món','','',12,'Limón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Linterna',126,'Lantern',12,'Ll',200,'Lin-ter-na','','',12,'Linterna','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Llave',127,'Key',11,'Kk',201,'Lla-ve','','',12,'Llave','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lobo',128,'Wolf',24,'Ww',202,'Lo-bo','','',12,'Lobo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Loro',129,'Parrot',17,'Pp',203,'Lo-ro','','',12,'Loro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Luna',130,'Moon',13,'Mm',204,'Lu-na','','',12,'Luna','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Maíz',131,'Corn',3,'Cc',205,'Ma-íz','','',13,'Maíz','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mamá',132,'Mom',13,'Mm',206,'Ma-má','','',13,'Mamá','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Manzana',133,'Apple',1,'Aa',207,'Man-za-na','','',13,'Manzana','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mapa',134,'Map',13,'Mm',208,'Ma-pa','','',13,'Mapa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mariposa',135,'Butterfly',2,'Bb',209,'Ma-ri-po-sa','','',13,'Mariposa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Martillo',136,'Hammer',8,'Hh',210,'Mar-ti-llo','','',13,'Martillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Melon_es',137,'Melon',13,'Mm',211,'Me-lón','','',13,'Melón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mesa',138,'Table',21,'Tt',212,'Me-sa','','',13,'Mesa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mono',139,'Monkey',13,'Mm',213,'Mo-no','','',13,'Mono','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Motocicleta',140,'Motorcycle',13,'Mm',214,'Mo-to-ci-cle-ta','','',13,'Motocicleta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Naranja',141,'Orange',16,'Oo',215,'Na-ran-ja','','',14,'Naranja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nariz',142,'Nose',14,'Nn',216,'Na-riz','','',14,'Nariz','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nido',143,'Nest',14,'Nn',217,'Ni-do','','',14,'Nido','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nieve',144,'Snow',20,'Ss',218,'Nie-ve','','',14,'Nieve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nube',145,'Cloud',3,'Cc',219,'Nu-be','','',14,'Nube','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ojo',146,'Eye',5,'Ee',220,'O-jo','','',16,'Ojo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oreja',147,'Ear',5,'Ee',221,'O-re-ja','','',16,'Oreja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oruga',148,'Caterpillar',3,'Cc',222,'O-ru-ga','','',16,'Oruga','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oveja',149,'Sheep',20,'Ss',223,'O-ve-ja','','',16,'Oveja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oso',150,'Bear',2,'Bb',224,'O-so','','',16,'Oso','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Olla',151,'Pot',17,'Pp',225,'O-lla','','',16,'Olla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Olivo',152,'Olive',16,'Oo',226,'O-li-vo','','',16,'Olivo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pájaro',155,'Bird',2,'Bb',227,'Pá-ja-ro','','',17,'Pájaro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pan',156,'Bread',2,'Bb',228,'Pan','','',17,'Pan','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pantalón',157,'Pants',17,'Pp',229,'Pan-ta-lón','','',17,'Pantalón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pantera',158,'Panther',17,'Pp',230,'Pan-te-ra','','',17,'Pantera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Papa',159,'Potato',17,'Pp',231,'Pa-pa','','',17,'Papa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Papagayo',160,'Kite',11,'Kk',232,'Pa-pa-ga-yo','','',17,'Papagayo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Paracaidas',161,'Parachute',17,'Pp',233,'Pa-ra-ca-í-das','','',17,'Paracaidas','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Paraguas',162,'Umbrella',22,'Uu',234,'Pa-ra-guas','','',17,'Paraguas','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pato',163,'Duck',4,'Dd',235,'Pa-to','','',17,'Pato','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pavo',164,'Turkey',21,'Tt',236,'Pa-vo','','',17,'Pavo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Payaso',165,'Clown',3,'Cc',237,'Pa-ya-so','','',17,'Payaso','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Perro',166,'Dog',4,'Dd',238,'Pe-rro','','',17,'Perro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Piano_es',167,'Piano',17,'Pp',239,'Pi-a-no','','',17,'Piano','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Piña',168,'Pineapple',17,'Pp',240,'Pi-ña','','',17,'Piña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pincel',169,'Paintbrush',17,'Pp',241,'Pin-cel','','',17,'Pincel','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pingüino',170,'Penguin',17,'PP',242,'Pin-güi-no','','',17,'Pingüino','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pipa',171,'Pipe',17,'Pp',243,'Pi-pa','','',17,'Pipa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Piscina',172,'Pool',17,'Pp',244,'Pis-ci-na','','',17,'Piscina','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Puente',174,'Bridge',2,'Bb',245,'Pu-en-te','','',17,'Puente','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pulgas',175,'Fleas',6,'Ff',246,'Pul-gas','','',17,'Pulgas','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Radio_es',176,'Radio',19,'Rr',247,'Ra-dio','','',19,'Radio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rana',177,'Frog',6,'Ff',248,'Ra-na','','',19,'Rana','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Raqueta',178,'Racket',19,'Rr',249,'Ra-que-ta','','',19,'Raqueta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rey',181,'King',11,'Kk',250,'Re-y','','',19,'Rey','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Robot_es',182,'Robot',19,'Rr',251,'Ro-bot','','',19,'Robot','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rueda',183,'Wheel',24,'Ww',252,'Rue-da','','',19,'Rueda','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sandia',184,'Watermelon',24,'Ww',253,'San-dí-a','','',20,'Sandia','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Semáforo',185,'Traffic Light',21,'Tt',254,'Se-má-fo-ro','','',20,'Semáforo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Serpiente',186,'Snake',20,'Ss',255,'Ser-pien-te','','',20,'Serpiente','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Silla',187,'Chair',3,'Cc',256,'Si-lla','','',20,'Silla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sofa_es',188,'Sofa',20,'Ss',257,'So-fá','','',20,'Sofá','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sol',189,'Sun',20,'Ss',258,'Sol','','',20,'Sol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Telaraña',192,'Spiderweb',20,'Ss',259,'Te-la-ra-ña','','',21,'Telaraña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Teléfono',193,'Telephone',21,'Tt',260,'Te-lé-fo-no','','',21,'Teléfono','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Televisor',194,'Television',21,'Tt',261,'Te-le-vi-sor','','',21,'Televisor','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tigre',195,'Tiger',21,'Tt',262,'Ti-gre','','',21,'Tigre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tijera',196,'Scissors',20,'Ss',263,'Ti-je-ra','','',21,'Tijera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tomate',197,'Tomato',21,'Tt',264,'To-ma-te','','',21,'Tomate','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tornillo',198,'Screw',20,'Ss',265,'Tor-ni-llo','','',21,'Tornillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tortuga',199,'Turtle',21,'Tt',266,'Tor-tu-ga','','',21,'Tortuga','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Trébol',200,'Clover',3,'Cc',267,'Tré-bol','','',21,'Trébol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tren',201,'Train',21,'Tt',268,'Tren','','',21,'Tren','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uva',202,'Grape',7,'Gg',269,'U-va','','',22,'Uva','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uña',204,'Nail',14,'Nn',270,'U-ña','','',22,'Uña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Unicornio',205,'Unicorn',22,'Uu',271,'U-ni-cor-nio','','',22,'Unicornio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vaca',206,'Cow',3,'Cc',272,'Va-ca','','',23,'Vaca','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vaquero',207,'Cowboy',3,'Cc',273,'Va-que-ro','','',23,'Vaquero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vino',208,'Wine',24,'Ww',274,'Vi-no','','',23,'Vino','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Violíin',209,'Violin',23,'Vv',275,'Vio-lín','','',23,'Violíin','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Volcán',210,'Volcano',23,'Vv',276,'Vol-cán','','',23,'Volcán','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xilófono',211,'Xylophone',25,'Xx',277,'Xi-ló-fo-no','','',25,'Xilófono','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yate',212,'Yatch',26,'Yy',278,'Ya-te','','',26,'Yate','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zapato',213,'Shoe',20,'Ss',279,'Za-pa-to','','',27,'Zapato','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zorra',214,'Fox',6,'Ff',280,'Zo-rra','','',27,'Zorra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zanahoria',215,'Carrot',3,'Cc',281,'Za-na-ho-ria','','',27,'Zanahoria','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Zoologico',216,'Zoo',27,'Zz',282,'Zo-o-ló-gi-co','','',27,'Zoologico','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Balanza',217,'Balance',2,'Bb',283,'Ba-lan-za','','',2,'Balanza','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Edificio',227,'Building',2,'Bb',284,'E-di-fi-cio','','',5,'Edificio','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Espada',229,'Sword',20,'Ss',285,'Es-pa-da','','',5,'Espada','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Kilo',230,'Kilogram',11,'Kk',286,'Ki-lo','','',11,'Kilo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Papaa',232,'Dad',4,'Dd',287,'Pa-pá','','',17,'Papá','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pepino',233,'Cucumber',3,'Cc',288,'Pe-pi-no','','',17,'Pepino','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tetero',235,'Bottle',2,'Bb',289,'Te-te-ro','','',21,'Tetero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ñandu',237,'Rhea',19,'Rr',290,'Ñan-dú','','',15,'Ñandu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nene',240,'Baby',2,'Bb',291,'Ne-né','','',14,'Nene','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pera',241,'Pear',17,'Pp',292,'Pe-ra','','',17,'Pera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pollito',242,'Chick',3,'Cc',293,'Po-lli-to','','',17,'Pollito','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Taza',243,'Cup',3,'Cc',294,'Ta-za','','',21,'Taza','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tiburon',244,'Shark',20,'Ss',295,'Ti-bu-rón','','',21,'Tiburon','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ha','','','','',296,'ha','','',35,'Ha','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['He','','','','',297,'he','','',35,'He','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hi','','','','',298,'hi','','',35,'Hi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ho','','','','',299,'ho','','',35,'Ho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hu','','','','',300,'hu','','',35,'Hu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ja','','','','',301,'ja','','',37,'Ja','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Je','','','','',302,'je','','',37,'Je','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ji','','','','',303,'ji','','',37,'Ji','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jo','','','','',304,'jo','','',37,'Jo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ju','','','','',305,'ju','','',37,'Ju','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ka','','','','',306,'ka','','',38,'Ka','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ke','','','','',307,'ke','','',38,'Ke','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ki','','','','',308,'ki','','',38,'Ki','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ko','','','','',309,'ko','','',38,'Ko','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ku','','','','',310,'ku','','',38,'Ku','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Wa','','','','',311,'wa','','',50,'Wa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['We','','','','',312,'we','','',50,'We','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Wi','','','','',313,'wi','','',50,'Wi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Wo','','','','',314,'wo','','',50,'Wo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Wu','','','','',315,'wu','','',50,'Wu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bra','','','','',316,'bra','','',80,'Bra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bre','','','','',317,'bre','','',80,'Bre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bri','','','','',318,'bri','','',80,'Bri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bro','','','','',319,'bro','','',80,'Bro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bru','','','','',320,'bru','','',80,'Bru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cra','','','','',321,'cra','','',81,'Cra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cre','','','','',322,'cre','','',81,'Cre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cri','','','','',323,'cri','','',81,'Cri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cro','','','','',324,'cro','','',81,'Cro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cru','','','','',325,'cru','','',81,'Cru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dra','','','','',326,'dra','','',82,'Dra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dre','','','','',327,'dre','','',82,'Dre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dri','','','','',328,'dri','','',82,'Dri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dro','','','','',329,'dro','','',82,'Dro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dru','','','','',330,'dru','','',82,'Dru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gra','','','','',331,'gra','','',83,'Gra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gre','','','','',332,'gre','','',83,'Gre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gri','','','','',333,'gri','','',83,'Gri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gro','','','','',334,'gro','','',83,'Gro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gru','','','','',335,'gru','','',83,'Gru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pla','','','','',336,'pla','','',84,'Pla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ple','','','','',337,'ple','','',84,'Ple','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pli','','','','',338,'pli','','',84,'Pli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Plo','','','','',339,'plo','','',84,'Plo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Plu','','','','',340,'plu','','',84,'Plu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bla','','','','',341,'bla','','',85,'Bla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ble','','','','',342,'ble','','',85,'Ble','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bli','','','','',343,'bli','','',85,'Bli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Blo','','','','',344,'blo','','',85,'Blo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Blu','','','','',345,'blu','','',85,'Blu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cla','','','','',346,'cla','','',86,'Cla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cle','','','','',347,'cle','','',86,'Cle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cli','','','','',348,'cli','','',86,'Cli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Clo','','','','',349,'clo','','',86,'Clo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Clu','','','','',350,'clu','','',86,'Clu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fla','','','','',351,'fla','','',87,'Fla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fle','','','','',352,'fle','','',87,'Fle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fli','','','','',353,'fli','','',87,'Fli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Flo','','','','',354,'flo','','',87,'Flo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Flu','','','','',355,'flu','','',87,'Flu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lla','','','','',356,'lla','','',88,'Lla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lle','','','','',357,'lle','','',88,'Lle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lli','','','','',358,'lli','','',88,'Lli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Llo','','','','',359,'llo','','',88,'Llo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Llu','','','','',360,'llu','','',88,'Llu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Que','','','','',361,'que','','',89,'Que','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Qui','','','','',362,'qui','','',89,'Qui','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tra','','','','',363,'tra','','',90,'Tra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tre','','','','',364,'tre','','',90,'Tre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tri','','','','',365,'tri','','',90,'Tri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tro','','','','',366,'tro','','',90,'Tro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tru','','','','',367,'tru','','',90,'Tru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cha','','','','',368,'cha','','',91,'Cha','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Che','','','','',369,'che','','',91,'Che','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Chi','','','','',370,'chi','','',91,'Chi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cho','','','','',371,'cho','','',91,'Cho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Chu','','','','',372,'chu','','',91,'Chu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Castor',246,'Beaver',2,'Bb',373,'Cas-tor','','',3,'Castor','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Negro',247,'Black',2,'Bb',374,'Ne-gro','','',14,'Negro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Marron',248,'Brown',2,'Bb',375,'Ma-rrón','','',13,'Marrón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cabra',249,'Goat',7,'Gg',376,'Ca-bra','','',3,'Cabra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Camaleon',250,'Chameleon',3,'Cc',377,'Ca-ma-le-ón','','',3,'Camaleón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Camello',251,'Camel',3,'Cc',378,'Ca-me-llo','','',3,'Camello','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cebra',252,'Zebra',27,'Zz',379,'Ce-bra','','',3,'Cebra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Queso',253,'Cheese',3,'Cc',380,'Que-so','','',18,'Queso','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dragon_es',254,'Dragon',4,'Dd',381,'Dra-gón','','',4,'Dragón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Estrella',255,'Star',20,'Ss',382,'Es-tre-lla','','',5,'Estrella','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oro',256,'Gold',7,'Gg',383,'O-ro','','',16,'Oro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Verde',257,'Green',7,'Gg',384,'Ver-de','','',23,'Verde','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Helado',258,'Ice Cream',9,'Ii',385,'He-la-do','','',8,'Helado','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hilo',259,'Thread',21,'Tt',386,'Hi-lo','','',8,'Hilo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Istmo',260,'Isthmus',9,'Ii',387,'Ist-mo','','',9,'Istmo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Montana',261,'Mountain',13,'Mm',388,'Mon-ta-ña','','',13,'Montaña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pulpo',262,'Octopus',16,'Oo',389,'Pul-po','','',17,'Pulpo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ola',263,'Wave',24,'Ww',390,'O-la','','',16,'Ola','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Orquidea',264,'Orchid',16,'Oo',391,'Or-quí-dea','','',16,'Orquídea','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Panda_es',265,'Panda',17,'Pp',392,'Pan-da','','',17,'Panda','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tetera',267,'Teapot',21,'Tt',393,'Te-te-ra','','',21,'Tetera','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yoyo_es',268,'Yoyo',26,'Yy',394,'Yo-yo','','',26,'Yoyo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Yuca_es',269,'Yuca',26,'Yy',395,'Yu-ca','','',26,'Yuca','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fucsia',270,'Magenta',13,'Mm',396,'Fuc-sia','','',6,'Fucsia','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gua','','','','',397,'gua','','',92,'Gua','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gue','','','','',398,'güe','','',92,'Güe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gui','','','','',399,'güi','','',92,'Güi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Guo','','','','',400,'guo','','',92,'Guo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nna','','','','',401,'ña','','',42,'Ña','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nne','','','','',402,'ñe','','',42,'Ñe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nni','','','','',403,'ñi','','',42,'Ñi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nno','','','','',404,'ño','','',42,'Ño','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nnu','','','','',405,'ñu','','',42,'Ñu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ai','','','','',406,'ai','','',28,'Ai','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Au','','','','',407,'au','','',28,'Au','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ei','','','','',408,'ei','','',32,'Ei','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Eu','','','','',409,'eu','','',32,'Eu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ia','','','','',410,'ia','','',36,'Ia','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ie','','','','',411,'ie','','',36,'Ie','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Io','','','','',412,'io','','',36,'Io','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Iu','','','','',413,'iu','','',36,'Iu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oi','','','','',414,'oi','','',43,'Oi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ou','','','','',415,'ou','','',43,'Ou','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ua','','','','',416,'ua','','',48,'Ua','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ue','','','','',417,'ue','','',48,'Ue','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ui','','','','',418,'ui','','',48,'Ui','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uo','','','','',419,'uo','','',48,'Uo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Al','','','','',420,'al','','',28,'Al','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['El','','','','',421,'el','','',32,'El','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Il','','','','',422,'il','','',36,'Il','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ol','','','','',423,'ol','','',43,'Ol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ul','','','','',424,'ul','','',48,'Ul','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['An','','','','',425,'an','','',28,'An','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['En','','','','',426,'en','','',32,'En','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['In','','','','',427,'in','','',36,'In','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['On','','','','',428,'on','','',43,'On','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Un','','','','',429,'un','','',48,'Un','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['As','','','','',430,'as','','',28,'As','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Es','','','','',431,'es','','',32,'Es','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Is','','','','',432,'is','','',36,'Is','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Os','','','','',433,'os','','',43,'Os','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Us','','','','',434,'us','','',48,'Us','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Am','','','','',435,'am','','',28,'Am','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Em','','','','',436,'em','','',32,'Em','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Im','','','','',437,'im','','',36,'Im','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Om','','','','',438,'om','','',43,'Om','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Um','','','','',439,'um','','',48,'Um','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ay','','','','',440,'ay','','',28,'Ay','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ey','','','','',441,'ey','','',32,'Ey','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Oy','','','','',442,'oy','','',43,'Oy','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uy','','','','',443,'uy','','',48,'Uy','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rra','','','','',444,'rra','','',0,'Rra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rre','','','','',445,'rre','','',0,'Rre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rri','','','','',446,'rri','','',0,'Rri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rro','','','','',447,'rro','','',0,'Rro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rru','','','','',448,'rru','','',0,'Rru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ar','','','','',449,'ar','','',28,'Ar','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Er','','','','',450,'er','','',32,'Er','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ir','','','','',451,'ir','','',36,'Ir','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Or','','','','',452,'or','','',43,'Or','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ur','','','','',453,'ur','','',48,'Ur','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xa','','','','',454,'xa','','',51,'Xa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xe','','','','',455,'xe','','',51,'Xe','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xi','','','','',456,'xi','','',51,'Xi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xo','','','','',457,'xo','','',51,'Xo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Xu','','','','',458,'xu','','',51,'Xu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rra_c','','','','',459,'rra','','',120,'Rra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rre_c','','','','',460,'rre','','',120,'Rre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rri_c','','','','',461,'rri','','',120,'Rri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rro_c','','','','',462,'rro','','',120,'Rro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rru_c','','','','',463,'rru','','',120,'Rru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Iau','','','','',464,'iau','','',124,'Iau','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Iei','','','','',465,'iei','','',124,'Iei','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ioi','','','','',466,'ioi','','',124,'Ioi','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uai','','','','',467,'uai','','',125,'Uai','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uei','','','','',468,'uei','','',125,'Uei','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uau','','','','',469,'uau','','',125,'Uau','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fra','','','','',470,'fra','','',121,'Fra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fre','','','','',471,'fre','','',121,'Fre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fri','','','','',472,'fri','','',121,'Fri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fro','','','','',473,'fro','','',121,'Fro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fru','','','','',474,'fru','','',121,'Fru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gla','','','','',475,'gla','','',122,'Gla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gle','','','','',476,'gle','','',122,'Gle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gli','','','','',477,'gli','','',122,'Gli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Glo','','','','',478,'glo','','',122,'Glo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Glu','','','','',479,'glu','','',122,'Glu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gue_c','','','','',480,'gue','','',92,'Gue','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gui_c','','','','',481,'gui','','',92,'Gui','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cero',238,'Zero',27,'Zz',482,'Ce-ro','','',54,'Cero','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Uno',203,'One',16,'Oo',483,'U-no','','',55,'Uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dos',77,'Two',21,'Tt',484,'Dos','','',56,'Dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tres',245,'Three',21,'Tt',485,'Tres','','',57,'Tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuatro',220,'Four',6,'Ff',486,'Cua-tro','','',58,'Cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cinco',219,'Five',6,'Ff',487,'Cin-co','','',59,'Cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Seis',190,'Six',20,'Ss',488,'Seis','','',60,'Seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Siete',191,'Seven',20,'Ss',489,'Sie-te','','',61,'Siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ocho',153,'Eight',5,'Ee',490,'O-cho','','',62,'Ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Nueve',231,'Nine',14,'Nn',491,'Nue-ve','','',63,'Nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Diez',225,'Ten',21,'Tt',492,'Diez','','',64,'Diez','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Once',154,'Eleven',5,'Ee',493,'On-ce','','',65,'Once','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Doce',226,'Twelve',21,'Tt',494,'Do-ce','','',66,'Doce','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Trece',236,'Thirteen',21,'Tt',495,'Tre-ce','','',67,'Trece','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Catorce',218,'Fourteen',6,'Ff',496,'Ca-tor-ce','','',68,'Catorce','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Quince',234,'Fifteen',6,'Ff',497,'Quin-ce','','',69,'Quince','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dieciseis',221,'Sixteen',20,'Ss',498,'Die-ci-seis','','',70,'Dieciseis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Diecisiete',222,'Seventeen',20,'Ss',499,'Die-ci-sie-te','','',71,'Diecisiete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dieciocho',223,'Eighteen',5,'Ee',500,'Die-ci-o-cho','','',72,'Dieciocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Diecinueve',224,'Nineteen',14,'Nn',501,'Die-ci-nue-ve','','',73,'Diecinueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veinte',239,'Twenty',21,'Tt',502,'Vein-te','','',74,'Veinte','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pra','','','','',503,'pra','','',126,'Pra','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pre','','','','',504,'pre','','',126,'Pre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pri','','','','',505,'pri','','',126,'Pri','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pro','','','','',506,'pro','','',126,'Pro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Pru','','','','',507,'pru','','',126,'Pru','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tla','','','','',508,'tla','','',127,'Tla','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tle','','','','',509,'tle','','',127,'Tle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tli','','','','',510,'tli','','',127,'Tli','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tlo','','','','',511,'tlo','','',127,'Tlo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Tlu','','','','',512,'tlu','','',127,'Tlu','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Lechuga',271,'Lettuce',12,'Ll',513,'Le-chu-ga','','',12,'Lechuga','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Mora',272,'Blackberry',2,'Bb',514,'Mo-ra','','',13,'Mora','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dia',274,'Day',4,'Dd',515,'Dí-a','','',4,'Día','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Domino_es',275,'Domino',4,'Dd',516,'Do-mi-nó','','',4,'Dominó','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Eje',276,'Axle',1,'Aa',517,'E-je','','',5,'Eje','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Espejo',277,'Mirror',13,'Mm',518,'Es-pe-jo','','',5,'Espejo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ventana',278,'Window',24,'Ww',519,'Ven-ta-na','','',23,'Ventana','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Hielo',279,'Ice',9,'Ii',520,'Hie-lo','','',8,'Hielo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Gota',280,'Drop',4,'Dd',521,'Go-ta','','',7,'Gota','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Dulce',282,'Candy',3,'Cc',522,'Dul-ce','','',4,'Dulce','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Maleta',283,'Suitcase',20,'Ss',523,'Ma-le-ta','','',13,'Maleta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Rio',284,'River',19,'Rr',524,'Rí-o','','',19,'Río','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Calle',285,'Street',20,'Ss',525,'Ca-lle','','',3,'Calle','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Arbol',286,'Tree',21,'Tt',526,'Ár-bol','','',1,'Árbol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Bol',287,'Bowl',2,'Bb',527,'Bol','','',2,'Bol','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Jarron',288,'Vase',23,'Vv',528,'Ja-rrón','','',10,'Jarrón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Vela',289,'Candle',3,'Cc',529,'Ve-la','','',23,'Vela','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cierre',290,'Zipper',27,'Zz',530,'Cie-rre','','',3,'Cierre','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Estufa',291,'Stove',20,'Ss',531,'Es-tu-fa','','',5,'Estufa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintiuno',292,'Twenty One',21,'Tt',532,'Vein-ti-u-no','','',128,'Veintiuno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintidos',293,'Twenty Two',21,'Tt',533,'Vein-ti-dos','','',129,'Veintidos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintitres',294,'Twenty Three',21,'Tt',534,'Vein-ti-tres','','',130,'Veintitres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veinticuatro',295,'Twenty Four',21,'Tt',535,'Vein-ti-cua-tro','','',131,'Veinticuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veinticinco',296,'Twenty Five',21,'Tt',536,'Vein-ti-cin-co','','',132,'Veinticinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintiseis',297,'Twenty Six',21,'Tt',537,'Vein-ti-seis','','',133,'Veintiseis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintisiete',298,'Twenty Seven',21,'Tt',538,'Vein-ti-sie-te','','',134,'Veintisiete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintiocho',299,'Twenty Eight',21,'Tt',539,'Vein-ti-o-cho','','',135,'Veintiocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Veintinueve',300,'Twenty Nine',21,'Tt',540,'Vein-ti-nue-ve','','',136,'Veintinueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta',301,'Thirty',21,'Tt',541,'Trein-ta','','',137,'Treinta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y uno',302,'Thirty one',21,'Tt',542,'Trein-ta -y- u-no','','',138,'Treinta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y dos',303,'Thirty two',21,'Tt',543,'Trein-ta -y- dos','','',139,'Treinta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y tres',304,'Thirty three',21,'Tt',544,'Trein-ta -y- tres','','',140,'Treinta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y cuatro',305,'Thirty four',21,'Tt',545,'Trein-ta -y- cua-tro','','',141,'Treinta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y cinco',306,'Thirty five',21,'Tt',546,'Trein-ta -y- cin-co','','',142,'Treinta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y seis',307,'Thirty six',21,'Tt',547,'Trein-ta -y- seis','','',143,'Treinta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y siete',308,'Thirty seven',21,'Tt',548,'Trein-ta -y- sie-te','','',144,'Treinta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y ocho',309,'Thirty eight',21,'Tt',549,'Trein-ta -y- o-cho','','',145,'Treinta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Treinta y nueve',310,'Thirty nine',21,'Tt',550,'Trein-ta -y- nue-ve','','',146,'Treinta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta',311,'Forty',6,'Ff',551,'Cua-ren-ta','','',147,'Cuarenta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y uno',312,'Forty one',6,'Ff',552,'Cua-ren-ta -y- u-no','','',148,'Cuarenta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y dos',313,'Forty two',6,'Ff',553,'Cua-ren-ta -y- dos','','',149,'Cuarenta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y tres',314,'Forty three',6,'Ff',554,'Cua-ren-ta -y- tres','','',150,'Cuarenta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y cuatro',315,'Forty four',6,'Ff',555,'Cua-ren-ta -y- cua-tro','','',151,'Cuarenta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y cinco',316,'Forty five',6,'Ff',556,'Cua-ren-ta -y- cin-co','','',152,'Cuarenta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y seis',317,'Forty six',6,'Ff',557,'Cua-ren-ta -y- seis','','',153,'Cuarenta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y siete',318,'Forty seven',6,'Ff',558,'Cua-ren-ta -y- sie-te','','',154,'Cuarenta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y ocho',319,'Forty eight',6,'Ff',559,'Cua-ren-ta -y- o-cho','','',155,'Cuarenta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cuarenta y nueve',320,'Forty nine',6,'Ff',560,'Cua-ren-ta -y- nue-ve','','',156,'Cuarenta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta',321,'Fifty',6,'Ff',561,'Cin-cuen-ta','','',157,'Cincuenta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y uno',322,'Fifty one',6,'Ff',562,'Cin-cuen-ta -y- u-no','','',158,'Cincuenta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y dos',323,'Fifty two',6,'Ff',563,'Cin-cuen-ta -y- dos','','',159,'Cincuenta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y tres',324,'Fifty three',6,'Ff',564,'Cin-cuen-ta -y- tres','','',160,'Cincuenta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y cuatro',325,'Fifty four',6,'Ff',565,'Cin-cuen-ta -y- cua-tro','','',161,'Cincuenta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y cinco',326,'Fifty five',6,'Ff',566,'Cin-cuen-ta -y- cin-co','','',162,'Cincuenta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y seis',327,'Fifty six',6,'Ff',567,'Cin-cuen-ta -y- seis','','',163,'Cincuenta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y siete',328,'Fifty seven',6,'Ff',568,'Cin-cuen-ta -y- sie-te','','',164,'Cincuenta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y ocho',329,'Fifty eight',6,'Ff',569,'Cin-cuen-ta -y- o-cho','','',165,'Cincuenta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cincuenta y nueve',330,'Fifty nine',6,'Ff',570,'Cin-cuen-ta -y- nue-ve','','',166,'Cincuenta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta',331,'Sixty',20,'Ss',571,'Se-sen-ta','','',167,'Sesenta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y uno',332,'Sixty one',20,'Ss',572,'Se-sen-ta -y- u-no','','',168,'Sesenta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y dos',333,'Sixty two',20,'Ss',573,'Se-sen-ta -y- dos','','',169,'Sesenta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y tres',334,'Sixty three',20,'Ss',574,'Se-sen-ta -y- tres','','',170,'Sesenta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y cuatro',335,'Sixty four',20,'Ss',575,'Se-sen-ta -y- cua-tro','','',171,'Sesenta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y cinco',336,'Sixty five',20,'Ss',576,'Se-sen-ta -y- cin-co','','',172,'Sesenta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y seis',337,'Sixty six',20,'Ss',577,'Se-sen-ta -y- seis','','',173,'Sesenta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y siete',338,'Sixty seven',20,'Ss',578,'Se-sen-ta -y- sie-te','','',174,'Sesenta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y ocho',339,'Sixty eight',20,'Ss',579,'Se-sen-ta -y- o-cho','','',175,'Sesenta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Sesenta y nueve',340,'Sixty nine',20,'Ss',580,'Se-sen-ta -y- nue-ve','','',176,'Sesenta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta',341,'Seventy',20,'Ss',581,'Se-ten-ta','','',177,'Setenta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y uno',342,'Seventy one',20,'Ss',582,'Se-ten-ta -y- u-no','','',178,'Setenta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y dos',343,'Seventy two',20,'Ss',583,'Se-ten-ta -y- dos','','',179,'Setenta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y tres',344,'Seventy three',20,'Ss',584,'Se-ten-ta -y- tres','','',180,'Setenta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y cuatro',345,'Seventy four',20,'Ss',585,'Se-ten-ta -y- cua-tro','','',181,'Setenta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y cinco',346,'Seventy five',20,'Ss',586,'Se-ten-ta -y- cin-co','','',182,'Setenta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y seis',347,'Seventy six',20,'Ss',587,'Se-ten-ta -y- seis','','',183,'Setenta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y siete',348,'Seventy seven',20,'Ss',588,'Se-ten-ta -y- sie-te','','',184,'Setenta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y ocho',349,'Seventy eight',20,'Ss',589,'Se-ten-ta -y- o-cho','','',185,'Setenta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Setenta y nueve',350,'Seventy nine',20,'Ss',590,'Se-ten-ta -y- nue-ve','','',186,'Setenta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta',351,'Eighty',5,'Ee',591,'O-chen-ta','','',187,'Ochenta','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y uno',352,'Eighty one',5,'Ee',592,'O-chen-ta -y- u-no','','',188,'Ochenta y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y dos',353,'Eighty two',5,'Ee',593,'O-chen-ta -y- dos','','',189,'Ochenta y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y tres',354,'Eighty three',5,'Ee',594,'O-chen-ta -y- tres','','',190,'Ochenta y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y cuatro',355,'Eighty four',5,'Ee',595,'O-chen-ta -y- cua-tro','','',191,'Ochenta y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y cinco',356,'Eighty five',5,'Ee',596,'O-chen-ta -y- cin-co','','',192,'Ochenta y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y seis',357,'Eighty six',5,'Ee',597,'O-chen-ta -y- seis','','',193,'Ochenta y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y siete',358,'Eighty seven',5,'Ee',598,'O-chen-ta -y- sie-te','','',194,'Ochenta y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y ocho',359,'Eighty eight',5,'Ee',599,'O-chen-ta -y- o-cho','','',195,'Ochenta y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Ochenta y nueve',360,'Eighty nine',5,'Ee',600,'O-chen-ta -y- nue-ve','','',196,'Ochenta y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa',361,'Ninety',14,'Nn',601,'No-ven-ta','','',197,'Noventa','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y uno',362,'Ninety one',14,'Nn',602,'No-ven-ta -y- u-no','','',198,'Noventa y uno','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y dos',363,'Ninety two',14,'Nn',603,'No-ven-ta -y- dos','','',199,'Noventa y dos','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y tres',364,'Ninety three',14,'Nn',604,'No-ven-ta -y- tres','','',200,'Noventa y tres','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y cuatro',365,'Ninety four',14,'Nn',605,'No-ven-ta -y- cua-tro','','',201,'Noventa y cuatro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y cinco',366,'Ninety five',14,'Nn',606,'No-ven-ta -y- cin-co','','',202,'Noventa y cinco','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y seis',367,'Ninety six',14,'Nn',607,'No-ven-ta -y- seis','','',203,'Noventa y seis','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y siete',368,'Ninety seven',14,'Nn',608,'No-ven-ta -y- sie-te','','',204,'Noventa y siete','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y ocho',369,'Ninety eight',14,'Nn',609,'No-ven-ta -y- o-cho','','',205,'Noventa y ocho','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Noventa y nueve',370,'Ninety nine',14,'Nn',610,'No-ven-ta -y- nue-ve','','',206,'Noventa y nueve','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Cien',371,'One hundred',16,'Oo',611,'Cien','','',207,'Cien','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Amarillo',16,'Yellow',26,'Yy',612,'A-ma-ri-llo','','',208,'Amarillo','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Anaranjado',9,'Orange',16,'Oo',613,'A-na-ran-ja-do','','',209,'Anaranjado','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Azul',23,'Blue',2,'Bb',614,'A-zul','','',210,'Azul','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Fucsia',270,'Magenta',13,'Mm',615,'Fuc-sia','','',211,'Fucsia','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Marron',248,'Brown',2,'Bb',616,'Ma-rrón','','',212,'Marrón','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Negro',247,'Black',2,'Bb',617,'Ne-gro','','',213,'Negro','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Verde',257,'Green',7,'Gg',618,'Ver-de','','',214,'Verde','','NE']);
    $cordovaSQLite.execute(db, "INSERT INTO words VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", ['Wifi_es',372,'Wifi',24,'Ww',619,'Wi-fi','','',24,'Wifi','','NE']);

    /* ----- Tabla Letters ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS letters (english_sound TEXT, Id INTEGER PRIMARY KEY, content TEXT, custom_image_path TEXT, custom_sound_path TEXT, learning_id INTEGER, system_image_id INTEGER, system_sound_id INTEGER, title TEXT)");

    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',1,'Aa','','',1,'','','A']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',2,'Bb','','',1,'','','B']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',3,'Cc','','',1,'','','C']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',4,'Dd','','',1,'','','D']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',5,'Ee','','',1,'','','E']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',6,'Ff','','',1,'','','F']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',7,'Gg','','',1,'','','G']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',8,'Hh','','',1,'','','H']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',9,'Ii','','',1,'','','I']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',10,'Jj','','',1,'','','J']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',11,'Kk','','',1,'','','K']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',12,'Ll','','',1,'','','L']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',13,'Mm','','',1,'','','M']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',14,'Nn','','',1,'','','N']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',15,'Ññ','','',1,'','','Ñ']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',16,'Oo','','',1,'','','O']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',17,'Pp','','',1,'','','P']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',18,'Qq','','',1,'','','Q']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',19,'Rr','','',1,'','','R']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',20,'Ss','','',1,'','','S']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',21,'Tt','','',1,'','','T']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',22,'Uu','','',1,'','','U']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',23,'Vv','','',1,'','','V']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',24,'Ww','','',1,'','','W']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',25,'Xx','','',1,'','','X']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',26,'Yy','','',1,'','','Y']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',27,'Zz','','',1,'','','Z']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',28,'Aa','','',2,'','','A']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',29,'Bb','','',2,'','','B']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',30,'Cc','','',2,'','','C']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',31,'Dd','','',2,'','','D']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',32,'Ee','','',2,'','','E']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',33,'Ff','','',2,'','','F']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',34,'Gg','','',2,'','','G']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',35,'Hh','','',2,'','','H']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',36,'Ii','','',2,'','','I']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',37,'Jj','','',2,'','','J']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',38,'Kk','','',2,'','','K']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',39,'Ll','','',2,'','','L']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',40,'Mm','','',2,'','','M']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',41,'Nn','','',2,'','','N']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',42,'Ññ','','',2,'','','Ñ']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',43,'Oo','','',2,'','','O']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',44,'Pp','','',2,'','','P']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',45,'Rr','','',2,'','','R']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',46,'Ss','','',2,'','','S']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',47,'Tt','','',2,'','','T']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',48,'Uu','','',2,'','','U']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',49,'Vv','','',2,'','','V']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',50,'Ww','','',2,'','','W']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',51,'Xx','','',2,'','','X']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',52,'Yy','','',2,'','','Y']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',53,'Zz','','',2,'','','Z']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',54,'Cero','','',3,'','','cero']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',55,'Uno','','',3,'','','uno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',56,'Dos','','',3,'','','dos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',57,'Tres','','',3,'','','tres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',58,'Cuatro','','',3,'','','cuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',59,'Cinco','','',3,'','','cinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',60,'Seis','','',3,'','','seis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',61,'Siete','','',3,'','','siete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',62,'Ocho','','',3,'','','ocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',63,'Nueve','','',3,'','','nueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',64,'Diez','','',3,'','','diez']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',65,'Once','','',3,'','','once']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',66,'Doce','','',3,'','','doce']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',67,'Trece','','',3,'','','trece']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',68,'Catorce','','',3,'','','catorce']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',69,'Quince','','',3,'','','quince']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',70,'Dieciseis','','',3,'','','dieciseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',71,'Diecisiete','','',3,'','','diecisiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',72,'Dieciocho','','',3,'','','dieciocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',73,'Diecinueve','','',3,'','','diecinueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',74,'Veinte','','',3,'','','veinte']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['a_en',75,'Aa','','',4,'','','A']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['e_en',76,'Ee','','',4,'','','E']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['i_en',77,'Ii','','',4,'','','I']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['o_en',78,'Oo','','',4,'','','O']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['u_en',79,'Uu','','',4,'','','U']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',80,'Br br','','',5,'','','Br']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',81,'Cr cr','','',5,'','','Cr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',82,'Dr dr','','',5,'','','Dr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',83,'Gr gr','','',5,'','','Gr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',84,'Pl pl','','',5,'','','Pl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',85,'Bl bl','','',5,'','','Bl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',86,'Cl cl','','',5,'','','Cl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',87,'Fl fl','','',5,'','','Fl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',88,'Ll ll','','',5,'','','Ll']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',89,'Qu qu','','',5,'','','Qu']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',90,'Tr tr','','',5,'','','Tr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',91,'Ch ch','','',5,'','','Ch']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',92,'Gu gu','','',5,'','','Gu']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['a_en',93,'Aa','','',6,'','','A']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['b_en',94,'Bb','','',6,'','','B']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['c_en',95,'Cc','','',6,'','','C']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['d_en',96,'Dd','','',6,'','','D']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['e_en',97,'Ee','','',6,'','','E']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['f_en',98,'Ff','','',6,'','','F']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['g_en',99,'Gg','','',6,'','','G']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['h_en',100,'Hh','','',6,'','','H']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['i_en',101,'Ii','','',6,'','','I']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['j_en',102,'Jj','','',6,'','','J']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['k_en',103,'Kk','','',6,'','','K']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['l_en',104,'Ll','','',6,'','','L']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['m_en',105,'Mm','','',6,'','','M']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['n_en',106,'Nn','','',6,'','','N']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['nn_en',107,'Ññ','','',6,'','','Ñ']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['o_en',108,'Oo','','',6,'','','O']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['p_en',109,'Pp','','',6,'','','P']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['q_en',110,'Qq','','',6,'','','Q']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['r_en',111,'Rr','','',6,'','','R']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['s_en',112,'Ss','','',6,'','','S']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['t_en',113,'Tt','','',6,'','','T']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['u_en',114,'Uu','','',6,'','','U']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['v_en',115,'Vv','','',6,'','','V']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['w_en',116,'Ww','','',6,'','','W']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['x_en',117,'Xx','','',6,'','','X']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['y_en',118,'Yy','','',6,'','','Y']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['z_en',119,'Zz','','',6,'','','Z']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',120,'Rr rr','','',5,'','','Rr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',121,'Fr fr','','',5,'','','Fr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',122,'Gl gl','','',5,'','','Gl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',124,'I i','','',5,'','','I']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',125,'U u','','',5,'','','U']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',126,'Pr pr','','',5,'','','Pr']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',127,'Tl tl','','',5,'','','Tl']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',128,'Veintiuno','','',3,'','','veintiuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',129,'Veintidos','','',3,'','','veintidos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',130,'Veintitres','','',3,'','','veintitres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',131,'Veinticuatro','','',3,'','','veinticuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',132,'Veinticinco','','',3,'','','veinticinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',133,'Veintiseis','','',3,'','','veintiseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',134,'Veintisiete','','',3,'','','veintisiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',135,'Veintiocho','','',3,'','','veintiocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',136,'Veintinueve','','',3,'','','veintinueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',137,'Treinta','','',3,'','','treinta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',138,'Treinta y uno','','',3,'','','treintayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',139,'Treinta y dos','','',3,'','','treintaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',140,'Treinta y tres','','',3,'','','treintaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',141,'Treinta y cuatro','','',3,'','','treintaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',142,'Treinta y cinco','','',3,'','','treintaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',143,'Treinta y seis','','',3,'','','treintayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',144,'Treinta y siete','','',3,'','','treintaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',145,'Treinta y ocho','','',3,'','','treintayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',146,'Treinta y nueve','','',3,'','','treintaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',147,'Cuarenta','','',3,'','','cuarenta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',148,'Cuarenta y uno','','',3,'','','cuarentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',149,'Cuarenta y dos','','',3,'','','cuarentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',150,'Cuarenta y tres','','',3,'','','cuarentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',151,'Cuarenta y cuatro','','',3,'','','cuarentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',152,'Cuarenta y cinco','','',3,'','','cuarentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',153,'Cuarenta y seis','','',3,'','','cuarentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',154,'Cuarenta y siete','','',3,'','','cuarentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',155,'Cuarenta y ocho','','',3,'','','cuarentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',156,'Cuarenta y nueve','','',3,'','','cuarentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',157,'Cincuenta','','',3,'','','cincuenta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',158,'Cincuenta y uno','','',3,'','','cincuentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',159,'Cincuenta y dos','','',3,'','','cincuentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',160,'Cincuenta y tres','','',3,'','','cincuentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',161,'Cincuenta y cuatro','','',3,'','','cincuentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',162,'Cincuenta y cinco','','',3,'','','cincuentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',163,'Cincuenta y seis','','',3,'','','cincuentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',164,'Cincuenta y siete','','',3,'','','cincuentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',165,'Cincuenta y ocho','','',3,'','','cincuentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',166,'Cincuenta y nueve','','',3,'','','cincuentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',167,'Sesenta','','',3,'','','sesenta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',168,'Sesenta y uno','','',3,'','','sesentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',169,'Sesenta y dos','','',3,'','','sesentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',170,'Sesenta y tres','','',3,'','','sesentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',171,'Sesenta y cuatro','','',3,'','','sesentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',172,'Sesenta y cinco','','',3,'','','sesentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',173,'Sesenta y seis','','',3,'','','sesentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',174,'Sesenta y siete','','',3,'','','sesentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',175,'Sesenta y ocho','','',3,'','','sesentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',176,'Sesenta y nueve','','',3,'','','sesentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',177,'Setenta','','',3,'','','setenta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',178,'Setenta y uno','','',3,'','','setentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',179,'Setenta y dos','','',3,'','','setentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',180,'Setenta y tres','','',3,'','','setentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',181,'Setenta y cuatro','','',3,'','','setentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',182,'Setenta y cinco','','',3,'','','setentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',183,'Setenta y seis','','',3,'','','setentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',184,'Setenta y siete','','',3,'','','setentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',185,'Setenta y ocho','','',3,'','','setentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',186,'Setenta y nueve','','',3,'','','setentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',187,'Ochenta','','',3,'','','ochenta']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',188,'Ochenta y uno','','',3,'','','ochentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',189,'Ochenta y dos','','',3,'','','ochentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',190,'Ochenta y tres','','',3,'','','ochentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',191,'Ochenta y cuatro','','',3,'','','ochentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',192,'Ochenta y cinco','','',3,'','','ochentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',193,'Ochenta y seis','','',3,'','','ochentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',194,'Ochenta y siete','','',3,'','','ochentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',195,'Ochenta y ocho','','',3,'','','ochentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',196,'Ochenta y nueve','','',3,'','','ochentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',197,'Noventa','','',3,'','','noventa']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',198,'Noventa y uno','','',3,'','','noventayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',199,'Noventa y dos','','',3,'','','noventaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',200,'Noventa y tres','','',3,'','','noventaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',201,'Noventa y cuatro','','',3,'','','noventaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',202,'Noventa y cinco','','',3,'','','noventaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',203,'Noventa y seis','','',3,'','','noventayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',204,'Noventa y siete','','',3,'','','noventaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',205,'Noventa y ocho','','',3,'','','noventayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',206,'Noventa y nueve','','',3,'','','noventaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',207,'Cien','','',3,'','','cien']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',208,'Amarillo','','',7,'','','amarillo']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',209,'Anaranjado','','',7,'','','anaranjado']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',210,'Azul','','',7,'','','azul']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',211,'Fucsia','','',7,'','','fucsia']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',212,'Marrón','','',7,'','','marron']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',213,'Negro','','',7,'','','negro']);
    $cordovaSQLite.execute(db, "INSERT INTO letters VALUES (?,?,?,?,?,?,?,?,?)", ['',214,'Verde','','',7,'','','verde']);

    /* ----- Tabla learnings ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS learnings (Id INTEGER PRIMARY KEY, content TEXT, description TEXT, title TEXT)");

    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [1,'','','Palabras']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [2,'','','Sonidos Simples']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [3,'','','Numeros']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [4,'','','Vocales']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [5,'','','Sonidos Complejos']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [6,'','','Letras']);
    $cordovaSQLite.execute(db, "INSERT INTO learnings VALUES (?,?,?,?,?)", [7,'','','Colores']);

    /* ----- Tabla learners ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS learners (name_sound_path TEXT, word_id INTEGER, profile_image_path TEXT, congrats_path TEXT, Id INTEGER PRIMARY KEY, title TEXT, content TEXT, letter_id INTEGER, selected BOOLEAN)");

    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',1,'Aaron','A-aron',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',2,'Abel','A-bel',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',3,'Abelardo','A-be-lar-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',4,'Abigaíl','A-bi-ga-íl',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',5,'Abraham','A-bra-ham',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',6,'Abril','A-bril',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',7,'Ada','A-da',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',8,'Adela','A-de-la',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',9,'Adelaida','A-de-lai-da',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',10,'Adelina','A-de-li-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',11,'Adelmo','A-del-mo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',12,'Adolfo','A-dol-fo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',13,'Adrián','A-dri-án',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',14,'Adriano','A-dria-no',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',15,'Adriana','A-dria-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',16,'Alison','A-li-son',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',17,'Aldo','Al-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',18,'Alejandro','A-le-jan-dro',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',19,'Alejandra','A-le-jan-dra',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',20,'Alejo','A-le-jo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',21,'Alfonso','Al-fon-so',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',22,'Alfredo','Al-fre-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',23,'Alicia','A-li-cia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',24,'Alina','A-li-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',25,'Almudena','Al-mu-de-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',26,'Alonso','A-lon-so',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',27,'Álvaro','Ál-va-ro',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',28,'Amadeo','A-ma-de-o',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',29,'Amalia','A-ma-lia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',30,'Amancio','A-man-cio',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',31,'Amanda','A-man-da',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',32,'Amaranto','A-ma-ran-to',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',33,'Amaya','A-ma-ya',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',34,'Amos','A-mos',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',35,'Ambrosio','Am-bro-sio',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',36,'Amelia','A-me-lia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',37,'Amira','A-mi-ra',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',38,'Amparo','Am-pa-ro',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',39,'Ana','A-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',40,'Anacleto','A-na-cle-to',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',41,'Anastasia','A-nas-ta-sia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',42,'Ander','An-der',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',43,'Andrés','An-drés',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',44,'Andrea','An-dre-a',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',45,'Andreína','An-dre-í-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',46,'Ángel','Án-gel',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',47,'Ángela','Án-ge-la',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',48,'Ángelo','Án-ge-lo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',49,'Angélica','An-gé-li-ca',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',50,'Ángeles','Án-ge-les',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',51,'Aníbal','A-ní-bal',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',52,'Aniceto','A-ni-ce-to',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',53,'Angustias','An-gus-tias',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',54,'Anselmo','An-sel-mo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',55,'Antonio','An-to-nio',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',56,'Antonia','An-to-nia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',57,'Antonieta','An-to-nie-ta',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',58,'Apolo','A-po-lo',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',59,'Aquiles','A-qui-les',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',60,'Aquilino','A-qui-li-no',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',61,'Araceli','A-ra-ce-li',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',62,'Archibaldo','Ar-chi-bal-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',63,'Ariadna','A-riad-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',64,'Ariana','A-ria-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',65,'Ariel','A-riel',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',66,'Arístides','A-rís-ti-des',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',67,'Armando','Ar-man-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',68,'Arnoldo','Ar-nol-do',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',69,'Aurelio','Au-re-lio',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',70,'Aurelia','Au-re-lia',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',71,'Aurora','Au-ro-ra',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',72,'Axel','A-xel',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',73,'Azucena','A-zu-ce-na',1,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',74,'Baldomero','Bal-do-me-ro',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',75,'Baltasar','Bal-ta-sar',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',76,'Barack','Ba-rack',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',77,'Bárbara','Bár-ba-ra',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',78,'Bartolomé','Bar-to-lo-mé',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',79,'Basilio','Ba-si-lio',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',80,'Bautista','Bau-tis-ta',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',81,'Beatriz','Bea-triz',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',82,'Belén','Be-lén',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',83,'Belinda','Be-lin-da',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',84,'Beltrán','Bel-trán',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',85,'Benigno','Be-nig-no',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',86,'Benito','Be-ni-to',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',87,'Benjamín','Ben-ja-mín',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',88,'Berenice','Be-re-ni-ce',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',89,'Bernabé','Ber-na-bé',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',90,'Bernadino','Ber-na-di-no',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',91,'Bernadina','Ber-na-di-na',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',92,'Bernardo','Ber-nar-do',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',93,'Berta','Ber-ta',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',94,'Bianca','Bian-ca',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',95,'Bibiana','Bi-bia-na',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',96,'Blanca','Blan-ca',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',97,'Blas','Blas',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',98,'Brenda','Bren-da',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',99,'Bonifacio','Bo-ni-fa-cio',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',100,'Boris','Bo-ris',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',101,'Brígida','Brí-gi-da',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',102,'Brunilda','Bru-nil-da',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',103,'Bruno','Bru-no',2,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',104,'Calixto','Ca-lix-to',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',105,'Camelia','Ca-me-lia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',106,'Camila','Ca-mi-la',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',107,'Camilo','Ca-mi-lo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',108,'Cándido','Cán-di-do',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',109,'Caridad','Ca-ri-dad',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',110,'Carina','Ca-ri-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',111,'Carlos','Car-los',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',112,'Carla','Car-la',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',113,'Carmelo','Car-me-lo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',114,'Carmen','Car-men',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',115,'Carola','Ca-ro-la',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',116,'Carolina','Ca-ro-li-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',117,'Casilda','Ca-sil-da',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',118,'Casimiro','Ca-si-mi-ro',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',119,'Catalina','Ca-ta-li-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',120,'Cayetano','Ca-ye-ta-no',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',121,'Cecilia','Ce-ci-lia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',122,'Celestina','Ce-les-ti-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',123,'Celia','Ce-lia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',124,'Celina','Ce-li-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',125,'Crisanto','Cri-san-to',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',126,'Cristian','Cris-tian',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',127,'Cristina','Cris-ti-na',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',128,'Celso','Cel-so',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',129,'César','Cé-sar',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',130,'Cesáreo','Ce-sá-reo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',131,'Cintia','Cin-tia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',132,'Cipriano','Ci-pria-no',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',133,'Cirilo','Ci-ri-lo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',134,'Ciro','Ci-ro',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',135,'Clara','Cla-ra',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',136,'Claudio','Clau-dio',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',137,'Claudia','Clau-dia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',138,'Clemente','Cle-men-te',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',139,'Cleopatra','Cleo-pa-tra',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',140,'Clotilde','Clo-til-de',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',141,'Concepción','Con-cep-ción',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',142,'Conrado','Con-ra-do',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',143,'Constancio','Cons-tan-cio',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',144,'Constantino','Cons-tan-ti-no',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',145,'Consuelo','Con-sue-lo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',146,'Cora','Co-ra',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',147,'Cordelia','Cor-de-lia',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',148,'Cornelio','Cor-ne-lio',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',149,'Cosme','Cos-me',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',150,'Cristóbal','Cris-tó-bal',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',151,'Crisóstomo','Cri-sós-to-mo',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',152,'Cruz','Cruz',3,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',153,'Dagoberto','Da-go-ber-to',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',154,'Dana','Da-na',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',155,'Daniel','Da-niel',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',156,'Darío','Da-rí-o',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',157,'David','Da-vid',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',158,'Débora','Dé-bo-ra',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',159,'Delia','De-lia',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',160,'Delmiro','Del-mi-ro',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',161,'Demetrio','De-me-trio',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',162,'Diana','Dia-na',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',163,'Diego','Die-go',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',164,'Dina','Di-na',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',165,'Dionisio','Dio-ni-sio',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',166,'Dolores','Do-lo-res',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',167,'Domingo','Do-min-go',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',168,'Dominico','Do-mi-ni-co',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',169,'Donato','Do-na-to',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',170,'Dora','Do-ra',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',171,'Dulcinea','Dul-ci-nea',4,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',172,'Edgardo','Ed-gar-do',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',173,'Edith','E-dith',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',174,'Edmundo','Ed-mun-do',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',175,'Eduardo','E-duar-do',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',176,'Efraín','E-fra-ín',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',177,'Efrén','E-frén',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',178,'Elena','E-le-na',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',179,'Eleonor','E-leo-nor',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',180,'Elías','E-lí-as',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',181,'Elisa','E-li-sa',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',182,'Elizabeth','E-li-za-beth',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',183,'Elisabeth','E-li-sa-beth',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',184,'Eloisa','E-lo-i-sa',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',185,'Eloy','E-lo-y',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',186,'Elsa','El-sa',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',187,'Elvira','El-vi-ra',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',188,'Emilia','E-mi-lia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',189,'Emilio','E-mi-lio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',190,'Ema','E-ma',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',191,'Emma','Em-ma',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',192,'Emanuel','E-ma-nuel',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',193,'Enmanuel','En-ma-nuel',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',194,'Encarnación','En-car-na-ci-ón',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',195,'Engracia','En-gra-cia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',196,'Enrique','En-ri-que',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',197,'Erasmo','E-ras-mo',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',198,'Erico','E-ri-co',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',199,'Eric','E-ric',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',200,'Erick','E-rick',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',201,'Erica','E-ri-ca',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',202,'Ericka','E-ri-cka',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',203,'Ernesto','Er-nes-to',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',204,'Esmeralda','Es-me-ral-da',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',205,'Esperanza','Es-pe-ran-za',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',206,'Esteban','Es-te-ban',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',207,'Estefanía','Es-te-fa-ní-a',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',208,'Estela','Es-te-la',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',209,'Ester','Es-ter',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',210,'Etel','E-tel',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',211,'Euclides','Eu-cli-des',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',212,'Eudosia','Eu-do-sia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',213,'Eudoxio','Eu-do-xio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',214,'Eufemio','Eu-fe-mio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',215,'Eufemia','Eu-fe-mia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',216,'Eufrasio','Eu-fra-sio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',217,'Eufrasia','Eu-fra-sia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',218,'Eugenio','Eu-ge-nio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',219,'Eugenia','Eu-ge-nia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',220,'Eulalia','Eu-la-lia',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',221,'Eulalio','Eu-la-lio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',222,'Eusebio','Eu-se-bio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',223,'Eustaquio','Eus-ta-quio',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',224,'Eva','E-va',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',225,'Evangelina','E-van-ge-li-na',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',226,'Evaristo','E-va-ris-to',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',227,'Ezequiel','E-ze-quiel',5,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',228,'Fabián','Fa-bi-án',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',229,'Fabiola','Fa-bio-la',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',230,'Fabio','Fa-bio',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',231,'Fanny','Fan-ny',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',232,'Fiorella','Fio-re-lla',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',233,'Fio','Fi-o',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',234,'Fátima','Fá-ti-ma',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',235,'Faustino','Faus-ti-no',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',236,'Fausto','Faus-to',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',237,'Federico','Fe-de-ri-co',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',238,'Fedor','Fe-dor',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',239,'Félix','Fé-lix',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',240,'Felipe','Fe-li-pe',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',241,'Fermín','Fer-mín',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',242,'Fernando','Fer-nan-do',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',243,'Fernanda','Fer-nan-da',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',244,'Fidel','Fi-del',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',245,'Filiberto','Fi-li-ber-to',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',246,'Filomeno','Fi-lo-me-no',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',247,'Flavio','Fla-vio',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',248,'Flora','Flo-ra',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',249,'Florencio','Flo-ren-cio',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',250,'Florencia','Flo-ren-cia',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',251,'Florinda','Flo-rin-da',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',252,'Florida','Flo-ri-da',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',253,'Florián','Flo-ri-án',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',254,'Francisco','Fran-cis-co',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',255,'Franco','Fran-co',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',256,'Frida','Fri-da',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',257,'Freddy','Fre-ddy',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',258,'Froilán','Froi-lán',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',259,'Fulvio','Ful-vio',6,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',260,'Gabriel','Ga-briel',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',261,'Gabriela','Ga-brie-la',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',262,'Gema','Ge-ma',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',263,'Gaspar','Gas-par',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',264,'Gedeón','Ge-de-ón',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',265,'Genoveva','Ge-no-ve-va',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',266,'Gerardo','Ge-rar-do',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',267,'Germán','Ger-mán',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',268,'Gertrudis','Ger-tru-dis',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',269,'Gilberto','Gil-ber-to',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',270,'Gladis','Gla-dis',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',271,'Gloria','Glo-ria',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',272,'Godofredo','Go-do-fre-do',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',273,'Gracia','Gra-cia',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',274,'Graciela','Gra-cie-la',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',275,'Gregorio','Gre-go-rio',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',276,'Grisel','Gri-sel',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',277,'Gorka','Gor-ka',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',278,'Guido','Gui-do',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',279,'Guillermo','Gui-ller-mo',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',280,'Gustavo','Gus-ta-vo',7,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',281,'Haroldo','Ha-rol-do',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',282,'Haydee','Ha-y-dee',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',283,'Hebe','He-be',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',284,'Helena','He-le-na',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',285,'Heli','He-li',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',286,'Héctor','Héc-tor',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',287,'Heriberto','He-ri-ber-to',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',288,'Heriberta','He-ri-ber-ta',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',289,'Hermelando','Her-me-lan-do',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',290,'Herminio','Her-mi-nio',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',291,'Herminia','Her-mi-nia',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',292,'Hernán','Her-nán',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',293,'Higinio','Hi-gi-nio',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',294,'Higinia','Hi-gi-nia',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',295,'Hilda','Hil-da',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',296,'Hipólito','Hi-pó-li-to',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',297,'Homero','Ho-me-ro',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',298,'Honorio','Ho-no-rio',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',299,'Horacio','Ho-ra-cio',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',300,'Hortencia','Hor-ten-cia',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',301,'Hortencio','Hor-ten-cio',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',302,'Hugo','Hu-go',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',303,'Huberto','Hu-ber-to',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',304,'Humberto','Hum-ber-to',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',305,'Helenia','He-le-nia',8,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',306,'Ida','I-da',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',307,'Idefonso','I-de-fon-so',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',308,'Ignacio','Ig-na-cio',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',309,'Imanol','I-ma-nol',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',310,'Imelda','I-mel-da',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',311,'Íñigo','Í-ñi-go',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',312,'Indalecio','In-da-le-cio',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',313,'Inés','I-nés',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',314,'Ingrid','In-grid',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',315,'Irene','I-re-ne',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',316,'Iris','I-ris',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',317,'Irma','Ir-ma',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',318,'Isaac','I-sa-ac',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',319,'Isabel','I-sa-bel',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',320,'Isidoro','I-si-do-ro',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',321,'Isaías','I-sa-í-as',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',322,'Israel','Is-ra-el',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',323,'Iván','I-ván',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',324,'Ivonne','I-von-ne',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',325,'Isis','I-sis',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',326,'Izaskun','I-zas-kun',9,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',327,'Jacinto','Ja-cin-to',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',328,'Jacobo','Ja-co-bo',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',329,'Jaqueline','Ja-que-li-ne',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',330,'Jairo','Jai-ro',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',331,'Javier','Ja-vier',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',332,'Jasmín','Jas-mín',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',333,'Jaime','Jai-me',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',334,'Jennifer','Je-nni-fer',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',335,'Jeremías','Je-re-mí-as',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',336,'Jerónimo','Je-ró-ni-mo',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',337,'Jessica','Je-ssi-ca',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',338,'Jesús','Je-sús',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',339,'Joaquín','Jo-a-quín',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',340,'Job','Job',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',341,'Jonathan','Jo-na-than',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',342,'Jonhatan','Jo-nha-tan',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',343,'Jonatan','Jo-na-tan',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',344,'Jorge','Jor-ge',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',345,'José','Jo-sé',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',346,'Josefa','Jo-se-fa',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',347,'Josefina','Jo-se-fi-na',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',348,'Jonás','Jo-nás',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',349,'Josué','Jo-su-é',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',350,'Juan','Juan',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',351,'Juana','Je-re-mí-as',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',352,'Judá','Ju-dá',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',353,'Judit','Ju-dit',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',354,'Judith','Ju-dith',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',355,'Julián','Ju-li-án',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',356,'Juliana','Ju-lia-na',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',357,'Julia','Ju-lia',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',358,'Julio','Ju-lio',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',359,'Justiniano','Jus-ti-nia-no',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',360,'Justo','Jus-to',10,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',361,'Karen','Ka-ren',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',362,'Katia','Ka-tia',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',363,'Kevin','Ke-vin',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',364,'Koldo','Kol-do',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',365,'Kilian','Ki-lian',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',366,'Krishna','Krish-na',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',367,'Kepa','Ke-pa',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',368,'Kenneth','Ke-nneth',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',369,'Karmele','Kar-me-le',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',370,'Karl','Karl',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',371,'Karla','Kar-la',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',372,'Kurt','Kurt',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',373,'Kali','Ka-li',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',374,'Karim','Ka-rim',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',375,'Kira','Ki-ra',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',376,'Karina','Ka-ri-na',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',377,'Kerry','Ke-rry',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',378,'Kerman','Ker-man',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',379,'Keisi','Kei-si',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',380,'Kenán','Ke-nán',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',381,'Katyuska','Ka-ty-us-ka',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',382,'Kassandra','Ka-ssan-dra',11,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',383,'Lamberto','Lam-ber-to',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',384,'Laura','Lau-ra',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',385,'Lázaro','Lá-za-ro',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',386,'Leandro','Le-an-dro',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',387,'Leo','Leo',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',388,'León','Le-ón',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',389,'Leonardo','Leo-nar-do',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',390,'Leónidas','Le-ó-ni-das',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',391,'Leonor','Leo-nor',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',392,'Leonora','Leo-no-ra',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',393,'Leopoldo','Leo-pol-do',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',394,'Leticia','Le-ti-cia',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',395,'Lía','Lí-a',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',396,'Lida','Li-da',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',397,'Lidia','Li-dia',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',398,'Lydia','Ly-dia',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',399,'Liliana','Li-lia-na',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',400,'Lina','Li-na',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',401,'Linda','Lin-da',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',402,'Lorenzo','Lo-ren-zo',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',403,'Lucas','Lu-cas',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',404,'Lucía','Lu-cí-a',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',405,'Luciano','Lu-cia-no',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',406,'Lucio','Lu-cio',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',407,'Lucrecia','Lu-cre-cia',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',408,'Luis','Luis',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',409,'Luisa','Lui-sa',12,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',410,'Mabel','Ma-bel',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',411,'Macarena','Ma-ca-re-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',412,'Magalí','Ma-ga-lí',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',413,'Magdalena','Mag-da-le-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',414,'Maite','Mai-te',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',415,'Malvina','Mal-vi-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',416,'Manuel','Ma-nuel',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',417,'Marcelo','Mar-ce-lo',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',418,'Marcos','Mar-cos',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',419,'Margarita','Mar-ga-ri-ta',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',420,'Macarena','Ma-ca-re-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',421,'María','Ma-rí-a',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',422,'Mario','Ma-rio',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',423,'Mariana','Ma-ria-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',424,'Mariano','Ma-ria-no',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',425,'Marta','Mar-ta',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',426,'Martín','Mar-tín',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',427,'Mateo','Ma-teo',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',428,'Matías','Ma-tí-as',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',429,'Matilde','Ma-til-de',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',430,'Matilda','Ma-til-da',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',431,'Mauricio','Mau-ri-cio',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',432,'Mauro','Mau-ro',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',433,'Maximiliano','Ma-xi-mi-lia-no',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',434,'Máximo','Má-xi-mo',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',435,'Miguel','Mi-guel',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',436,'Mina','Mi-na',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',437,'Miriam','Mi-riam',13,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',438,'Napoleón','Na-po-le-ón',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',439,'Narciso','Nar-ci-so',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',440,'Natalia','Na-ta-lia',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',441,'Natán','Na-tán',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',442,'Naúm','Na-úm',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',443,'Nélida','Né-li-da',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',444,'Néstor','Nés-tor',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',445,'Nicolás','Ni-co-lás',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',446,'Nidia','Ni-dia',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',447,'Noé','No-é',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',448,'Norma','Nor-ma',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',449,'Nuria','Nu-ria',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',450,'Nadir','Na-dir',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',451,'Noa','Noa',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',452,'Nazaret','Na-za-ret',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',453,'Nazareth','Na-za-reth',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',454,'Nerea','Ne-rea',14,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',455,'Obama','O-ba-ma',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',456,'Octavio','Oc-ta-vio',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',457,'Ofelia','O-fe-lia',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',458,'Olga','Ol-ga',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',459,'Olimpia','O-lim-pia',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',460,'Óliver','Ó-li-ver',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',461,'Oliverio','O-li-ve-rio',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',462,'Olivia','O-li-via',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',463,'Omar','O-mar',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',464,'Orlando','Or-lan-do',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',465,'Óscar','Ós-car',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',466,'Oswaldo','Os-wal-do',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',467,'Osvaldo','Os-val-do',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',468,'Otón','O-tón',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',469,'Ovidio','O-vi-dio',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',470,'Osiris','O-si-ris',16,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',471,'Pablo','Pa-blo',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',472,'Pánfilo','Pán-fi-lo',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',473,'Paloma','Pa-lo-ma',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',474,'Paola','Pa-o-la',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',475,'Paolo','Pa-o-lo',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',476,'Pascual','Pas-cual',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',477,'Patricia','Pa-tri-cia',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',478,'Patricio','Pa-tri-cio',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',479,'Paul','Paul',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',480,'Paula','Pau-la',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',481,'Paulino','Pau-li-no',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',482,'Paz','Paz',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',483,'Pedro','Pe-dro',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',484,'Penélope','Pe-né-lo-pe',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',485,'Perla','Per-la',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',486,'Pilar','Pi-lar',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',487,'Piedad','Pie-dad',17,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',488,'Querubín','Que-ru-bín',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',489,'Quintina','Quin-ti-na',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',490,'Quirico','Qui-ri-co',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',491,'Qulidonia','Qu-li-do-nia',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',492,'Quiterio','Qui-te-rio',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',493,'Quiriaco','Qui-ri-a-co',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',494,'Quirina','Qui-ri-na',18,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',495,'Rafael','Ra-fa-el',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',496,'Rafaela','Ra-fa-e-la',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',497,'Raimundo','Rai-mun-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',498,'Ramiro','Ra-mi-ro',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',499,'Ramona','Ra-mo-na',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',500,'Ramón','Ra-món',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',501,'Raquel','Ra-quel',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',502,'Raúl','Ra-úl',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',503,'Rebeca','Re-be-ca',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',504,'Reina','Rei-na',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',505,'Reinaldo','Rei-nal-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',506,'Regina','Re-gi-na',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',507,'Remedios','Re-me-dios',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',508,'Renato','Re-na-to',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',509,'René','Re-né',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',510,'Ricardo','Ri-car-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',511,'Rita','Ri-ta',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',512,'Roberto','Ro-ber-to',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',513,'Roberta','Ro-ber-ta',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',514,'Rocío','Ro-cí-o',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',515,'Rodolfo','Ro-dol-fo',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',516,'Rodrigo','Ro-dri-go',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',517,'Rogelio','Ro-ge-lio',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',518,'Rolando','Ro-lan-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',519,'Rolán','Ro-lán',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',520,'Román','Ro-mán',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',521,'Romeo','Ro-meo',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',522,'Romilda','Ro-mil-da',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',523,'Romildo','Ro-mil-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',524,'Ronaldo','Ro-nal-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',525,'Roque','Ro-que',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',526,'Rosa','Ro-sa',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',527,'Rosalía','Ro-sa-lí-a',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',528,'Rosamunda','Ro-sa-mun-da',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',529,'Rosana','Ro-sa-na',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',530,'Rosario','Ro-sa-rio',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',531,'Rosendo','Ro-sen-do',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',532,'Roxana','Ro-xa-na',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',533,'Rubén','Ru-bén',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',534,'Rufino','Ru-fi-no',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',535,'Ruperto','Ru-per-to',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',536,'Ruth','Ruth',19,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',537,'Sabina','Sa-bi-na',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',538,'Sagrario','Sa-gra-rio',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',539,'Salomé','Sa-lo-mé',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',540,'Salomón','Sa-lo-món',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',541,'Salvador','Sal-va-dor',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',542,'Samantha','Sa-man-tha',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',543,'Samanta','Sa-man-ta',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',544,'Samuel','Sa-muel',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',545,'Sancho','San-cho',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',546,'Sandra','San-dra',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',547,'Sansón','San-són',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',548,'Santiago','San-tia-go',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',549,'Sara','Sa-ra',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',550,'Saúl','Sa-úl',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',551,'Sebastián','Se-bas-ti-án',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',552,'Segismundo','Se-gis-mun-do',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',553,'Serafín','Se-ra-fín',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',554,'Serena','Se-re-na',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',555,'Sergio','Ser-gio',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',556,'Sheila','Shei-la',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',557,'Silvano','Sil-va-no',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',558,'Silvestre','Sil-ves-tre',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',559,'Silvia','Sil-via',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',560,'Silvio','Sil-vio',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',561,'Simón','Si-món',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',562,'Siro','Si-ro',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',563,'Sixto','Six-to',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',564,'Socorro','So-co-rro',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',565,'Sofía','So-fí-a',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',566,'Sol','Sol',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',567,'Solange','So-lan-ge',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',568,'Soledad','So-le-dad',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',569,'Soraya','So-ra-ya',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',570,'Sonia','So-nia',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',571,'Susana','Su-sa-na',20,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',572,'Tadeo','Ta-deo',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',573,'Tamara','Ta-ma-ra',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',574,'Tania','Ta-nia',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',575,'Tatiana','Ta-tia-na',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',576,'Telma','Tel-ma',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',577,'Teobaldo','Teo-bal-do',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',578,'Teodoro','Teo-do-ro',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',579,'Teodórico','Teo-dó-ri-co',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',580,'Teodosio','Teo-do-sio',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',581,'Teófilo','Teó-fi-lo',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',582,'Teresa','Te-re-sa',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',583,'Tirso','Tir-so',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',584,'Tito','Ti-to',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',585,'Timoteo','Ti-mo-te-o',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',586,'Tobías','To-bí-as',21,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',587,'Ulises','U-li-ses',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',588,'Ulrico','Ul-ri-co',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',589,'Urbano','Ur-ba-no',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',590,'Urbana','Ur-ba-na',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',591,'Uriel','U-riel',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',592,'Úrsula','Úr-su-la',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',593,'Urano','U-ra-no',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',594,'Ulfrida','Ul-fri-da',22,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',595,'Valentín','Va-len-tín',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',596,'Valentina','Va-len-ti-na',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',597,'Valeria','Va-le-ria',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',598,'Valerio','Va-le-rio',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',599,'Vanesa','Va-ne-sa',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',600,'Ventura','Ven-tu-ra',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',601,'Velasco','Ve-las-co',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',602,'Vera','Ve-ra',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',603,'Verónica','Ve-ró-ni-ca',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',604,'Vicente','Vi-cen-te',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',605,'Victor','Vic-tor',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',606,'Victoria','Vic-to-ria',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',607,'Victoriano','Vic-to-ria-no',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',608,'Vida','Vi-da',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',609,'Vilma','Vil-ma',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',610,'Violeta','Vio-le-ta',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',611,'Virgilio','Vir-gi-lio',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',612,'Virginia','Vir-gi-nia',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',613,'Viviana','Vi-via-na',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',614,'Vladimir','Vla-di-mir',23,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',615,'Walter','Wal-ter',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',616,'Wenceslao','Wen-ces-la-o',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',617,'Wilfredo','Wil-fre-do',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',618,'Wulstana','Wuls-ta-na',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',619,'Wulfrano','Wul-fra-no',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',620,'Witerico','Wi-te-ri-co',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',621,'Wendy','Wen-dy',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',622,'William','Wil-liam',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',623,'Waldo','Wal-do',24,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',624,'Xavier','Xa-vier',25,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',625,'Xabier','Xa-bier',25,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',626,'Ximena','Xi-me-na',25,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',627,'Xenia','Xe-nia',25,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',628,'Yeray','Ye-ray',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',629,'Yolanda','Yo-lan-da',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',630,'Yonatán','Yo-na-tán',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',631,'Yeudil','Yeu-dil',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',632,'Yehudí','Ye-hu-dí',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',633,'Yahvé','Yah-vé',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',634,'Yvonne','Y-von-ne',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',635,'Yusuf','Yu-suf',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',636,'Yuri','Yu-ri',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',637,'Yasir','Ya-sir',26,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',638,'Zacarías','Za-ca-rí-as',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',639,'Zoe','Zoe',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',640,'Zoraida','Zo-rai-da',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',641,'Zulema','Zu-le-ma',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',642,'Zeus','Zeus',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',643,'Zenaida','Ze-nai-da',27,0]);
    $cordovaSQLite.execute(db, "INSERT INTO learners VALUES (?,?,?,?,?,?,?,?,?)", ['','','','',644,'Zahara','Za-ha-ra',27,0]);

    /* ----- Tabla images ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS images (Id INTEGER PRIMARY KEY, title TEXT)");

    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [1,'Abeja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [2,'Abrigo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [3,'Abuela']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [4,'Abuelo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [5,'Aguacate']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [6,'Águila']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [7,'Avispa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [8,'Avestruz']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [9,'Anaranjado']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [10,'Áncla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [11,'Aguja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [12,'Ajo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [13,'Alfombra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [14,'Almendra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [15,'Ardilla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [16,'Amarillo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [17,'Abanico']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [18,'Ángel']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [19,'Avión']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [20,'Astronauta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [21,'Anillo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [22,'Araña']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [23,'Azul']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [24,'Bufanda']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [25,'Bloque']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [26,'Bombero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [27,'Bombilla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [28,'Bosque']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [29,'Bota']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [30,'Botella']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [31,'Brazo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [32,'Asno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [33,'Ballena']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [34,'Balón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [35,'Bambú']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [36,'Banano']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [37,'Bandera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [38,'Barco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [39,'Barril']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [40,'Bate']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [41,'Baúl']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [42,'Bebe']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [43,'Biberón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [44,'Biblioteca']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [45,'Búho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [46,'Caballo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [47,'Cable']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [48,'Cacao']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [49,'Cactus']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [50,'Cama']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [51,'Calabaza']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [52,'Campana']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [53,'Cangrejo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [54,'Canguro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [55,'Carro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [56,'Cascada']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [57,'Cebolla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [58,'Ciguena']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [59,'Cinturón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [60,'Circo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [61,'Cisne']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [62,'Coco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [63,'Cohete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [64,'Colibrí']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [65,'Conejo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [66,'Cono']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [67,'Corazón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [68,'Corbata']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [69,'Cucaracha']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [70,'Cuchillo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [71,'Dado']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [72,'Dedo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [73,'Delfín']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [74,'Desierto']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [75,'Diente']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [76,'Dinosaurio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [77,'Dos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [78,'Durazno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [79,'Elefante']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [80,'Escalera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [81,'Esponja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [82,'Estatua']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [83,'Faro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [84,'Flecha']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [85,'Flor']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [86,'Frambuesa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [87,'Fresa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [88,'Fuego']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [89,'Fuente']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [90,'Foca']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [91,'Falda']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [92,'Galleta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [93,'Gallina']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [94,'Gallo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [95,'Gato']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [96,'Gavilán']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [97,'Girasol']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [98,'Gorra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [99,'Guante']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [100,'Guerrero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [101,'Guitarra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [102,'Gusano']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [103,'Hacha']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [104,'Helicóptero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [105,'Hormiga']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [106,'Huevo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [107,'Indio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [108,'Iglesia']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [109,'Jirafa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [110,'Jardín']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [111,'Jarra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [112,'Jaula']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [113,'Jabón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [114,'Juez']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [115,'Kiwi']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [116,'Labio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [117,'Lámpara']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [118,'Lápiz']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [119,'Látigo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [120,'Leche']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [121,'Lengua']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [122,'León']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [123,'Leopardo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [124,'Libro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [125,'Limón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [126,'Linterna']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [127,'Llave']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [128,'Lobo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [129,'Loro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [130,'Luna']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [131,'Maíz']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [132,'Mamá']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [133,'Manzana']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [134,'Mapa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [135,'Mariposa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [136,'Martillo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [137,'Melon']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [138,'Mesa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [139,'Mono']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [140,'Motocicleta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [141,'Naranja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [142,'Nariz']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [143,'Nido']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [144,'Nieve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [145,'Nube']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [146,'Ojo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [147,'Oreja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [148,'Oruga']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [149,'Oveja']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [150,'Oso']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [151,'Olla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [152,'Olivo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [153,'Ocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [154,'Once']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [155,'Pájaro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [156,'Pan']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [157,'Pantalón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [158,'Pantera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [159,'Papa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [160,'Papagayo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [161,'Paracaidas']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [162,'Paraguas']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [163,'Pato']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [164,'Pavo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [165,'Payaso']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [166,'Perro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [167,'Piano']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [168,'Piña']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [169,'Pincel']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [170,'Pingüino']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [171,'Pipa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [172,'Piscina']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [173,'Pluma']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [174,'Puente']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [175,'Pulgas']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [176,'Radio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [177,'Rana']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [178,'Raqueta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [179,'Ratón']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [180,'Reloj']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [181,'Rey']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [182,'Robot']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [183,'Rueda']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [184,'Sandia']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [185,'Semáforo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [186,'Serpiente']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [187,'Silla']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [188,'Sofá']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [189,'Sol']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [190,'Seis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [191,'Siete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [192,'Telaraña']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [193,'Teléfono']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [194,'Televisor']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [195,'Tigre']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [196,'Tijera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [197,'Tomate']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [198,'Tornillo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [199,'Tortuga']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [200,'Trébol']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [201,'Tren']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [202,'Uva']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [203,'Uno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [204,'Uña']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [205,'Unicornio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [206,'Vaca']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [207,'Vaquero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [208,'Vino']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [209,'Violíin']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [210,'Volcán']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [211,'Xilófono']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [212,'Yate']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [213,'Zapato']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [214,'Zorra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [215,'Zanahoria']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [216,'Zoologico']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [217,'Balanza']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [218,'Catorce']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [219,'Cinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [220,'Cuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [221,'Dieciseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [222,'Diecisiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [223,'Dieciocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [224,'Diecinueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [225,'Diez']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [226,'Doce']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [227,'Edificio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [228,'Enano']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [229,'Espada']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [230,'Kilo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [231,'Nueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [232,'Papaa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [233,'Pepino']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [234,'Quince']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [235,'Tetero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [236,'Trece']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [237,'Nandu']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [238,'Cero']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [239,'Veinte']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [240,'Nene']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [241,'Pera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [242,'Pollito']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [243,'Taza']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [244,'Tiburon']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [245,'Tres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [246,'Castor']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [247,'Negro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [248,'Marron']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [249,'Cabra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [250,'Camaleon']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [251,'Camello']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [252,'Cebra']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [253,'Queso']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [254,'Dragon']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [255,'Estrella']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [256,'Oro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [257,'Verde']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [258,'Helado']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [259,'Hilo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [260,'Istmo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [261,'Montana']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [262,'Pulpo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [263,'Ola']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [264,'Orquidea']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [265,'Panda']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [266,'Tuerca']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [267,'Tetera']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [268,'Yoyo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [269,'Yuca']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [270,'Fucsia']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [271,'Lechuga']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [272,'Mora']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [273,'Dama']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [274,'Dia']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [275,'Domino']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [276,'Eje']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [277,'Espejo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [278,'Ventana']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [279,'Hielo']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [280,'Gota']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [281,'Jardin']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [282,'Dulce']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [283,'Maleta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [284,'Rio']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [285,'Calle']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [286,'Arbol']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [287,'Bol']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [288,'Jarron']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [289,'Vela']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [290,'Cierre']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [291,'estufa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [292,'veintiuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [293,'veintidos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [294,'veintitres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [295,'veinticuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [296,'veinticinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [297,'veintiseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [298,'veintisiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [299,'veintiocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [300,'veintinueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [301,'treinta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [302,'treintayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [303,'treintaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [304,'treintaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [305,'treintaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [306,'treintaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [307,'treintayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [308,'treintaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [309,'treintayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [310,'treintaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [311,'cuarenta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [312,'cuarentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [313,'cuarentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [314,'cuarentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [315,'cuarentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [316,'cuarentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [317,'cuarentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [318,'cuarentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [319,'cuarentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [320,'cuarentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [321,'cincuenta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [322,'cincuentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [323,'cincuentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [324,'cincuentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [325,'cincuentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [326,'cincuentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [327,'cincuentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [328,'cincuentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [329,'cincuentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [330,'cincuentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [331,'sesenta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [332,'sesentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [333,'sesentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [334,'sesentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [335,'sesentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [336,'sesentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [337,'sesentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [338,'sesentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [339,'sesentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [340,'sesentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [341,'setenta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [342,'setentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [343,'setentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [344,'setentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [345,'setentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [346,'setentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [347,'setentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [348,'setentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [349,'setentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [350,'setentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [351,'ochenta']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [352,'ochentayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [353,'ochentaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [354,'ochentaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [355,'ochentaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [356,'ochentaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [357,'ochentayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [358,'ochentaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [359,'ochentayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [360,'ochentaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [361,'noventa']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [362,'noventayuno']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [363,'noventaydos']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [364,'noventaytres']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [365,'noventaycuatro']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [366,'noventaycinco']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [367,'noventayseis']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [368,'noventaysiete']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [369,'noventayocho']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [370,'noventaynueve']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [371,'cien']);
    $cordovaSQLite.execute(db, "INSERT INTO images VALUES (?,?)", [372,'wifi']);

    /* ----- Tabla family members ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS family_members (Id INTEGER PRIMARY KEY, description TEXT, description_en TEXT, cant INTEGER)");

    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [1,'Mamá','Mom',1]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [2,'Papá','Dad',1]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [3,'Abuelo','Grandfather',2]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [4,'Abuela','Grandmother',2]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [5,'Hermano','Brother',3]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [6,'Hermana','Sister',3]);
    $cordovaSQLite.execute(db, "INSERT INTO family_members VALUES (?,?,?,?)", [7,'Mascota','Pet',2]);

    /* ----- Tabla family ----- */
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS family (Id INTEGER PRIMARY KEY, family_member_id INTEGER, word_id INTEGER, name TEXT, congrats_sound_path TEXT)");

  });
})

 .config(function($stateProvider, $urlRouterProvider, mainViewProvider) {

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('app.vocales', {
    url: '/vocales',
    views: {
      'menuContent': {
        templateUrl: 'templates/vocales.html',
        controller: 'LettersCtrl'
      }
    }
  })
  .state('app.sonidos', {
    url: '/sonidos',
    views: {
      'menuContent': {
        templateUrl: 'templates/sonidos.html'
      }
    }
  })
  .state('app.simples', {
    url: '/simples',
    views: {
      'menuContent': {
        templateUrl: 'templates/sonidosimples.html',
        controller: 'SoundsCtrl'
      }
    }
  })
  .state('app.compuestos', {
    url: '/compuestos',
    views: {
      'menuContent': {
        templateUrl: 'templates/sonidoscompuestos.html',
        controller: 'SoundsCompCtrl'
      }
    }
  })
  .state('app.simple', {
      cache: false,
      url: '/simples/:simpleId',
      views: {
        'menuContent': {
          templateUrl: 'templates/detallesimples.html',
          controller: 'SoundsSimpleCtrl'
        }
      }
    })
  .state('app.compuesto', {
      cache: false,
      url: '/compuestos/:compuestoId',
      views: {
        'menuContent': {
          templateUrl: 'templates/detallecompuestos.html',
          controller: 'SoundsCompuestoCtrl'
        }
      }
    })
   .state('app.editSimple', {
      url: '/editSimple/:sencilloId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editSimples.html',
          controller: 'SoundsSimpleEditCtrl'
        }
      }
    })
    .state('app.editCompuesto', {
      url: '/editComp/:compId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editCompuestos.html',
          controller: 'SoundsCompEditCtrl'
        }
      }
    })
  .state('app.palabras', {
    url: '/palabras',
    views: {
      'menuContent': {
        templateUrl: 'templates/palabras.html',
        controller: 'wordsCtrl'
      }
    }
  })
  .state('app.palabra', {
    cache: false,
    reload: true,
    url: '/palabras/:palabrasId',
    views: {
      'menuContent': {
        templateUrl: 'templates/detallePalabras.html',
        controller: 'wordsDetalleCtrl'
      }
    }
  })
  .state('app.editPalabra', {
      url: '/editPalabra/:palabraId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editPalabra.html',
          controller: 'wordsEditCtrl'
        }
      }
  })
  .state('app.addPalabra', {
      url: '/addPalabra/:palabraId',
      views: {
        'menuContent': {
          templateUrl: 'templates/addPalabra.html',
          controller: 'wordsAddCtrl'
        }
      }
  })
  .state('app.numeros', {
    url: '/numeros',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros.html'
      }
    }
  })
  .state('app.numeros10', {
    url: '/numeros10',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros10.html',
        controller: 'NumbersCtrl10'
      }
    }
  })
  .state('app.numeros20', {
    url: '/numeros20',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros20.html',
        controller: 'NumbersCtrl20'
      }
    }
  })
  .state('app.numeros30', {
    url: '/numeros30',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros30.html',
        controller: 'NumbersCtrl30'
      }
    }
  })
  .state('app.numeros40', {
    url: '/numeros40',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros40.html',
        controller: 'NumbersCtrl40'
      }
    }
  })
  .state('app.numeros50', {
    url: '/numeros50',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros50.html',
        controller: 'NumbersCtrl50'
      }
    }
  })
  .state('app.numeros60', {
    url: '/numeros60',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros60.html',
        controller: 'NumbersCtrl60'
      }
    }
  })
  .state('app.numeros70', {
    url: '/numeros70',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros70.html',
        controller: 'NumbersCtrl70'
      }
    }
  })
  .state('app.numeros80', {
    url: '/numeros80',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros80.html',
        controller: 'NumbersCtrl80'
      }
    }
  })
  .state('app.numeros90', {
    url: '/numeros90',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros90.html',
        controller: 'NumbersCtrl90'
      }
    }
  })
  .state('app.numeros100', {
    url: '/numeros100',
    views: {
      'menuContent': {
        templateUrl: 'templates/numeros100.html',
        controller: 'NumbersCtrl100'
      }
    }
  })

  .state('app.colores', {
    url: '/colores',
    views: {
      'menuContent': {
        templateUrl: 'templates/colores.html',
        controller: 'ColoresCtrl'
      }
    }
  })
  .state('app.familiaSlides', {
    cache: false,
    reload: true,
    url: '/familiaSlides',
    views: {
      'menuContent': {
        templateUrl: 'templates/familiaSlides.html',
        controller: 'FamiliaSlidesCtrl'
      }
    }
  })
  .state('app.familia', {
    cache: false,
    reload: true,
    url: '/familia',
    views: {
      'menuContent': {
        templateUrl: 'templates/familia.html',
        controller: 'FamiliaCtrl'
      }
    }
  })
  .state('app.addFamilia', {
    cache: false,
    reload: true,
    url: '/addFamilia/:familiaId',
    views: {
      'menuContent': {
        templateUrl: 'templates/addFamilia.html',
        controller: 'FamiliaAddCtrl'
      }
    }
  })
  .state('app.editFamilia', {
    url: '/editFamilia/:familiaId',
    views: {
      'menuContent': {
        templateUrl: 'templates/editFamilia.html',
        controller: 'FamiliaEditCtrl'
      }
    }
  })
  .state('app.completar', {
    url: '/completar',
    views: {
      'menuContent': {
        templateUrl: 'templates/completar.html'
      }
    }
  })
  .state('app.compPalabras', {
    url: '/compPalabras',
    views: {
      'menuContent': {
        templateUrl: 'templates/compPalabras.html',
        controller: 'wordsCtrl'
      }
    }
  })
  .state('app.compPalabras2', {
    url: '/compPalabras2',
    views: {
      'menuContent': {
        templateUrl: 'templates/compPalabras2.html',
        controller: 'wordsCtrl'
      }
    }
  })
  .state('app.compPalabrasEsp', {
    cache: false,
    reload: true,
    url: '/compPalabrasEsp/:palabrasEspId',
    views: {
      'menuContent': {
        templateUrl: 'templates/compPalabrasEsp.html',
        controller: 'completarPalabrasEspCtrl'
      }
    }
  })
  .state('app.compPalabrasEng', {
    cache: false,
    reload: true,
    url: '/compPalabrasEng/:palabrasEngId',
    views: {
      'menuContent': {
        templateUrl: 'templates/compPalabrasEng.html',
        controller: 'completarPalabrasEngCtrl'
      }
    }
  })
  .state('app.compSonidos', {
    url: '/compSonidos',
    views: {
      'menuContent': {
        templateUrl: 'templates/compSonidos.html',
        controller: 'wordsCtrl'
      }
    }
  })
  .state('app.compSonidos2', {
    url: '/compSonidos2',
    views: {
      'menuContent': {
        templateUrl: 'templates/compSonidos2.html',
        controller: 'wordsCtrl'
      }
    }
  })
  .state('app.compSonidosEsp', {
    cache: false,
    reload: true,
    url: '/compSonidosEsp/:palabrasEspId',
    views: {
      'menuContent': {
        templateUrl: 'templates/compSonidosEsp.html',
        controller: 'completarPalabrasEspCtrl'
      }
    }
  })
  .state('app.compSonidosEng', {
    cache: false,
    reload: true,
    url: '/compSonidosEng/:palabrasEngId',
    views: {
      'menuContent': {
        templateUrl: 'templates/compSonidosEng.html',
        controller: 'completarPalabrasEngCtrl'
      }
    }
  })

  .state('app.registro', {
      url: '/registro',
      views: {
        'menuContent': {
          templateUrl: 'templates/registro.html',
          controller: 'registerCtrl'
        }
      }
  })
  .state('app.login', {
    cache: false,
    reload: true,
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      }
    }
  })
  .state('app.olvidoClave', {
      url: '/olvidoClave',
      views: {
        'menuContent': {
          templateUrl: 'templates/olvidoClave.html',
          controller: 'olvidoClaveCtrl'
        }
      }
  })
  .state('app.cambiarClave', {
      url: '/cambiarClave',
      views: {
        'menuContent': {
          templateUrl: 'templates/cambiarClave.html',
          controller: 'cambiarClaveCtrl'
        }
      }
  })
  .state('app.inicio', {
    cache: false,
    reload: true,
    url: '/inicio',
    views: {
      'menuContent': {
        templateUrl: 'templates/inicio.html'
      }
    }
  })
  .state('app.inicio2', {
    cache: false,
    reload: true,
    url: '/inicio2',
    views: {
      'menuContent': {
        templateUrl: 'templates/inicio2.html',
        controller: 'ProfileCtrl'
      }
    }
  })
  .state('app.letras', {
    url: '/letras',
    views: {
      'menuContent': {
        templateUrl: 'templates/letras.html',
        controller: 'LettersCtrl'
      }
    }
  });

  //$urlRouterProvider.otherwise('/app/inicio');
  $urlRouterProvider.otherwise(mainViewProvider.getCurrentMainView());
});
