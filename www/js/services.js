/* global angular, db */
angular.module('starter.services', [])

.service('userService', function() {
  this.userData = {};

  this.user = function() {
    return this.userData;
  };

  this.setId = function(id_app_user) {
    this.userData.id_app_user = id_app_user;
  };
  this.getId = function() {
    return this.userData.id_app_user;
  };

  this.setName = function(name) {
    this.userData.name = name;
  };
  this.getName = function() {
    return this.userData.name;
  };

  this.setUsername = function(username) {
    this.userData.username = username;
  };
  this.getUsername = function() {
    return this.userData.username;
  };

  this.setPassword = function(password) {
    this.userData.password = password;
  };
  this.getPassword = function() {
    return this.userData.password;
  };

  this.setEmail = function(email) {
    this.userData.email = email;
  };
  this.getEmail = function() {
    return this.userData.email;
  };

  this.setIdPlayer = function(player_id) {
    this.userData.player_id = player_id;
  };
  this.getIdPlayer = function() {
    return this.userData.player_id;
  };

  this.setPicture = function(image_path) {
    this.userData.image_path = image_path;
  };
  this.getPicture = function() {
    return this.userData.image_path;
  };

  this.setSound = function(sound_path) {
    this.userData.sound_path = sound_path;
  };
  this.getSound = function() {
    return this.userData.sound_path;
  };

  this.setCongrats = function(congrats_path) {
    this.userData.congrats_path = congrats_path;
  };
  this.getCongrats = function() {
    return this.userData.congrats_path;
  };

  this.setStatus = function(status) {
    this.userData.status = status;
  };
  this.getStatus = function() {
    return this.userData.status;
  };

})


.factory('DBA', function($cordovaSQLite, $q, $ionicPlatform) {
  var self = this;

  self.query = function (query, parameters) {
    parameters = parameters || [];
    var q = $q.defer();

    $ionicPlatform.ready(function () {
      $cordovaSQLite.execute(db, query, parameters)
        .then(function (result) {
          q.resolve(result);
        }, function (error) {
            q.reject(error);
        });
    });
    return q.promise;
  };

  self.getAll = function(result) {
    var output = [];
    for (var i = 0; i < result.rows.length; i++) {
      output.push(result.rows.item(i));
    }
    return output;
  };

  // Procesa un resultado simple
  self.getById = function(result) {
    var output = null;
    output = angular.copy(result.rows.item(0));
    return output;
  };

  return self;
})

// --- Letras ---
.factory('Letters', function(DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM letters WHERE learning_id = 6")
      .then(function(result){
        return DBA.getAll(result);
      });
  };

  self.allNumbers10 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 0,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers20 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 10,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers30 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 20,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers40 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 30,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers50 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 40,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers60 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 50,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers70 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 60,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers80 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 70,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers90 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 80,10")
      .then(function(result){
        return DBA.getAll(result);
      });
  };
  self.allNumbers100 = function(){
    return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 3) LIMIT 90,11")
      .then(function(result){
        return DBA.getAll(result);
      });
  };

  self.allVocals = function(){
    return DBA.query("SELECT english_sound, content, title FROM letters WHERE learning_id = 4")
      .then(function(result){
        return DBA.getAll(result);
      });
  };

  self.get = function(letterId) {
    var parameters = [letterId];
    return DBA.query("SELECT Id, content, title FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };

  self.add = function(letter) {
    var parameters = [letter.Id,letter.content,letter.custom_image_path,letter.custom_sound_path,letter.learning_id,letter.system_image_id,letter.system_sound_id,letter.title];
    return DBA.query("INSERT INTO letters (Id, content, custom_image_path, custom_sound_path, learning_id, system_image_id, system_sound_id, title) VALUES (?,?,?,?,?,?,?,?)", parameters);
  };

  self.remove = function(letterid) {
    var parameters = [letterid];
    return DBA.query("DELETE FROM letter WHERE id = (?)", parameters);
  };

  self.update = function(origLetter, editLetter) {
    var parameters = [editLetter.content,editLetter.custom_image_path, editLetter.custom_sound_path, editLetter.learning_id, editLetter.system_image_id, editLetter.system_sound_id, editLetter.title, origLetter.id];
    return DBA.query("UPDATE letter SET content = (?), custom_image_path = (?), custom_sound_path = (?), learning_id = (?), system_image_id = (?), system_sound_id = (?), title = (?) WHERE Id = (?)", parameters);
  };
  return self;
})

// --- Sonidos simples, compuestos ---
.factory('Sounds', function(DBA) {
 var self = this;

 self.allSimple = function(){
     return DBA.query("SELECT * FROM letters WHERE learning_id = 2 ORDER BY Id")
      .then(function(result){
        return DBA.getAll(result);
      });
 };
 self.allCompuestos = function(){
     return DBA.query("SELECT * FROM letters WHERE learning_id = 5 ORDER BY title")
      .then(function(result){
        return DBA.getAll(result);
      });
 };

 self.getSimple = function(simpleId) {
    var parameters = [simpleId];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getCompuesto = function(compuestoId) {
    var parameters = [compuestoId];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getEditSimple = function(Id) {
    var parameters = [Id];
    return DBA.query("SELECT * FROM words WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getEditCompuesto = function(Id) {
    var parameters = [Id];
    return DBA.query("SELECT * FROM words WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getSimpleArr = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM words WHERE letter_id = (?)",parameters)
        .then(function(result){
          return DBA.getAll(result);
      });
  };
  self.getCompArr = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM words WHERE letter_id = (?)",parameters)
        .then(function(result){
          return DBA.getAll(result);
      });
  };
  self.updateSound = function(wordId, custom_sound_path) {
    var parameters = [custom_sound_path, wordId];
    return DBA.query("UPDATE words SET custom_sound_path = (?) WHERE Id = (?)", parameters);
  };
  return self;
})

// --- Colores ---
.factory('Colores', function(DBA) {
 var self = this;

 self.allColors = function(){
     return DBA.query("SELECT letters.content, letters.title, words.content_english FROM letters, words WHERE (letters.Id = words.letter_id AND letters.learning_id = 7) ORDER BY letters.title")
      .then(function(result){
        return DBA.getAll(result);
      });
 };
 return self;
})

// --- Familia ---
.factory('Familia', function(DBA) {
 var self = this;

 self.allFamiliaMembers = function(){
     return DBA.query("SELECT Id, description, description_en, cant FROM family_members WHERE Id NOT IN (SELECT family_member_id FROM family)")
     //return DBA.query("SELECT Id, description, description_en, cant FROM family_members WHERE cant <> '0' ")
      .then(function(result){
        return DBA.getAll(result);
      });
 };
 self.allFamily = function(){
     return DBA.query("SELECT * FROM family as familia, family_members as miembro_familia, words as words WHERE (familia.family_member_id = miembro_familia.Id AND familia.word_id = words.Id) ORDER BY familia.Id ASC")
      .then(function(result){
        return DBA.getAll(result);
      });
 };
 self.getFamiliaMembers = function(Id) {
    var parameters = [Id];
    return DBA.query("SELECT * FROM family_members WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
 };
 self.UpdateFamiliaMembers = function(Id, cant) {
    var parameters = [cant, Id];
    return DBA.query("UPDATE family_members SET cant = (?) WHERE Id = (?)", parameters);
 };
 self.updateFamiliaMembers2 = function(word_id) {
    var parameters = [word_id];
    return DBA.query("UPDATE family_members SET cant = cant+1 WHERE Id IN (SELECT family_member_id FROM family WHERE word_id = (?))", parameters);
 };

 self.ConsultaFamilia = function(word_id) {
    var parameters = [word_id];
    return DBA.query("SELECT * FROM family_members, family WHERE family.family_member_id = family_members.Id AND family.word_id = (?)", parameters)
    .then(function(result) {
      return DBA.getById(result);
    });
 };

 self.getFamilia = function(word_id) {
   var parameters = [word_id];
   return DBA.query("SELECT * FROM family, family_members, words WHERE family.family_member_id = family_members.Id AND family.word_id = words.Id AND word_id = (?)", parameters)
     .then(function(result) {
       return DBA.getById(result);
     });
 };
 self.addFamilia = function(familia) {
   var parameters = [familia.Id, familia.family_member_id, familia.word_id, familia.name, familia.congrats_sound_path];
   return DBA.query("INSERT INTO family (Id, family_member_id, word_id, name, congrats_sound_path) VALUES (?,?,?,?,?)", parameters);
 };
 self.deleteFamilia = function(word_id) {
   var parameters = [word_id];
   return DBA.query("DELETE FROM family WHERE word_id = (?)", parameters);
 };

 self.updateFamiliaSound = function(word_id, congrats_sound_path) {
   var parameters = [congrats_sound_path, word_id];
   return DBA.query("UPDATE family SET congrats_sound_path = (?) WHERE word_id = (?)", parameters);
 };
 self.updateFamiliaImg = function(Id, custom_image_path) {
   var parameters = [custom_image_path, Id];
   return DBA.query("UPDATE words SET custom_image_path = (?) WHERE Id = (?)", parameters);
 };
 self.updateFamiliaCont = function(word_id, name) {
   var parameters = [name, word_id];
   return DBA.query("UPDATE family SET name = (?) WHERE word_id = (?)", parameters);
 };

 self.updateFamiliaContWord = function(Id, content, custom_sound_path, title) {
   var parameters = [content, custom_sound_path, title, Id];
   return DBA.query("UPDATE words SET content = (?), custom_sound_path = (?), title = (?) WHERE Id = (?)", parameters);
 };

 return self;

})

.factory('Words', function(DBA) {
 var self = this;

 self.allLetters = function(){
     return DBA.query("SELECT * FROM letters WHERE learning_id = 1 ORDER BY Id ASC")
      .then(function(result){
        return DBA.getAll(result);
      });
 };
 self.allCompuestos = function(){
     return DBA.query("SELECT * FROM letters WHERE learning_id = 5 ORDER BY title")
      .then(function(result){
        return DBA.getAll(result);
      });
 };

 self.getSimple = function(simpleId) {
    var parameters = [simpleId];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getCompuesto = function(compuestoId) {
    var parameters = [compuestoId];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getEditSimple = function(Id) {
    var parameters = [Id];
    return DBA.query("SELECT * FROM words WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getEditCompuesto = function(Id) {
    var parameters = [Id];
    return DBA.query("SELECT * FROM words WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getSimpleArr = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM words WHERE letter_id = (?)",parameters)
        .then(function(result){
          return DBA.getAll(result);
      });
  };
  self.getCompArr = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM words WHERE letter_id = (?)",parameters)
        .then(function(result){
          return DBA.getAll(result);
      });
  };
  self.updateWord = function(Id, custom_sound_path) {
    var parameters = [custom_sound_path, Id];
    return DBA.query("UPDATE words SET custom_sound_path = (?) WHERE Id = (?)", parameters);
  };
  self.updateWordImg = function(Id, custom_image_path) {
    var parameters = [custom_image_path, Id];
    return DBA.query("UPDATE words SET custom_image_path = (?) WHERE Id = (?)", parameters);
  };
  self.updateWordCont = function(Id, content) {
    var parameters = [content, Id];
    return DBA.query("UPDATE words SET content = (?) WHERE Id = (?)", parameters);
  };
  self.updateWordContEnglish = function(Id, content_english) {
    var parameters = [content_english, Id];
    return DBA.query("UPDATE words SET content_english = (?) WHERE Id = (?)", parameters);
  };
  self.updateLetterId = function(Id, letter_id) {
    var parameters = [letter_id, Id];
    return DBA.query("UPDATE words SET letter_id = (?) WHERE Id = (?)", parameters);
  };
  self.updateWordContNew = function(Id, content, title) {
    var parameters = [content, title, Id];
    return DBA.query("UPDATE words SET content = (?), title = (?) WHERE Id = (?)", parameters);
  };
  self.addWord = function(word) {
    var parameters = [word.sound_title,word.image_id,word.content_english,word.letter_id_english,word.letter_eng,word.Id,word.content,word.custom_image_path,word.custom_sound_path,word.letter_id,word.title,word.level,word.tword];
    return DBA.query("INSERT INTO words (sound_title, image_id, content_english, letter_id_english, letter_eng, Id, content, custom_image_path, custom_sound_path, letter_id, title, level, tword) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
  };
  self.deleteWord = function(Id) {
    var parameters = [Id];
    return DBA.query("DELETE FROM words WHERE Id = (?)", parameters);
  };
  self.addProfile = function(word) {
    var parameters = [word.name_sound_path,word.word_id,word.profile_image_path,word.congrats_path,word.title,word.content,word.letter_id,word.selected];
    return DBA.query("INSERT INTO learners (name_sound_path, word_id, profile_image_path, congrats_path, title, content, letter_id, selected) VALUES (?,?,?,?,?,?,?,?)", parameters);
  };

  self.getWords = function(simpleId) {
    var parameters = [simpleId];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getWordsArr = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM words WHERE letter_id = (?)", parameters)
      .then(function(result){
          return DBA.getAll(result);
      });
  };
  self.getWordsToCompare = function(value) {
    var parameters = [value];
    return DBA.query("SELECT title,letter_id FROM words WHERE title = (?)", parameters)
      .then(function(result){
          return DBA.getById(result);
      });
  };
  self.getNumLetterEng = function(letter_id_english) {
    var parameters = [letter_id_english];
    return DBA.query("SELECT * FROM words, letters WHERE words.letter_id_english = letters.Id AND letter_id_english = (?)", parameters)
      .then(function(result){
          return DBA.getById(result);
      });
  };

 // --- Completar ---
  self.getCompletarPalabras = function(value) {
    var parameters = [value];
    return DBA.query("SELECT * FROM letters WHERE Id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.allcompletarPalabrasEsp = function(completarId){
      var parameters = [completarId];
      return DBA.query("SELECT DISTINCT * FROM words WHERE letter_id = (?) ORDER BY ABS(RANDOM()) LIMIT 1", parameters)
       .then(function(result){
         return DBA.getAll(result);
       });
  };
  self.allcompletarPalabrasEng = function(completarId){
      var parameters = [completarId];
      return DBA.query("SELECT DISTINCT * FROM words WHERE letter_id_english = (?) ORDER BY ABS(RANDOM()) LIMIT 1", parameters)
       .then(function(result){
         return DBA.getAll(result);
       });
  };

  self.getProfileLetter = function(letter) {
    var parameters = [letter];
    return DBA.query("SELECT * FROM letters WHERE title = (?) and learning_id = 1", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  // Para obtener el próximo ID a insertar con sqlite_sequence
  self.getLastIdWord = function() {
    return DBA.query("SELECT seq AS Id FROM sqlite_sequence WHERE name='words'")
      .then(function(result) {
        return DBA.getById(result);
      });
  };

  self.getProfileWord = function() {
    return DBA.query("SELECT * FROM words ORDER BY Id DESC LIMIT 1")
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getProfileSimple = function() {
    return DBA.query("SELECT name_sound_path, profile_image_path, Id, title, selected FROM learners WHERE selected = '1'")
      .then(function(result) {
        return DBA.getById(result);
      });
  };
  self.getProfileContent = function(content) {
    var parameters = [content];
    return DBA.query("SELECT * FROM learners WHERE title = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  };

  self.updateProfileFull = function(content, profile) {
    var parameters = [profile.name_sound_path,profile.word_id,profile.profile_image_path,profile.congrats_path,profile.selected,content];
    return DBA.query("UPDATE learners SET name_sound_path = (?), word_id= (?), profile_image_path = (?), congrats_path = (?), selected = (?)  WHERE title = (?)", parameters);
  };
  self.updateProfileSrc = function(Id, name_sound_path) {
    var parameters = [name_sound_path, Id];
    return DBA.query("UPDATE learners SET name_sound_path = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileIdword = function(Id, word_id) {
    var parameters = [word_id, Id];
    return DBA.query("UPDATE learners SET word_id = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileImg = function(Id, profile_image_path) {
    var parameters = [profile_image_path, Id];
    return DBA.query("UPDATE learners SET profile_image_path = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileLetterID = function(Id, letter_id) {
    var parameters = [letter_id, Id];
    return DBA.query("UPDATE learners SET letter_id = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileSelected = function(Id, selected) {
    var parameters = [selected, Id];
    return DBA.query("UPDATE learners SET selected = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileSrcCon = function(Id, congrats_path) {
    var parameters = [congrats_path, Id];
    return DBA.query("UPDATE learners SET congrats_path = (?) WHERE Id = (?)", parameters);
  };
  self.updateProfileCont = function(Id, content, title) {
    var parameters = [content, title, Id];
    return DBA.query("UPDATE learners SET content = (?), title = (?) WHERE Id = (?)", parameters);
  };
  self.deleteOldWord = function(Id) {
    var parameters = [Id];
    return DBA.query("DELETE FROM words WHERE Id = (?)", parameters);
  };

  return self;

})

.factory('Utilities', function() {
    var self = this;
    self.removeAccents = function (source) {
        var accent = [
            /[\300-\306]/g, /[\340-\346]/g, // A, a
            /[\310-\313]/g, /[\350-\353]/g, // E, e
            /[\314-\317]/g, /[\354-\357]/g, // I, i
            /[\322-\330]/g, /[\362-\370]/g, // O, o
            /[\331-\334]/g, /[\371-\374]/g, // U, u
            /[\321]/g, /[\361]/g, // N, n
            /[\307]/g, /[\347]/g, // C, c
        ],
        noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
        for (var i = 0; i < accent.length; i++){
            source = source.replace(accent[i], noaccent[i]);
        }
        return source;
    };
  return self;
 })

 // ---------------------- PROVIDERS -----------------------
 .provider('mainView', function() {
  //var someCondition = false;

  this.getCurrentMainView = function() {
    var value = false;
    var exp = window.localStorage.getItem('count');

    // Inicializo para visualizar nombre de perfil en barra menú
    window.localStorage.setItem('profile', 'Inicio');

    if (exp == 1) {
         value = window.localStorage.getItem('profile');
    }
    if (value) {
      return "/app/inicio";
    }
    else {
      return "/app/login";
    }
  };
  //return a dummy factory that will never be used or injected
  this.$get = ["nullService", function(){return null;}];
});
